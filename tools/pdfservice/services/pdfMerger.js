const hummus = require('hummus');

/**
 * Concatenate list of PDFs into one file
 *
 * @param {string[]} paths array of paths of pdf files to merge
 * @param {string} outputPath path to output the merged pdf file
 */
exports.combinePDFsAsync = async (paths, outputPath) => {
    try {
        console.group('combining pdfs in paths:');
        console.log(paths);
        console.log('will be outputted to: ' + outputPath);
        combinePDFs(paths, outputPath);
        await deleteFilesAsync(paths);
        console.log('combining completed');
        console.groupEnd();
    } catch (er) {
        throw new Error('Error during PDF combination: ' + er.message);
    }
};

const combinePDFs = (paths, outputPath) => {
    var outputFile = new hummus.PDFWStreamForFile(outputPath);
    var pdfWriter = hummus.createWriter(outputFile);
    for (const path of paths) {
        var pdfStream = new hummus.PDFRStreamForFile(path);
        pdfWriter.appendPDFPagesFromPDF(pdfStream);
        pdfStream.close();
    }

    pdfWriter.end();
    outputFile.close();
};

const deleteFilesAsync = async (paths) => {
    const fs = require('fs-extra');
    for (const path of paths) {
        await fs.remove(path);
    }
};