module.exports = {
  apps : [{
    name: 'PDF-SERVICE',
    script: 'app.js',
    cwd: '/var/www/pdfservice/',
    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    // args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    output: '/var/www/pdfservice/logs.log',
    error: '/var/www/pdfservice/errorlogs.log',
    env: {
      NODE_ENV: 'development',
      PORT: 8150
    },
    env_production: {
      NODE_ENV: 'production',
      PORT: 8150
    }
  }],

  deploy : {
  }
};
