'use strict';

/**
 * @module communication/backInStock
 */

const sendTrigger = require('./util/send').sendTrigger;
var hookPath = 'app.communication.backInStock.';

/**
 * Trigger an back in stock notification
 * @param {SynchronousPromise} promise sync promise of handler framework
 * @param {module:communication/util/trigger~BackInStock} data  backInStockData
 * @returns {SynchronousPromise} synch promise
 */
function send(promise, data) {
    return sendTrigger(hookPath + 'send', promise, data);
}

/**
 * Declares attributes available for data mapping configuration
 * @returns {Object} Map of hook function to an array of strings
 */
function triggerDefinitions() {
    return {
        send: {
            description: 'Back In Stock Trigger',
            attributes: [
                'BackInStock.email',
                'BackInStock.productId',
                'BackInStock.locale'
            ]
        }
    };
}

module.exports = require('dw/system/HookMgr').callHook(
    'app.communication.handler.initialize',
    'initialize',
    require('./handler').handlerID,
    'app.communication.backInStock',
    {
        send: send
    }
);

// non-hook exports
module.exports.triggerDefinitions = triggerDefinitions;
