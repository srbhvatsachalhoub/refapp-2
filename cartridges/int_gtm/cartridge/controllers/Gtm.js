'use strict';

var server = require('server');

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');

server.get('DataLayer', function (req, res, next) {
    var pageDataLayer = JSON.parse(req.querystring.datalayer);
    gtmDataLayerHelpers.attachNonCachablePageDataLayer(req, res, pageDataLayer, req.querystring.action);
    res.render('/gtm/pushdatalayer');
    next();
});

module.exports = server.exports();
