'use strict';

var server = require('server');
server.extend(module.superModule);

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Begin', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkout(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkoutOption(req, res);
    next();
});

module.exports = server.exports();
