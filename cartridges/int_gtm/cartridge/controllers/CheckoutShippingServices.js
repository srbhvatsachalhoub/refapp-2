'use strict';

var server = require('server');
server.extend(module.superModule);

var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('SubmitShipping', function (req, res, next) {
    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        gtmEnhancedEcommerceDataLayerHelpers.checkout(req, res, 'payment');

        // attach shipping method checkoutoption
        gtmEnhancedEcommerceDataLayerHelpers.checkoutOption(req, res, 'shipping');
    });
    next();
});

module.exports = server.exports();
