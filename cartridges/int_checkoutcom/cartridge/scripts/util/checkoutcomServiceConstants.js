'use strict';

var CurrentSite = require('dw/system/Site').getCurrent();

/**
 * Define checkout.com service related constants.
 */
module.exports = {
    apiURL: CurrentSite.getCustomPreferenceValue('checkoutcomMode').value === 'production' ? 'https://api.checkout.com/payments' : 'https://api.sandbox.checkout.com/payments',
    tokensURL: CurrentSite.getCustomPreferenceValue('checkoutcomMode').value === 'production' ? 'https://api.checkout.com/tokens' : 'https://api.sandbox.checkout.com/tokens',
    serviceCredentialType: CurrentSite.getCustomPreferenceValue('checkoutcomServiceCredentialType').value,
    serviceCredentialId: CurrentSite.getCustomPreferenceValue('checkoutcomServiceCredentialId'),
    autoCapture: CurrentSite.getCustomPreferenceValue('checkoutcomAutoCapture') || false,
    autoCaptureTime: CurrentSite.getCustomPreferenceValue('checkoutcomAutoCaptureTime'),
    threeDSecure: CurrentSite.getCustomPreferenceValue('checkoutcom3ds') || false,
    nonThreeDSecure: CurrentSite.getCustomPreferenceValue('checkoutcomN3ds') || false,
    risk: CurrentSite.getCustomPreferenceValue('checkoutcomRisk') || false
};
