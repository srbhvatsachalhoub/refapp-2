'use strict';

/**
 * Define checoutcom related constants.
 */
module.exports = {
    action: {
        authorization: 'authorization',
        paymentDetail: 'payment-detail',
        capture: 'capture',
        refund: 'refund',
        void: 'void',
        token: 'token',
        knet: 'knet'
    },
    sourceType: {
        card: 'card',
        id: 'id',
        applepay: 'applepay',
        token: 'token',
        knet: 'knet'
    },
    paymentType: {
        regular: 'Regular',
        recurring: 'Recurring',
        moto: 'MOTO'
    },
    eci: {
        ecommerce: 'ECOMMERCE'
    },
    serviceCredentialType: {
        single: 'single',
        country: 'country',
        locale: 'locale'
    },
    logExcludedKeys: ['number', 'expiry_month', 'expiry_year', 'cvv', 'success_url', 'failure_url'],
    noteExcludedKeys: ['billing_address', 'phone', 'billing_descriptor', 'shipping', '_links', 'payment_ip', 'description', 'requested_on', 'last4', 'bin', 'cryptogram', 'xid', 'scheme_id', 'fingerprint'],
    serviceStatus: {
        pending: 'Pending',
        authorized: 'Authorized',
        cardVerified: 'Card Verified',
        voided: 'Voided',
        partiallyCaptured: 'Partially Captured',
        captured: 'Captured',
        partiallyRefunded: 'Partially Refunded',
        refunded: 'Refunded',
        declined: 'Declined',
        cancelled: 'Canceled'
    },
    serviceParams: {
        links: '_links',
        redirect: 'redirect',
        href: 'href',
        ckoSessionId: 'cko-session-id'
    },
    webhooksType: {
        paymentApproved: 'payment_approved',
        paymentFlagged: 'payment_flagged',
        paymentPending: 'payment_pending',
        paymentDeclined: 'payment_declined',
        paymentExpired: 'payment_expired',
        paymentCancelled: 'payment_cancelled',
        paymentVoided: 'payment_voided',
        paymentVoidDeclined: 'payment_void_declined',
        paymentCaptured: 'payment_captured',
        paymentCaptureDeclined: 'payment_capture_declined',
        paymentCapturePending: 'payment_capture_pending',
        paymentRefunded: 'payment_refunded',
        paymentRefundDeclined: 'payment_refund_declined',
        paymentRefundPending: 'payment_refund_pending',
        paymentChargeback: 'payment_chargeback',
        paymentRetrieval: 'payment_retrieval'
    },
    pspPaymentStatus: {
        authorized: 'authorized',
        captured: 'captured',
        declined: 'declined',
        pending: 'pending',
        voided: 'voided',
        refunded: 'refunded',
        error: 'error'
    },
    pspPaymentStatusNoteSubject: 'Payment Status',
    creditCardTypes: {
        mada: 'Mada'
    }
};
