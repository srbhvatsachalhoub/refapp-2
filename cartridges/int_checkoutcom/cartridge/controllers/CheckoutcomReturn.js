'use strict';
/* global session */

var server = require('server');

var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');
var logger = require('dw/system/Logger').getLogger('checkoutcom.return');
var checkoutcomHelpers = require('*/cartridge/scripts/helpers/checkoutcomHelpers');
var checkoutcomConstants = require('*/cartridge/scripts/util/checkoutcomConstants');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

/**
 * Error processed for Return end point
 * @param {dw.order.Order} order - The order to be proccesed
 * @param {string} logMessage - log message to be logged
 */
function returnError(order, logMessage) {
    if (order) {
        checkoutcomHelpers.failOrder(order);
    }

    logger.error(logMessage);
}

/**
 * Processes the response both for Tokenization and Authorization
 * @param {Object} req - the Request Object
 * @param {Object} res - the Response object
 * @param {string} endPoint - the endPoint
 * @returns {Object} - processed result
 */
function processResponse(req, res, endPoint) {
    var OrderMgr = require('dw/order/OrderMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var PaymentMgr = require('dw/order/PaymentMgr');

    var ckoSessionId = req.querystring[checkoutcomConstants.serviceParams.ckoSessionId];
    // check the provided checkoutcom session id value
    if (!ckoSessionId) {
        returnError(
            null,
            StringUtils.format('Error on CheckoutcomReturn-{0}, ckoSessionId is empty', endPoint)
        );
        res.redirect(URLUtils.url('Cart-Show', 'psperror', '1'));
        return {
            error: true
        };
    }

    var paymentDetailResult = checkoutcomHelpers.getPaymentDetailsByCheckoutcomId(ckoSessionId);
    if (paymentDetailResult.error) {
        returnError(
            null,
            StringUtils.format('Error on CheckoutcomReturn-{0}, checkoutcom id: {1}', endPoint, ckoSessionId)
        );
        res.redirect(URLUtils.url('Cart-Show', 'psperror', '1'));
        return {
            error: true
        };
    }

    var postedPayload = paymentDetailResult.paymentDetail;
    var order = OrderMgr.getOrder(postedPayload.reference || postedPayload.source.user_defined_field1);
    if (!order) {
        returnError(
            null,
            StringUtils.format('Error on CheckoutcomReturn-{0}, order is empty, payload: {1}',
                endPoint,
                checkoutcomHelpers.getLogablePayload(postedPayload, false))
        );
        res.redirect(URLUtils.url('Cart-Show', 'psperror', '1'));
        return {
            error: true
        };
    }

    // add payload to order note as response
    checkoutcomHelpers.addNoteToOrder(order, StringUtils.format('Response {0}', endPoint), postedPayload);

    // check the status of authorization is approved
    // if it is not success then fail the order and stop process
    if (!postedPayload.approved
        && postedPayload.status !== checkoutcomConstants.serviceStatus.authorized
        && postedPayload.status !== checkoutcomConstants.serviceStatus.captured) {
        // set last payment status as declined
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.declined);

        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}, posted payload status is not OK, orderNo: {1}, payload: {2}',
                endPoint,
                order.orderNo,
                checkoutcomHelpers.getLogablePayload(postedPayload, false))
        );
        var redirectUrl = URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1');
        if (postedPayload.actions && postedPayload.actions[0]) {
            redirectUrl.append('code', postedPayload.actions[0].response_code || postedPayload.actions[0].type);
        }

        session.custom.knetPaymentId = postedPayload.source.knet_payment_id || null;
        session.custom.knetTransactionId = postedPayload.source.knet_transaction_id || null;

        res.redirect(redirectUrl);
        return {
            error: true
        };
    }

    var paymentInstrumentPaymentMethod =
        postedPayload.source.type === checkoutcomConstants.sourceType.knet
            ? paymentMethodHelpers.getKnetPaymentMethodId()
            : PaymentInstrument.METHOD_CREDIT_CARD;
    var paymentInstruments = order.getPaymentInstruments(paymentInstrumentPaymentMethod);
    if (paymentInstruments.empty) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, 'Payment Instruments are empty.');

        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}, order payment instrument is empty, orderNo: {1}, payload: {2}',
                endPoint,
                order.orderNo,
                checkoutcomHelpers.getLogablePayload(postedPayload, false))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    var paymentInstrument = paymentInstruments[0];
    var paymentMethod = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod);
    if (!paymentMethod) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, 'Payment Method is empty.');

        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}, payment method is empty, orderNo: {1}, payload: {2}',
                endPoint,
                order.orderNo,
                checkoutcomHelpers.getLogablePayload(postedPayload, false))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    var paymentProcessor = paymentMethod.paymentProcessor;
    if (!paymentProcessor) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, 'Payment Processor is empty.');

        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}, payment processor is empty, orderNo: {1}, payload: {2}',
                endPoint,
                order.orderNo,
                checkoutcomHelpers.getLogablePayload(postedPayload, false))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    return {
        success: true,
        postedPayload: postedPayload,
        order: order,
        paymentInstrument: paymentInstrument,
        paymentProcessor: paymentProcessor
    };
}

/**
 * Continues on to finalize Authorize process
 * @param {Object} req - the Request Object
 * @param {Object} res - the Response object
 * @param {Object} next - the next function to be executed
 * @param {string} endPoint - the endPoint
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted JSON payload object
 * @returns {Object} - the result object
 */
function continueAuthorization(req, res, next, endPoint, order, paymentInstrument, paymentProcessor, postedPayload) {
    var continueAuthorizedCreditCardResult = {};
    try {
        continueAuthorizedCreditCardResult = checkoutcomHelpers.continueAuthorizedCreditCard(
            req,
            order,
            paymentInstrument,
            paymentProcessor,
            postedPayload
        );
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, e.message);

        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}-continueAuthorization, exception occured, orderNo: {1}, message: {2}, payload: {3}',
                endPoint,
                order.orderNo,
                e.message,
                checkoutcomHelpers.getLogablePayload(postedPayload, false)
            )
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return next();
    }

    if (continueAuthorizedCreditCardResult.error) {
        returnError(
            order,
            StringUtils.format('Error on CheckoutcomReturn-{0}-continueAuthorization, orderNo: {1}, message: {2}, payload: {3}',
                endPoint,
                order.orderNo,
                continueAuthorizedCreditCardResult.message,
                checkoutcomHelpers.getLogablePayload(postedPayload, false)
            )
        );

        if (continueAuthorizedCreditCardResult.fraudDetectionStatus
            && continueAuthorizedCreditCardResult.fraudDetectionStatus.status === 'fail') {
            res.redirect(continueAuthorizedCreditCardResult.redirectUrl);
            return next();
        }

        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return next();
    }

    // success, redirect to order confirmation
    res.redirect(URLUtils.url('Order-Confirm', 'ID', order.orderNo, 'token', order.orderToken));
    return next();
}

/**
 * Controller To AuthorizationSuccess Return URL
 * to be called by checkout.com with cko-session-id
 */
server.get('AuthorizationSuccess', function (req, res, next) {
    var responseResult = processResponse(req, res, 'AuthorizationSuccess');
    if (responseResult.error) {
        return next();
    }

    var postedPayload = responseResult.postedPayload;
    var order = responseResult.order;
    var paymentInstrument = responseResult.paymentInstrument;
    var paymentProcessor = responseResult.paymentProcessor;

    return continueAuthorization(req, res, next, 'Authorization', order, paymentInstrument, paymentProcessor, postedPayload);
});

/**
 * Controller To AuthorizationFail Return URL
 * to be called by checkout.com with cko-session-id
 */
server.get('AuthorizationFail', function (req, res, next) {
    var responseResult = processResponse(req, res, 'AuthorizationFail');
    if (responseResult.error) {
        return next();
    }

    var postedPayload = responseResult.postedPayload;
    var order = responseResult.order;
    var paymentInstrument = responseResult.paymentInstrument;
    var paymentProcessor = responseResult.paymentProcessor;

    return continueAuthorization(req, res, next, 'Authorization', order, paymentInstrument, paymentProcessor, postedPayload);
});


module.exports = server.exports();
