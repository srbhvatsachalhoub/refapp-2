'use strict';

var productPagination = {};

productPagination.getProductsList = function (akeneoProductsURL, nextURL) {
    var config = require('~/cartridge/scripts/utils/generalUtils').config;
    var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
    var initAkeneoServices = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var akeneoService = initAkeneoServices.getGeneralService();
    akeneoService.setURL(config.serviceGeneralUrl);

    if (!nextURL) {
        AkeneoServicesHandler.nextUrl = '';
    } else {
        AkeneoServicesHandler.nextUrl = nextURL;
    }

    var productsList = AkeneoServicesHandler.serviceRequestProductAkeneo(akeneoService, akeneoProductsURL);
    var response = {
        productsList: productsList,
        serviceNextURL: AkeneoServicesHandler.nextUrl
    };

    return response;
};

module.exports = productPagination;
