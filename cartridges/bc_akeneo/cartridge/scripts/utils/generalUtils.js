/* general utility functions */
/* global request */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var Encoding = require('dw/crypto/Encoding');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var ObjectAttributeDefinition = require('dw/object/ObjectAttributeDefinition');
var removeCategoryPrefixes = ['UAE_', 'KSA_'];
var Utils = {};

/**
* Indicates if the category is part of navigation
* @param {string} categoryCode categoryCode
* @returns {boolean} boolean
*/
Utils.verifyNavigationCategory = function (categoryCode) {
    var topLevelPrefix = Site.current.getCustomPreferenceValue('akeneoTopLevelCategoryID') || '';
    topLevelPrefix = topLevelPrefix.split('_')[0] + '_';
    return categoryCode && categoryCode.indexOf(topLevelPrefix) !== -1;
};

Utils.adjustLocale = function () {
    var allowedLocales = require('dw/system/Site').getCurrent().allowedLocales.toArray();
    // Latin numeric system is not working for ar locales, although it is set properly in BM. Temporary fix to set english locale in job.
    var reqLocale;
    for (var i = 0; i < allowedLocales.length; i++) {
        if (allowedLocales[i].indexOf('en') !== -1) {
            reqLocale = allowedLocales[i];
            break;
        }
    }
    request.setLocale(reqLocale);
};

/**
 * Returns attribute type as string.
 * @param {Object} AkeneoAttribute AkeneoAttribute
 * @returns {string} valueType
 */
Utils.getAkeneoAttributeType = function (AkeneoAttribute) {
    var valueType;
    switch (AkeneoAttribute.type) {
        case 'pim_catalog_boolean' :
            valueType = 'boolean';
            break;
        case 'pim_catalog_simpleselect' :
        case 'pim_catalog_multiselect' :
            if (AkeneoAttribute.code.indexOf('response') > -1) {
                valueType = 'set-of-string';
            } else {
                valueType = 'enum-of-string';
            }
            break;
        case 'pim_catalog_file' :
        case 'pim_catalog_text' :
        case 'pim_catalog_metric' :
        case 'pim_reference_data_simpleselect' :
            valueType = 'string';
            break;
        case 'pim_catalog_number' :
            valueType = 'double';
            break;
        case 'pim_catalog_date' :
            valueType = 'date';
            break;
        case 'pim_catalog_image' :
            valueType = 'image';
            break;
        case 'pim_assets_collection' :
        case 'pim_catalog_price_collection' :
        case 'pim_reference_data_multiselect' :
        case 'akeneo_reference_entity_collection' :
        case 'akeneo_reference_entity' :
            valueType = 'set-of-string';
            break;
        case 'pim_catalog_textarea' :
        default :
            if (AkeneoAttribute.wysiwyg_enabled === true) {
                valueType = 'html';
            } else {
                valueType = 'text';
            }
            break;
    }
    return valueType;
};

/**
 * Returns attribute type as string.
 * @param {dw.object.ObjectAttributeDefinition} attributeDefinition attributeDefinition
 * @returns {string} valueType
 */
function getAttributeTypeString(attributeDefinition) {
    var valueType;
    switch (attributeDefinition.valueTypeCode) {
        case ObjectAttributeDefinition.VALUE_TYPE_BOOLEAN:
            valueType = 'boolean';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_DATE:
            valueType = 'date';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_DATETIME:
            valueType = 'datetime';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_EMAIL:
            valueType = 'email';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_ENUM_OF_INT:
            valueType = 'enum-of-int';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_ENUM_OF_STRING:
            valueType = 'enum-of-string';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_HTML:
            valueType = 'html';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_IMAGE:
            valueType = 'image';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_INT:
            valueType = 'int';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_MONEY:
            valueType = 'money';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_NUMBER:
            valueType = 'double';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_PASSWORD:
            valueType = 'password';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_QUANTITY:
            valueType = 'quantity';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_SET_OF_INT:
            valueType = 'set-of-int';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_SET_OF_NUMBER:
            valueType = 'set-of-number';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_SET_OF_STRING:
            valueType = 'set-of-string';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_TEXT:
            valueType = 'set-of-string';
            break;
        case ObjectAttributeDefinition.VALUE_TYPE_STRING:
        default:
            valueType = 'string';
            break;
    }
    return valueType;
}

/**
 * Get custom attributes for category and product
 * @param {string} type - category or product
 * @returns {Object} objectAttributes
 */
Utils.getCustomAttributeDefinitions = function (type) {
    var objectAttributes = {}; // stores attributeID: { type: string, system: true | false}
    var objectDef = SystemObjectMgr.describe(type);
    if (!objectDef) {
        return {};
    }
    var attributeDefinitions = objectDef.getAttributeDefinitions();
    for (var i = 0; i < attributeDefinitions.length; i++) {
        var attributeDefinition = attributeDefinitions[i];
        // attributeDefinition.isSystem()
        objectAttributes[attributeDefinition.ID] = {
            type: getAttributeTypeString(attributeDefinition),
            system: attributeDefinition.isSystem()
        };
    }
    return objectAttributes;
};

Utils.getCategoryId = function (categoryId) {
    if (!categoryId || !categoryId.length) {
        return categoryId;
    }
    removeCategoryPrefixes.forEach(function (prefix) {
        categoryId = categoryId.replace(prefix, ''); // eslint-disable-line
    });
    return categoryId;
};

/* Get Last Imported Time from custom object */
Utils.getLastImportedTime = function () {
    var importedTime = require('~/cartridge/scripts/jobs/importedTime');
    return importedTime.getLastImportedTime();
};

/* get Akeneo Product API URL for full import OR differential based on lastImportTime in custom object */
Utils.getAkeneoProductURLArgs = function (akeneoProductsUrl) {
    var lastImportTime = Utils.getLastImportedTime();
    var filterArg = {};
    var filterParams = '';

    if (this.config.importType === 'advanced') {
        try {
            var importBuilderConfigName = (akeneoProductsUrl.indexOf('product-models') === -1) ? 'akeneoProductsImportBuilderConfig' : 'akeneoModelProductsImportBuilderConfig';
            var importBuilderConfig = Utils.getJSONObjectFromSitePreferences(importBuilderConfigName);

            Object.keys(importBuilderConfig).forEach(function (key) {
                if (key === 'search') {
                    filterArg = importBuilderConfig.search;
                } else {
                    filterParams += '&' + key + '=' + importBuilderConfig[key];
                }
            });
        } catch (e) {
            throw new Error('ERROR while parsing import builder config: ' + e.message);
        }
    }

    if (lastImportTime) {
        filterArg.updated = this.config.APIURL.parameter.search.updated;
        filterArg.updated[0].value = lastImportTime;
    }
    return '?pagination_type=search_after&search=' + Encoding.toURI(JSON.stringify(filterArg)) + filterParams + '&limit=' + this.config.APIURL.parameter.pagination;
};

/**
 * @desc Get 1st level model product from cache
 * @param {Object} akeneoProduct - The product for which 1st level model product is required
 * @return {Object} - The 1st level model product
 */
Utils.getMasterModelProduct = function (akeneoProduct) {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var masterCode = akeneoProduct.parent;
    var cacheFileName = StringUtils.format(Utils.config.cacheDirectory.modelProducts.endPoint, masterCode);
    var modelProduct = customCacheWebdav.getCache(cacheFileName);

    if (!modelProduct) {
        modelProduct = Utils.fetchModelProductObject(masterCode);
    }

    if (!modelProduct.parent) {
        return modelProduct;
    }
    /* The modelProduct is a 2nd level model. Calling this method for 2nd level model */
    return Utils.getMasterModelProduct({
        parent: modelProduct.parent
    });
};

/* fetch model product from Akeneo and create custom object */
Utils.fetchModelProductObject = function (masterCode) {
    var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
    // define service used for call
    var akeneoService = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var AkeneoService = akeneoService.getGeneralService();
    var masterProduct;

    // setting the default akeneo hostname
    AkeneoService.setURL(this.config.serviceGeneralUrl);
    AkeneoServicesHandler.nextUrl = '';

    var serviceURL = StringUtils.format(this.config.APIURL.endpoints.getMasterProduct, masterCode);

    try {
        masterProduct = AkeneoServicesHandler.serviceRequestVariationProduct(AkeneoService, serviceURL);
        Utils.saveModelProductInCache(masterProduct);
    } catch (e) {
        throw new Error('ERROR : While calling service to get family variants  : ' + e.stack + ' with Error: ' + e.message);
    }

    return masterProduct;
};

/**
 * @desc Saves model product details to custom cache files
 * @param {Object} modelProduct - The model product to be saved in cache
 * */
Utils.saveModelProductInCache = function (modelProduct) {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var createModelProductCache = Utils.categoryMatchesWithConfig(modelProduct);

    if (createModelProductCache) {
        var fileName = StringUtils.format(Utils.config.cacheDirectory.modelProducts.endPoint, modelProduct.code);
        customCacheWebdav.clearCache(fileName);

        var akeneoModelProductObject = {
            code: modelProduct.code,
            parent: modelProduct.parent,
            masterFamilyVariant: modelProduct.family_variant,
            values: modelProduct.values,
            categories: modelProduct.categories,
            associations: modelProduct.associations
        };
        customCacheWebdav.setCache(fileName, akeneoModelProductObject);
    }
};

/* Check if scope is matching with the scope which is in configuration */
Utils.checkScope = function (attributeScope) {
    var akeneoScope = this.config.scope;

    if (attributeScope && akeneoScope) {
        return attributeScope === akeneoScope;
    }
    return true;
};


/* Check if the category belongs to the category tree ID mentioned in the custom preference Akeneo Main Catalogs */
Utils.categoryMatchesWithConfig = function (akeneoProduct) {
    var isImportableProduct = false;

    if (akeneoProduct.categories && akeneoProduct.categories.length > 0) {
        var topLevelCatCodes = Utils.config.customObjectType.TopLevelCategoriesCodes;
        var topLevelCatCodesCustomObj = CustomObjectMgr.getCustomObject(topLevelCatCodes, topLevelCatCodes);

        if (topLevelCatCodesCustomObj) {
            var categoriesCodeList = JSON.parse(topLevelCatCodesCustomObj.custom.categoriesCodes || '[]');
            for (var index = 0; index < akeneoProduct.categories.length; index++) {
                var akeneoCategory = Utils.getCategoryId(akeneoProduct.categories[index]);
                isImportableProduct = categoriesCodeList.indexOf(akeneoCategory) !== -1;

                if (isImportableProduct) {
                    break;
                }
            }
        } else {
            isImportableProduct = true;
        }
    } else {
        isImportableProduct = true;
    }

    return isImportableProduct;
};

/* save all the categories of catalog mentioned in the custom preference Akeneo Main Catalogs */
Utils.saveCategoriesToCustomObject = function (akeneoCategoriesList, akeneoMainCatalogs) {
    if (akeneoMainCatalogs.length > 0) {
        var categoryCustomObjectID = this.config.customObjectType.TopLevelCategoriesCodes;
        var categoryCustomObject = CustomObjectMgr.getCustomObject(categoryCustomObjectID, categoryCustomObjectID);

        if (!categoryCustomObject) {
            Transaction.begin();
            categoryCustomObject = CustomObjectMgr.createCustomObject(categoryCustomObjectID, categoryCustomObjectID);
            Transaction.commit();
        }
        var configMatchingTopCatCodes = JSON.parse(categoryCustomObject.custom.categoriesCodes || '[]');

        var categoriesIterator = akeneoCategoriesList.iterator();

        while (categoriesIterator.hasNext()) {
            var akeneoCategory = categoriesIterator.next();
            if (!Utils.verifyNavigationCategory(akeneoCategory.code)) {
                continue;
            }
            var akeneoCategoryId = Utils.getCategoryId(akeneoCategory.code);
            var akeneoParentCategoryId = Utils.getCategoryId(akeneoCategory.parent);
            if (configMatchingTopCatCodes.indexOf(akeneoCategoryId) === -1) {
                if (akeneoCategory.parent === null) {
                    if (akeneoMainCatalogs.indexOf(akeneoCategoryId) !== -1) {
                        configMatchingTopCatCodes.push(akeneoCategoryId);
                    }
                } else if (configMatchingTopCatCodes.indexOf(akeneoParentCategoryId) !== -1) {
                    configMatchingTopCatCodes.push(akeneoCategoryId);
                }
            }
        }
        Transaction.begin();
        categoryCustomObject.custom.categoriesCodes = JSON.stringify(configMatchingTopCatCodes);
        Transaction.commit();
    }
};

/* Clears the akeneo flux directory */
Utils.clearDirectoryAkeneo = function () {
    var File = require('dw/io/File');
    var AKENEO_FLUX_DIR = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'akeneo' + File.SEPARATOR;

    var AkeneoFluxPath = new File(AKENEO_FLUX_DIR);
    // filter on directory only
    var folderList = AkeneoFluxPath.listFiles(function (file) {
        return file.isDirectory();
    });
    var folderIterator = folderList.iterator();

    if (folderList && folderList.getLength() > 0) {
        while (folderIterator.hasNext()) {
            var folder = folderIterator.next();
            var AKENEO_ARCHIVE_DIRECTORY = folder.getFullPath() + File.SEPARATOR + 'archives' + File.SEPARATOR + 'failed' + File.SEPARATOR;
            var filesList = folder.listFiles(function (file) {
                return file.isFile();
            });
            var filesIterator = filesList.iterator();

            if (filesList && filesList.getLength() > 0) {
                while (filesIterator.hasNext()) {
                    var file = filesIterator.next();
                    file.renameTo(new File(AKENEO_ARCHIVE_DIRECTORY + file.getName()));
                }
            }
        }
    }
};

Utils.getJSONObjectFromSitePreferences = function (preferenceName) {
    var preferenceStr = Site.current.preferences ? Site.current.getCustomPreferenceValue(preferenceName) : '{}';
    var preferenceObj;
    try {
        preferenceObj = JSON.parse(preferenceStr || '{}');
    } catch (e) {
        var Logger = require('dw/system/Logger');
        Logger.error('ERROR: Failed to Parse site preference JSON: ' + preferenceName + ' Error: ' + e);
    }
    return preferenceObj;
};

/* Writes single XML element */
Utils.writeElement = function (xswHandle, elementName, chars, attrKey, attrValue) {
    xswHandle.writeStartElement(elementName);

    if (attrKey && attrValue) {
        xswHandle.writeAttribute(attrKey, attrValue);
    }
    if (typeof chars === 'string') {
        chars = chars.replace(/[^\x09\x0A\x0D\x20-\xFF\x85\xA0-\uD7FF\uE000-\uFDCF\uFDE0-\uFFFD]/g, ''); //eslint-disable-line
    }
    xswHandle.writeCharacters(chars);
    xswHandle.writeEndElement();
};

/* Configurations */
Utils.config = {
    serviceGeneralUrl: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoServiceGeneralUrl') : '',
    sfccMasterCatalogID: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductsCatalogID') : '',
    importType: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoImportType').value : 'advanced',
    imageType: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoImageType').value : '',
    topLevelCategoryID: Site.current.preferences ? Utils.getCategoryId(Site.current.getCustomPreferenceValue('akeneoTopLevelCategoryID')) : '',
    scope: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoScope') : '',
    imageVariationValue: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoImageVariationValue') : 'color',
    productSetFamily: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductSetFamily') : '',
    productBundleFamily: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductBundleFamily') : '',
    productSetAssociationType: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductSetAssociationType') : '',
    productBundleAssociationType: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductBundleAssociationType') : '',
    akeneoMainCatalogs: Site.current.preferences ? Array.prototype.slice.call(Site.current.getCustomPreferenceValue('akeneoMainCatalogs')).map(function (catalogId) { return Utils.getCategoryId(catalogId); }) : [],
    akeneoCategoryOnline: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoCategoryOnline') : true,
    productPrimaryFlag: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoProductPrimaryFlag') : true,
    modelImport: {
        type: Site.current.preferences ? Site.getCurrent().getCustomPreferenceValue('akeneoModelImportType').value : ''
    },
    productAssociationImportType: {
        type: Site.current.preferences ? Site.getCurrent().getCustomPreferenceValue('akeneoProductAssociation').value : ''
    },
    productsImportBuilderConfig: Utils.getJSONObjectFromSitePreferences('akeneoProductsImportBuilderConfig'),
    modelProductsImportBuilderConfig: Utils.getJSONObjectFromSitePreferences('akeneoModelProductsImportBuilderConfig'),
    systemAttrsMapping: Utils.getJSONObjectFromSitePreferences('akeneoProductAttrsMapping'),
    customAttrsMapping: Utils.getJSONObjectFromSitePreferences('akeneoCustomAttrMapping'),
    recommendationsMapping: Utils.getJSONObjectFromSitePreferences('akeneoRecommendationsMapping'),
    productLinkMapping: Utils.getJSONObjectFromSitePreferences('akeneoProductLinkMapping'),
    imageViewTypes: Utils.getJSONObjectFromSitePreferences('akeneoViewTypesConfig'),
    customObjectType: {
        RunTime: 'AkeneoRunTime',
        AccessToken: 'AkeneoToken',
        TopLevelCategoriesCodes: 'AkeneoTopLevelCategoriesCode'
    },
    cacheDirectory: {
        modelProducts: {
            baseLocation: '/model-products',
            endPoint: '/model-products/{0}'
        },
        familyVariants: {
            baseLocation: '/families',
            endPoint: '/families/{0}/variants/{1}'
        },
        attributes: {
            baseLocation: '/attributes',
            attributesList: '/attributes/attributesList',
            imageCodesList: '/attributes/imageCodesList',
            assetCodesList: '/attributes/assetCodesList'
        }
    },
    customLogFileName: 'akeneo',
    APIURL: {
        endpoints: {
            attributes: Site.current.preferences ? Site.current.getCustomPreferenceValue('akeneoAttributesUrl') : '/api/rest/v1/attributes',
            tokenAPIURL: '/api/oauth/v1/token',
            getMasterProduct: '/api/rest/v1/product-models/{0}',
            getVariationProduct: '/api/rest/v1/products/{0}',
            getAttributeFamily: '/api/rest/v1/families/{0}',
            getFamilyVariant: '/api/rest/v1/families/{0}/variants/{1}',
            ModelProductsUrl: '/api/rest/v1/product-models',
            ProductsUrl: '/api/rest/v1/products',
            AttributesUrl: '/api/rest/v1/attributes',
            CategoriesUrl: '/api/rest/v1/categories'
        },
        timeout: 600000,
        retryLimit: 5,
        parameter: {
            pagination: 100, // Number of items per page
            search: {
                updated: [{
                    operator: '>',
                    value: ''
                }]
            },
            paginationType: 'search_after'
        }
    },
    debug: {
        breakCodeOnLimit: false, // In debug mode keep it true if you want to run job for whole available data. In production this must be false
        pageLimit: 4 // Number of pages to be considered. This will be in action only when breakCodeOnLimit is true
    },
    removeCategoryPrefixes: removeCategoryPrefixes
};

module.exports = Utils;
