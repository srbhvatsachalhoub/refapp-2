'use strict';

var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var StringUtilsExt = require('~/cartridge/scripts/utils/libStringUtilsExt');
var customCacheMgr = require('~/cartridge/scripts/io/customCacheWebdav');
var writeCustomAttributes = require('~/cartridge/scripts/akeneoAttributes/writeCustomAttributes');

var config = generalUtils.config;


var productCustomAttributes = {};

/**
 * Calls price attribute writing logic
 * @param {*} attribute -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 */
function createPriceAttributes(attribute, xmlAttrKey, xswHandle) {
    writeCustomAttributes.writePriceAttribute(attribute, xmlAttrKey, xswHandle);
}

/**
 * Calls metric attribute writing logic
 * @param {*} attribute -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 */
function createMetricAttributes(attribute, xmlAttrKey, xswHandle) {
    writeCustomAttributes.writeMetricAttribute(attribute, xmlAttrKey, xswHandle);
}

/**
 * Calls general attribute writing logic
 * @param {*} attribute -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 * @param {*} parentCode -
 */
function createGeneralAttributes(attribute, xmlAttrKey, xswHandle, parentCode) {
    writeCustomAttributes.writeGeneralAttribute(attribute, xmlAttrKey, xswHandle, parentCode);
}

/**
 * Calls new data type attribute writing logic
 * @param {*} attribute -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 */
function createNewDataTypeAttrs(attribute, xmlAttrKey, xswHandle) {
    writeCustomAttributes.writeNewDataTypeAttribute(attribute, xmlAttrKey, xswHandle);
}

/**
 * Calls entity attribute writing logic
 * @param {*} attribute -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 * @param {*} parentCode -
 * @param {*} referenceDataName -
 */
function createEntityAttributes(attribute, xmlAttrKey, xswHandle, parentCode, referenceDataName) {
    writeCustomAttributes.writeCustomEntityAttributes(attribute, xmlAttrKey, xswHandle, parentCode, referenceDataName);
}

/**
 * Gets attribute definition from cache or API
 * @param {*} akeneoAttrCode -
 * @returns {Object} -
 */
function getAttributeDefinition(akeneoAttrCode) {
    var attrApiUrl = config.APIURL.endpoints.attributes + '/' + akeneoAttrCode;
    var attrDef = customCacheMgr.getCache(attrApiUrl);

    if (!attrDef) {
        var akeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
        var akeneoServices = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
        var akeneoService = akeneoServices.getGeneralService();
        var nextUrlBackup = akeneoServicesHandler.nextUrl;

        akeneoServicesHandler.nextUrl = '';

        try {
            attrDef = akeneoServicesHandler.serviceRequestAttribute(akeneoService, config.serviceGeneralUrl + attrApiUrl);
            customCacheMgr.setCache(attrApiUrl, attrDef);
        } catch (e) {
            throw new Error('ERROR : While calling service to get attribute definition with Error: ' + e.message);
        }

        akeneoServicesHandler.nextUrl = nextUrlBackup;
    }

    return attrDef;
}

/**
 * Call appropriate attribute write functions as per the attribute type
 * @param {*} attribute -
 * @param {*} akeneoAttrCode -
 * @param {*} xmlAttrKey -
 * @param {*} xswHandle -
 * @param {*} parentCode -
 */
function filterAttrOnType(attribute, akeneoAttrCode, xmlAttrKey, xswHandle, parentCode) {
    var attrDefinition = getAttributeDefinition(akeneoAttrCode);
    var attrType = attrDefinition.type || '';

    switch (attrType) {
        case 'pim_catalog_price_collection' :
            createPriceAttributes(attribute, xmlAttrKey, xswHandle);
            break;
        case 'pim_catalog_metric' :
            createMetricAttributes(attribute, xmlAttrKey, xswHandle);
            break;
        case 'pim_catalog_file' :
        case 'pim_catalog_image' :
        case 'pim_catalog_boolean' :
            createNewDataTypeAttrs(attribute, xmlAttrKey, xswHandle);
            break;
        case 'pim_assets_collection' :
        case 'pim_catalog_number' :
        case 'pim_catalog_date' :
        case 'pim_catalog_text' :
        case 'pim_catalog_textarea' :
        case 'pim_catalog_identifier' :
        case 'pim_catalog_simpleselect' :
        case 'pim_catalog_multiselect' :
        case 'pim_reference_data_multiselect' :
        case 'pim_reference_data_simpleselect' :
            createGeneralAttributes(attribute, xmlAttrKey, xswHandle, parentCode);
            break;
        case 'akeneo_reference_entity' :
        case 'akeneo_reference_entity_collection' :
            var referenceDataName = attrDefinition.reference_data_name;
            createEntityAttributes(attribute, xmlAttrKey, xswHandle, parentCode, referenceDataName);
            break;
        default :
            createNewDataTypeAttrs(attribute, xmlAttrKey, xswHandle);
    }
}

productCustomAttributes.getCustomAttributes = function (akeneoProduct, xswHandle) {
    var systemAttrsMapping = config.systemAttrsMapping;
    var customAttrsMapping = config.customAttrsMapping;

    if (akeneoProduct && akeneoProduct.values) {
        var parentCode = akeneoProduct.parent;
        xswHandle.writeStartElement('product');
        xswHandle.writeAttribute('product-id', (akeneoProduct.identifier ? akeneoProduct.identifier : akeneoProduct.code));

        xswHandle.writeStartElement('custom-attributes');

        Object.keys(akeneoProduct.values).forEach(function (attrCode) {
            var attribute = akeneoProduct.values[attrCode];
            var camelizeAttrCode;
            var xmlAttrKey;

            if (isNaN(attrCode)) {
                camelizeAttrCode = 'akeneo_' + StringUtilsExt.camelize(attrCode);
            }

            if (!(camelizeAttrCode in systemAttrsMapping.matching)) { // not in system attributes mappin
                xmlAttrKey = camelizeAttrCode in customAttrsMapping.matching ? customAttrsMapping.matching[camelizeAttrCode] : camelizeAttrCode;
                filterAttrOnType(attribute, attrCode, xmlAttrKey, xswHandle, parentCode);
            }
        });

        xswHandle.writeEndElement();// custom-attributes
        xswHandle.writeEndElement();// product
    }
};

module.exports = productCustomAttributes;
