'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    var $containers = $('.contentContainer');
    $containers.each(function () {
        var $elements = $(this).find('.experience-component');
        if ($(this).hasClass('flex-row')) {
            $('.experience-region').addClass('row');
            $elements.addClass('col-12 px-lg-3');
            if ($elements.length === 2) {
                $elements.addClass('col-xl-6');
            } if ($elements.length === 3) {
                $elements.addClass('col-xl-4');
            } if ($elements.length === 4) {
                $elements.addClass('col-xl-3');
            }
        }
    });
});

processInclude(module.exports);
