<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/paymentInstruments.js');
        assets.addCss('/css/account.css');
    </isscript>

    <isconfirm confirm_action="delete" confirm_type="payment" />
    <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>

    <div class="container pt-4">
        <iscontainertitle containertitle_value="${Resource.msg('label.payment.methods', 'account', null)}" />

        <div class="row">
            <div class="col-md-4 col-lg-3 hidden-sm-down">
                <isdashboardmenu dashboardmenu_activepage="paymentList" />
            </div>

            <div class="col-md-8 col-lg-9">
                <div class="dashboard-content">
                    <isnorecords
                        norecords_icon="sc-icon-payment-method"
                        norecords_visible="${!pdict.paymentInstruments || pdict.paymentInstruments.length === 0}"
                        norecords_message="${Resource.msg('payment.no.payment', 'account', null)}" />

                    <isif condition="${pdict.paymentInstruments && pdict.paymentInstruments.length > 0}">
                        <iscomment>Rows for Credit Cards</iscomment>
                        <div class="row">
                            <isloop items="${pdict.paymentInstruments}" var="paymentInstrument" status="loopState" >
                                <div class="col-12 js-payment-method-item" id="uuid-${paymentInstrument.UUID}">
                                    <div class="row box-delta">
                                        <div class="col-9">
                                            <div class="mb-1">
                                                <iscreditcard   creditcard_type="${paymentInstrument.creditCardType}"
                                                                creditcard_number="${paymentInstrument.maskedCreditCardNumber}" />
                                            </div>

                                            <div class="mb-1">
                                                <span>${paymentInstrument.creditCardHolder}</span>
                                            </div>

                                            <div class="mb-4 d-flex">
                                                <span>${Resource.msg('field.credit.card.expiry.date', 'creditCard', null)}</span><span>:</span>
                                                <span>&nbsp;</span>
                                                <span>
                                                    ${paymentInstrument.creditCardExpirationMonth}/
                                                    ${paymentInstrument.creditCardExpirationYear}
                                                </span>
                                            </div>

                                            <div>
                                                <a href="${'#/'}" class="radio-button-link selected js-text-default ${loopState.first === false ? 'd-none' : ''}">
                                                    <i></i>
                                                    <span>
                                                        ${Resource.msg('payment.default.payment', 'account', null)}
                                                    </span>
                                                </a>
                                                <isif condition="${loopState.first === false}">
                                                    <a href="${URLUtils.url('PaymentInstruments-SetDefault', 'uuid', paymentInstrument.UUID)}"
                                                        class="radio-button-link js-link-set-default">
                                                        <i></i>
                                                        <span>
                                                            ${Resource.msg('payment.set.as.default', 'account', null)}
                                                        </span>
                                                    </a>
                                                </isif>
                                            </div>
                                        </div>
                                        <div class="col-3 text-right">
                                            <button type="button"
                                                class="link js-button-delete-payment"
                                                data-toggle="modal"
                                                data-target="#deletePaymentModal"
                                                data-id="${paymentInstrument.UUID}"
                                                data-url="${pdict.actionUrl}"
                                                aria-label="${Resource.msg('label.payment.deletepayment','payment',null)}">
                                                ${Resource.msg('button.delete', 'common', null)}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </isloop>
                        </div>
                    </isif>
                </div>
            </div>
        </div>
    </div>
</isdecorate>
