<isdecorate template="common/layout/page">
    <isinclude template="/components/modules" sf-toolkit="off" />

    <isscript>
        var assets = require('*/cartridge/scripts/assets');
        assets.addJs('/js/productDetail.js');
        assets.addCss('/css/productDetail.css');
    </isscript>

    <isset name="product" value="${pdict.product}" scope="page" />
    <isset name="isQuickView" value="${false}" scope="page" />
    <isset name="isProductSet" value="${pdict.product.productType === 'set'}" scope="page" />
    <isobject object="${product.raw}" view="detail">
        <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>

        <iscomment>Banner</iscomment>
        <div class="pdp-banner hidden-lg-down">
            <isif condition="${product.backgroundImage}">
                <img src="${product.backgroundImage}" alt="${product.productName}" />
            </isif>
        </div>

        <iscomment>Details</iscomment>
        <div class="container product-detail product-wrapper js-product-details"
            data-pid="${product.id}">
            <div class="row">
                <div class="col-12 col-xl-4 hidden-lg-down">
                    <iscomment>Badges</iscomment>
                    <div class="badges-container">
                        <isset name="badges" value="${product.badges}" scope="page" />
                        <isinclude template="product/components/productTileBadges" />
                    </div>

                    <iscomment>Product Name</iscomment>
                    <div class="js-name-container">
                        <h1 class="product-name js-name">${product.productName}</h1>
                    </div>

                    <iscomment>Rating and Review Count</iscomment>
                    <div class="ratings-container">
                        <isif condition="${product.ratingSummary.ratingCount > 0}">
                            <isratingstar
                                ratingstar_show_summary="false"
                                ratingstar_count="${product.ratingSummary.ratingCount}"
                                ratingstar_value="${product.ratingSummary.averageRating}"
                                ratingstar_filled_class="sc-icon-star"
                                ratingstar_half_filled_class="sc-icon-star-fill-half"
                                ratingstar_empty_class="sc-icon-star-empty" />
                        </isif>
                    </div>

                    <iscomment>Short Description</iscomment>
                    <isif condition="${product.shortDescription}">
                        <p class="product-short-description js-short-description" itemprop="description">
                            <isprint value="${product.shortDescription}" encoding="off" />
                        </p>
                    </isif>

                    <iscomment>Sitckers Dektop</iscomment>
                    <iscustombadge custombadge_product="${product}" />
                </div>
                <div class="col-12 col-xl-4 px-xl-0">
                    <iscomment>Add to wishlist mobile</iscomment>
                    <div class="add-to-wishlist-mobile d-xl-none">
                        <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', product.id)}" />
                    </div>

                    <iscomment>Product Images Carousel</iscomment>
                    <isinclude template="product/components/imageCarousel" />

                    <iscomment>CTA Image</iscomment>
                    <div class="d-none js-image-container">
                        <isif condition="${product.images && product.images.large}">
                            <img src="${product.images.large[0].url}"
                                class="d-block img-fluid"
                                alt="${product.images.large[0].alt}"
                                itemprop="image" />
                        </isif>
                    </div>

                    <div class="d-xl-none">
                        <iscomment>Rating and Review Count</iscomment>
                        <div class="ratings-container">
                            <isif condition="${product.ratingSummary.ratingCount > 0}">
                                <isratingstar
                                    ratingstar_show_summary="false"
                                    ratingstar_count="${product.ratingSummary.ratingCount}"
                                    ratingstar_value="${product.ratingSummary.averageRating}"
                                    ratingstar_filled_class="sc-icon-star"
                                    ratingstar_half_filled_class="sc-icon-star-fill-half"
                                    ratingstar_empty_class="sc-icon-star-empty" />
                            </isif>
                        </div>

                        <iscomment>Badges</iscomment>
                        <div class="badges-container">
                            <isset name="badges" value="${product.badges}" scope="page" />
                            <isinclude template="product/components/productTileBadges" />
                        </div>

                        <h1 class="product-name" itemprop="name">${product.productName}</h1>

                        <isif condition="${product.shortDescription}">
                            <p class="product-short-description" itemprop="description">
                                <isprint value="${product.shortDescription}" encoding="off" />
                            </p>
                        </isif>

                        <iscomment>Refill Mobile</iscomment>
                        <div class="d-xl-none text-center">
                            <div class="d-inline-block">
                                <isinclude template="product/components/refill" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-4">
                    <div class="product-info frame hidden-frame-lg-down js-product-info">
                        <span class="product-id d-none" itemprop="productID">${product.id}</span>

                        <div class="js-main-attributes-container">
                            <isinclude template="product/components/mainAttributes" />
                        </div>

                        <iscomment>Quantity</iscomment>
                        <isset name="isBundle" value="${false}" scope="page" />
                        <div class="attributes-container js-attributes-container">
                            <isloop items="${product.variationAttributes}" var="attr" status="attributeStatus">
                                <div data-attr="${attr.id}">
                                    <isinclude template="product/components/variationAttribute" />
                                </div>
                                <isif condition="${attributeStatus.last && !isBundle && product.options.length === 0}">
                                    <div class="quantity">
                                        <isinclude template="product/components/quantity" />
                                    </div>
                                </isif>
                            </isloop>

                            <isif condition="${!product.variationAttributes && product.options.length === 0}">
                                <div class="simple-quantity">
                                    <isinclude template="product/components/quantity" />
                                </div>
                            </isif>
                        </div>

                        <iscomment>Options</iscomment>
                        <isif condition="${product.options && product.options.length > 0}">
                            <isinclude template="product/components/options" />
                        </isif>

                        <div class="js-price-container">
                            <div class="prices-add-to-cart-actions">
                                <iscomment>Prices</iscomment>
                                <div class="prices">
                                    <isset name="price" value="${product.price}" scope="page" />
                                    <isinclude template="product/components/pricing/main" />
                                </div>
                            </div>
                        </div>

                        <iscontentasset aid="pdp-below-price-static" />

                        <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled') && product.isGiftCard}">
                            <iscomment>Gift Card Product Form</iscomment>
                            <isinclude template="giftCard/product/giftCardProductForm" />
                        </isif>

                       <isif condition="${ !dw.system.Site.getCurrent().getCustomPreferenceValue('disableCheckout') }">
                            <div class="js-add-to-cart-btn-container">
                                <iscomment>Product Availability</iscomment>
                                <isinclude template="product/components/productAvailability" />

                                <iscomment>Back In Stock</iscomment>
                                <isinclude url="${URLUtils.url('BackInStock-Start', 'pid', product.id)}" />

                                <iscomment>Cart and [Optionally] Apple Pay</iscomment>
                                <isinclude template="product/components/addToCartProduct" />
                            </div>
                        </isif>
                        <iscomment>Applicable Promotions</iscomment>
                        <div class="promotions js-promotions">
                            <isinclude template="product/components/promotions" />
                        </div>

                        <iscomment>Social Sharing Icons</iscomment>
                        <isinclude template="product/components/socialIcons" />

                        <iscomment>Manufacturer Info</iscomment>
                        <isinclude template="product/components/manufacturerInfo" />

                        <iscomment>Size Chart</iscomment>
                        <isif condition="${product.variationAttributes && product.sizeChartId}">
                            <div class="size-chart">
                                <isinclude template="product/components/sizeChart" />
                            </div>
                        </isif>

                        <iscomment>Shipping Information</iscomment>
                        <isinclude template="product/components/shippingInfo" />

                        <iscomment>Add to wishlist desktop</iscomment>
                        <div class="d-none d-xl-block">
                            <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', product.id)}" />
                        </div>

                        <iscomment>Refill Desktop</iscomment>
                        <div class="hidden-lg-down">
                            <isinclude template="product/components/refill" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <isinclude template="product/components/descriptionAndDetails" />

        <isset name="rrTemplateHelper" value="${require('*/cartridge/scripts/helpers/rrTemplateHelper')}" scope="page"/>
        <isif condition="${rrTemplateHelper.ratingsReviewsEnabled()}">
            <isinclude template="product/components/ratingsReviews/ratingsReviews" />
        </isif>

        <isinclude template="product/components/recommendationsComplementary" />

        <isinclude template="product/components/recommendationsAlternative" />

        <div class="recommendations">
            <isslot id="product-recommendations-m" description="Recommended products" context="global" />
        </div>

        <isinclude template="product/components/schemaorg" />
    </isobject>
</isdecorate>
