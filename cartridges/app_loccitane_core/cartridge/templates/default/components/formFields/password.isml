<div class="form-group position-relative
    ${pdict.inputpassword_formfield.mandatory === true ? 'required' : ''}
    ${pdict.inputpassword_formfield.htmlName}">
    <label class="form-control-label" for="${'id_' + pdict.inputpassword_formfield.htmlName}">
        ${pdict.inputpassword_label || pdict.inputpassword_formfield.label}
    </label>
    <input type="password"
        autocomplete="off"
        class="form-control js-input-password ${pdict.inputpassword_class || ''}"
        id="${'id_' + pdict.inputpassword_formfield.htmlName}"
        <isif condition="${pdict.inputpassword_constraints}">
            data-constraint-min-length="${pdict.inputpassword_constraints.minLength}"
            data-constraint-min-special-chars="${pdict.inputpassword_constraints.minSpecialChars}"
            data-constraint-is-force-mixed-case="${pdict.inputpassword_constraints.isForceMixedCase}"
            data-constraint-is-force-letters="${pdict.inputpassword_constraints.isForceLetters}"
            data-constraint-is-force-numbers="${pdict.inputpassword_constraints.isForceNumbers}"
        </isif>
        data-missing-error="<isprint value="${pdict.inputpassword_missingerror || Resource.msg('error.message.required.password', 'forms', null)}" encoding="htmldoublequote" />"
        data-range-error="<isprint value="${pdict.inputpassword_rangeerror || Resource.msg('error.message.range.password', 'forms', null)}" encoding="htmldoublequote" />"
        <isif condition="${pdict.inputpassword_mismatchid}">
            data-mismatch-field-id="${pdict.inputpassword_mismatchid}"
            data-mismatch-error="<isprint value="${Resource.msg('error.message.mismatch.password', 'forms', null)}" encoding="htmldoublequote" />"
        </isif>
        <isprint value="${pdict.inputpassword_disabled ? 'disabled' : ''}" encoding="off" />
        <isprint value="${pdict.inputpassword_value ? 'value="' + pdict.inputpassword_value + '"' : ''}" encoding="off" />
        <isprint value="${pdict.inputpassword_placeholder ? 'placeholder="' + pdict.inputpassword_placeholder + '"' : ''}" encoding="off" />
        <isprint value="${pdict.inputpassword_formfield.attributes}" encoding="off" /> />
    <span class="password-show-toggle js-password-show-toggle">
        <i class="sc-icon-eye"></i>
    </span>
    <div class="invalid-feedback">${pdict.inputpassword_formfield.error || ''}</div>
    <isif condition="${pdict.inputpassword_constraints}">
        <div class="password-feedback d-flex flex-wrap">
            <isif condition="${pdict.inputpassword_constraints.minLength > 0}">
                <div class="js-validate-password-min-length d-none">
                    <div class="d-flex align-items-center">
                        <i class="sc-icon-checkbox-v d-none js-validate-password-success"></i>
                        <i class="sc-icon-cross d-none js-validate-password-error"></i>
                        ${Resource.msgf('label.input.password.minlength', 'forms', null, pdict.inputpassword_constraints.minLength)}
                    </div>
                </div>
            </isif>
            <isif condition="${pdict.inputpassword_constraints.minSpecialChars > 0}">
                <div class="js-validate-password-min-special-chars d-none">
                    <div class="d-flex align-items-center">
                        <i class="sc-icon-checkbox-v d-none js-validate-password-success"></i>
                        <i class="sc-icon-cross d-none js-validate-password-error"></i>
                        ${Resource.msgf('label.input.password.minspecialchars', 'forms', null, pdict.inputpassword_constraints.minSpecialChars)}
                    </div>
                </div>
            </isif>
            <isif condition="${pdict.inputpassword_constraints.isForceMixedCase}">
                <div class="js-validate-password-is-force-mixed-case d-none">
                    <div class="d-flex align-items-center">
                        <i class="sc-icon-checkbox-v d-none js-validate-password-success"></i>
                        <i class="sc-icon-cross d-none js-validate-password-error"></i>
                        ${Resource.msg('label.input.password.isforcemixedcase', 'forms', null)}
                    </div>
                </div>
            </isif>
            <isif condition="${pdict.inputpassword_constraints.isForceLetters}">
                <div class="js-validate-password-is-force-letters d-none">
                    <div class="d-flex align-items-center">
                        <i class="sc-icon-checkbox-v d-none js-validate-password-success"></i>
                        <i class="sc-icon-cross d-none js-validate-password-error"></i>
                        ${Resource.msg('label.input.password.isforceletters', 'forms', null)}
                    </div>
                </div>
            </isif>
            <isif condition="${pdict.inputpassword_constraints.isForceNumbers}">
                <div class="js-validate-password-is-force-numbers d-none">
                    <div class="d-flex align-items-center">
                        <i class="sc-icon-checkbox-v d-none js-validate-password-success"></i>
                        <i class="sc-icon-cross d-none js-validate-password-error"></i>
                        ${Resource.msg('label.input.password.isforcenumbers', 'forms', null)}
                    </div>
                </div>
            </isif>
        </div>
    </isif>
</div>
