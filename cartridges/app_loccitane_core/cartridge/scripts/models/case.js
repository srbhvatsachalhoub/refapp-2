'use strict';
/* global empty, customer, request */
/**
 * @module models/case
 */

/**
 * Case class
 * @param {Object} caseObj object
 * @constructor
 * @alias module:models/caseObj~Case
 */
function Case(caseObj) {
    if (empty(caseObj)) {
        throw new Error('Submitted form object is empty.');
    }

    /**
     * @type {string} The object type represented in Service Cloud
     */
    this.type = 'Case';

    // use 'base' for SFRA, if it does not exist fall back to 'object' for SiteGenesis
    this.data = caseObj.base || caseObj.object;
    /**
     * @type {dw.customer.Customer}
     */
    this.customer = caseObj.customer || customer;
    /**
     * @type {dw.customer.Profile}
     */
    this.profile = !empty(this.customer.profile) ? this.customer.profile : {};
}

/**
 * @alias module:models/case~Case#prototype
 */
Case.prototype = {
    /**
     * Builds up a formatted object for JSON.stringify()
     * @returns {Object} case object
     */
    toJSON: function () {
        var StringUtils = require('dw/util/StringUtils');
        var Locale = require('dw/util/Locale');
        var Resource = require('dw/web/Resource');
        var customerLoc = Locale.getLocale(((request && request.locale) ? request.locale : 'AE'));
        var market = Resource.msg(StringUtils.format('case.country.{0}', customerLoc.country), 'casemapping', null);
        var toJSON = {
            Reason: this.data.reason.value,
            Origin: 'Web',
            SuppliedName: StringUtils.format('{0} {1}', this.data.firstname.value, this.data.lastname.value),
            first_name: this.data.firstname.value,
            last_name: this.data.lastname.value,
            email: this.data.email.value,
            SuppliedEmail: this.data.email.value,
            SuppliedPhone: this.data.phone.value,
            Subject: this.data.title.value,
            Description: this.data.comment.value,
            SFCC_Order_No__c: this.data.ordernumber.value,
            Brand__c: 'L’Occitane',
            Channel__c: 'ECommerce',
            Market__c: market
        };

        if (this.customer.authenticated && !empty(this.profile)) {
            toJSON.contact_id = this.profile.custom.sscid;
            toJSON.SFCC_Customer_Id__c = this.profile.getCustomer().getID();
            toJSON.SFCC_Customer_Number__c = this.profile.getCustomerNo();
        }

        return toJSON;
    }
};

module.exports = Case;
