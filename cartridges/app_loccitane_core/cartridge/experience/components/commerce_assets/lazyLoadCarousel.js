'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.lazyLoadCarousel layout.
 * @param {dw.experience.ComponentScriptContext} context The component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();

    // general config
    model.pids = content.pids;
    model.fullWidthFromTablet = content.fullWidthFromTablet || null;
    model.header = content.header || null;

    // carousel config
    var config = context.content.jsonConfig;

    model.cssSlideClass = config.cssSlideClass || '';
    model.initialCount = content.pids ? content.pids.split(',').length : '';
    model.slidesToShow = config.slidesToShow || '';
    model.carouselCenterMode = config.carouselCenterMode || '';
    model.carouselCenterPadding = config.carouselCenterPadding || '';
    model.itemtype = config.itemtype || '';
    model.itemid = config.itemid || '';
    return new Template('experience/components/commerce_layouts/lazyLoadCarousel').render(model).text;
};
