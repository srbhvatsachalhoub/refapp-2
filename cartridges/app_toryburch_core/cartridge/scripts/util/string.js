'use strict';

/**
 * Capitalization of the first letter of words
 * @param {string} str - target string
 * @returns {string} string with capitalized words
 */
function capitalize(str) {
    var stringAsArray = str.split(' ');

    var resultString = stringAsArray.map(function (word) {
        return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    });

    return resultString.join(' ');
}

module.exports = {
    capitalize: capitalize
};
