'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * To select current option for title
 * @param {Object} titleList profile object
 * @param {string} titleValue current user title
 */
function normalizeSelectedOption(titleList, titleValue) {
    titleList.options.forEach(function (option) {
        if (option.value === titleValue) {
            Object.defineProperty(option, 'selected', {
                enumerable: true,
                writable: true,
                value: true
            });
        }
    });
}

server.append(
    'EditProfile',
    server.middleware.https,
    csrfProtection.generateToken,
    userLoggedIn.validateLoggedIn,
    consentTracking.consent,
    function (req, res, next) {
        var profileForm = server.forms.getForm('profile');
        var titleList = profileForm.title.titleList;
        var titleValue = req.currentCustomer.raw.profile.title;
        titleList.value = titleValue;

        normalizeSelectedOption(titleList, titleValue);
        next();
    }
);

/**
 * Extended to check if the given email has the account
 * Returns success if no account found from app_storefront_base
 */
server.append('PasswordResetDialogForm', server.middleware.https, function (req, res, next) {
    var viewData = res.getViewData();
    if (!viewData.success) {
        var rurl = req.querystring.rurl || 1;
        var link = renderTemplateHelper.getRenderedHtml({ rurl: rurl }, 'account/components/invalidFeedbackLink');

        viewData.fields.loginEmail = Resource.msgf('error.message.login.form', 'login', null, link);
    }
    next();
});

module.exports = server.exports();
