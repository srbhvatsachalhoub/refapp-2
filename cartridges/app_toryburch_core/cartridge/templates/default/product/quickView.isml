<isset name="productCopy" value="${pdict.product}" scope="page"/>
<isset name="product" value="${pdict.product}" scope="page"/>
<isset name="isBundle" value="${false}" scope="page"/>
<isset name="isQuickView" value="${true}" scope="page"/>
<isset name="isProductSet" value="${false}" scope="page" />
<isset name="loopState" value="{count: 1}" scope="page" />
<div class="product-quickview product-${product.productType} col product-wrapper product-detail"
    data-pid="${product.id}">
    <div class="row">
        <iscomment>Product Image</iscomment>
        <isset name="image" value="${product.images.large[0]}" scope="page" />
        <div class="product-image col-sm-6">
            <img src="${image.url}" class="d-block img-fluid" alt="${image.alt}" title="${image.title}" itemprop="image" />
        </div>


        <div class="col-sm-6">
            <iscomment>Product Name and Number</iscomment>
            <div class="row">
                <div class="col-12">
                    <section>
                        <h1 class="product-name">${product.productName}</h1>
                    </section>
                </div>
                <iscomment>Prices</iscomment>
                <div class="prices col-12">
                    <isset name="price" value="${product.price}" scope="page" />
                    <isinclude template="product/components/pricing/main" />
                </div>
                <div class="cart-product col">
                    <isif condition="${(product.variationAttributes && product.variationAttributes.length > 0) || (product.options && product.options.length > 0)}">
                        <isloop items="${product.variationAttributes}" var="attribute">
                            <div class="cart-product-attribute d-flex">
                                <div class="cart-product-attribute-title">${attribute.displayName}:</div>
                                <p class="line-item-attributes ${attribute.displayName}-${product.UUID}">
                                    ${attribute.selectedValue.displayValue}
                                </p>
                            </div>
                        </isloop>
                        <isloop items="${product.options}" var="option">
                            <isif condition="${!!option}">
                                <div class="cart-product-attribute">
                                    <div class="lineItem-options-values" data-option-id="${option.optionId}"
                                        data-value-id="${option.selectedValueId}">
                                        <p class="line-item-attributes">${option.displayName}</p>
                                    </div>
                                </div>
                            </isif>
                        </isloop>
                    </isif>
                </div>
            </div>

            <div class="detail-panel product-info-cart">
                <iscomment>Attributes</iscomment>
                <section class="attributes">
                    <isinclude template="product/components/mainAttributes" />
                    <isloop items="${product.variationAttributes}" var="attr" status="attributeStatus">
                        <div data-attr="${attr.id}" class="swatch row">
                            <div class="col-12">
                                <isinclude template="product/components/variationAttribute" />
                            </div>

                            <isif condition="${attributeStatus.last}">
                                <iscomment>Quantity Drop Down Menu</iscomment>
                                <div class="attribute quantity px-3 col-12 d-sm-none">
                                    <isif condition="${pdict.addToCartUrl || pdict.updateCartUrl}">
                                        <isinclude template="product/components/quantity" />
                                    </isif>
                                </div>
                            </isif>
                        </div>
                    </isloop>

                    <div class="badges-container">
                        <isset name="badges" value="${product.badges}" scope="page" />
                        <isinclude template="product/components/productTileBadges" />
                    </div>

                    <div class="d-sm-none row availability align-self-end " data-ready-to-order="${product.readyToOrder}" data-available="${product.available}">
                        <isinclude template="product/components/availability" />
                    </div>

                    <iscomment>Applicable Promotions</iscomment>
                    <div class="row">
                        <div class="col-12 promotions">
                            <div class="align-self-center">
                                <isinclude template="product/components/promotions" />
                            </div>
                        </div>
                    </div>

                    <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled') && product.isGiftCard}">
                        <iscomment>Gift Card Product Form</iscomment>
                        <div class="row">
                            <div class="col-12">
                                <isinclude template="giftCard/product/giftCardProductForm" />
                            </div>
                        </div>
                    </isif>
                </section>

                <isif condition="${product.productType !== 'bundle'}">
                    <iscomment>Options</iscomment>
                    <isif condition="${product.options && product.options.length > 0}">
                        <isinclude template="product/components/options" />
                    </isif>
                </isif>
            </div>

            <div class="col-xs-5 mx-auto">
            <iscomment>Cart and [Optionally] Apple Pay</iscomment>
                <isif condition="${pdict.addToCartUrl}">
                    <isinclude template="product/components/addToCartGlobal" />
                <iselse/>
                    <isinclude template="product/components/updateProduct" />
                </isif>
            </div>
            
            <div class="quantity-selector-wrapper">
                <iscomment>Quantity</iscomment>
                <div class="hidden-xs-down">
                    <isif condition="${pdict.addToCartUrl || pdict.updateCartUrl}">
                        <isinclude template="product/components/quantity" />
                    </isif>
                </div>
            </div>

            <div class="hidden-xs-down availability align-self-end global-availability"
                data-ready-to-order="${product.readyToOrder}" data-available="${product.available}">
                <div class="row">
                    <isinclude template="product/components/availability" />
                </div>
            </div>
        </div>
    </div>

    <isif condition="${product.productType === 'bundle'}">
        <div class="hidden-xs-down">
            <hr />
        </div>
        <isinclude template="product/components/bundleItems" />
        <hr />
        <iscomment>Quantity Drop Down Menu</iscomment>
        <div class="row d-sm-none">
            <div class="quantity col-10 mx-auto">
                <isinclude template="product/components/quantity" />
            </div>
        </div>

        <iscomment>Availability</iscomment>
        <div class="row d-sm-none">
            <div class="col-11 mx-auto availability" data-ready-to-order="${product.readyToOrder}"
                data-available="${product.available}">
                <isinclude template="product/components/availability" />
            </div>
        </div>
    </isif>

    <isset name="product" value="${productCopy}" scope="page" />
</div>
<div class="modal-footer row align-items-end d-none">
    <isset name="loopState" value="{count: 1}" scope="page" />
</div>