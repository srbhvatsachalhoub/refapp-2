'use strict';

var MasterAttributesModel = require('*/cartridge/models/product/masterAttributes');

module.exports = function (object, product) {
    Object.defineProperty(object, 'masterProduct', {
        enumerable: true,
        writable: true,
        value: new MasterAttributesModel(product)
    });
};
