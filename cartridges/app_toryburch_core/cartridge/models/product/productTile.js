'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType, options) {
    decorators.base(product, apiProduct, productType);
    decorators.backInStock(product, apiProduct);
    decorators.sellable(product, apiProduct);
    decorators.availability(
        product,
        options.quantity,
        apiProduct.minOrderQuantity.value,
        inventoryHelpers.getProductAvailabilityModel(apiProduct)
    );

    decorators.quantity(product, apiProduct, 1);

    if (apiProduct.isMaster()) {
        decorators.images(product, options.variationModel.selectedVariants[0], { types: ['medium'], quantity: 'all' });
    } else {
        decorators.images(product, apiProduct, { types: ['medium'], quantity: 'all' });
    }

    decorators.price(product, apiProduct, options.promotions, false, options.optionModel);
    decorators.badges(product, apiProduct, options.promotions);

    if (productType === 'set') {
        decorators.setProductsCollection(product, apiProduct);
    }

    decorators.customBadge(product, apiProduct);
    decorators.variationAttributes(product, options.variationModel, {
        attributes: '*',
        endPoint: 'Variation'
    });
    decorators.category(product, apiProduct, productType);
    decorators.custom(product, apiProduct);

    return product;
};
