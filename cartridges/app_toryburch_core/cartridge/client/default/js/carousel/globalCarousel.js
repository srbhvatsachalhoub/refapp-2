'use strict';
var globalSwatchesOptions = require('./../config/swatchesCarousel.json').options;
var globalRecommendationsOptions = require('./../config/recommendationsCarousel.json').options;
var carouselSelector = '.js-global-carousel';

require('jquery.waitforimages');

/**
 * Gets scrollbar width
 * @param {jQuery} $element - $element
 * @returns {Object} options
 */
function getOption($element) {
    var options = null;
    var width = $(window).width();
    var selector = 'carouselOptionsMobile';
    if (width >= window.RA_BREAKPOINTS.md) {
        selector = 'carouselOptionsTablet';
    }
    if (width >= window.RA_BREAKPOINTS.lg) {
        selector = 'carouselOptions';
    }
    if ($element.hasClass('js-recommendations-carousel')) {
        options = globalRecommendationsOptions[selector];
        // config for recommendations is global, it is processed as const config from .json
    }
    if ($element.hasClass('js-swatches-carousel')) {
        options = globalSwatchesOptions[selector];
        // config for swatches is global, it is processed as const config from .json
    }
    if (!options) {
        // if config depends on screen width it is processed via data attr, should be seated in .isml
        options = $element.data(selector);
    }
    return options || {};
}

/**
 * @description Inits one carousel instance
 * @param {jQuery} $carousel, carousel jQuery object
 */
function initCarousel($carousel) {
    var options = getOption($carousel);
    if ($carousel.find('img').length) {
        $carousel.waitForImages().done(function () {
            if ($carousel[0].slick) {
                $carousel.slick('unslick');
            } else {
                $carousel.removeClass('slick-initialized');
            }
            $carousel.slick(options);
        });
    } else {
        $carousel.slick(options);
    }
}

/**
* init all carousels
* @param {jQuery} $container - $element
*/
function initAllCarousels($container) {
    $($container || carouselSelector).each(function () {
        initCarousel($(this));
    });
}

/**
* init events for carousels
* @param {jQuery} $element - $element
*/
function initEvents() {
    $(window).on('resize orientationchange', function () {
        initAllCarousels();
    });

    // init carousel for new elements, after update DOM on PLP
    $(document).on('search:success', function () {
        $(carouselSelector).not('.slick-initialized').each(function () {
            initAllCarousels($(this));
        });
    });

    $(document).on('minicart:popover', function () {
        initAllCarousels($('.js-minicart-carousel'));
    });

    $(document).on('carousel:reinit', function (event, data) {
        initAllCarousels(data.$container);
    });
}

/**
 * init carousel
 */
function init() {
    $(window).on('load', function () {
        initAllCarousels();
        initEvents();
    });
}

module.exports = {
    init: init
};
