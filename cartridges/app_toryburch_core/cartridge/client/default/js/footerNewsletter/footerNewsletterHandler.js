'use strict';

var formValidation = require('base/components/formValidation');

/**
 * @typedef {Object} QuickViewHtml
 * @property {string} body - Main Quick View body
 */

/**
 * Parse HTML code in Ajax response
 * @param {string} html - Rendered HTML from quickview template
 * @return {QuickViewHtml} - QuickView content components
 */
function parseHtml(html) {
    var $html = $('<div>').append($.parseHTML(html));

    var body = $html.find('.js-register-container');

    return { body: body };
}

/**
 * replaces the content in the modal window on for the selected product variation.
 * @param {string} selectedValueUrl - url to be used to retrieve a new product model
 */
function fillModalElement(selectedValueUrl) {
    $('.body').spinner().start();
    var $form = $('.subscribe-email-form');

    $.ajax({
        url: selectedValueUrl,
        method: 'GET',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            var parsedHtml = parseHtml(data.renderedTemplate);
            var $subscribeModal = $('#subscribeModalForm');

            var $modalBody = $subscribeModal.find('.modal-body');

            $modalBody.empty();
            $modalBody.html(parsedHtml.body);

            var $modalHeader = $modalBody.find('.close .sr-only');
            $modalHeader.text(data.closeButtonText);

            $('body').addClass('subscribe-modal-open');

            $subscribeModal.modal('show');
        },
        error: function () {
            $.spinner().stop();
        }
    });
}

/**
 * Handle subscribe modal close events
 */
function handleSubscribeModalClose() {
    var $body = $('body');

    $body.on('click', '.js-subscribe-close', function () {
        $body.removeClass('subscribe-modal-open');
    });
}

/**
 * Handle footer news letter
 */
function handleFooterNewsletter() {
    $('body').on('click', '.js-footer-newsletter-btn', function (e) {
        e.preventDefault();
        var $this = $('#newsletterEmail');

        setTimeout(function () {
            if (!$this.hasClass('is-invalid')) {
                var selectedValueUrl = $this.closest('form').attr('action');
                fillModalElement(selectedValueUrl);
            }
        }, 500);
    });
}

/**
 * Handle subscribe image show
 */
function handleSubscribeImageShow() {
    $(document).ready(function () {
        var $section = $('.home-subscribe-section');
        if ($section.length) {
            var bg = $('#subscribeBackgroundImage').val();
            if (bg.length) {
                $section.css('background-image', 'url(' + bg + ')');
            }
        }
    });
}

/**
 * Handle subscribe event
 */
function handleSubscribeEvent() {
    $('body').on('click', '.js-subbscribe-modal-button', function (e) {
        var $form = $('.subscribe-newsletter-form');
        var $subscribeModal = $('#subscribeModalForm');
        e.preventDefault();
        var url = $form.attr('action');
        $('.js-error, .js-success', $form).addClass('d-none');
        $form.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                $form.spinner().stop();
                if (!data.success) {
                    if (data.fieldErrors) {
                        formValidation($form, data);
                    } else {
                        $('.js-error', $form).removeClass('d-none');
                        $('.js-error').fadeIn('slow');
                    }
                } else {
                    if (data.redirectUrl) { //eslint-disable-line
                        window.location = data.redirectUrl;
                    } else {
                        $('.js-success', $form).removeClass('d-none');
                        $('.js-success').fadeIn('slow');
                        setTimeout(function () {
                            $subscribeModal.modal('hide');
                        }, 2000);
                    }
                }
            },
            error: function () {
                $('.js-error', $form).removeClass('d-none');
                $form.spinner().stop();
            }
        });
        setTimeout(function () {
            $('.js-error,.js-success').fadeOut('slow');
        }, 8000);

        return false;
    });
}

module.exports = {
    handleFooterNewsletter: handleFooterNewsletter,
    handleSubscribeModalClose: handleSubscribeModalClose,
    handleSubscribeImageShow: handleSubscribeImageShow,
    handleSubscribeEvent: handleSubscribeEvent
};
