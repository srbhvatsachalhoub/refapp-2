'use strict';
var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./checkout/checkout'));
    processInclude(require('./checkout/billingCustom'));

    if (window.dw &&
        window.dw.applepay &&
        window.ApplePaySession &&
        window.ApplePaySession.canMakePayments()) {
        $('body').addClass('apple-pay-enabled');
    }

    processInclude(require('brand_core/checkout/smsVerification'));
    processInclude(require('brand_core/checkout/giftWrapper'));
});
