'use strict';

var throttle = require('lodash/throttle');
var base = require('brand_core/product/detail');
var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var nextArrowVertical = require('../templates/slickArrowNextVertical');
var prevArrowVertical = require('../templates/slickArrowPrevVertical');
var carousel = require('brand_core/components/carousel');
var quantitySelector = require('./quantitySelector');
var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');
var latestKnownScrollY = 0;

/**
 * Initializes product image carousel
 */
function initProductImageCarousel() {
    var selImageCarousel = '.js-pdp-product-image-carousel';
    var selThumbnailCarousel = '.js-pdp-product-thumbnail-carousel';
    var $imageCarousel = $(selImageCarousel);
    var $thumbnailCarousel = $(selThumbnailCarousel);
    var thumbnailCount = $thumbnailCarousel.data('count');
    var isVertical = $thumbnailCarousel.data('configVertical');
    var thumbnailSlideLimit = $thumbnailCarousel.data('count');
    var verticalThumbnailsSlidesToShow = thumbnailCount > thumbnailSlideLimit ? thumbnailSlideLimit : thumbnailCount;
    var width = $(window).width();

    carousel($imageCarousel, {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        autoplay: false,
        asNavFor: selThumbnailCarousel,
        nextArrow: nextArrow,
        prevArrow: prevArrow
    });

    carousel($thumbnailCarousel, {
        infinite: false,
        vertical: isVertical ? commonHelpers.isDesktop(width, window.RA_BREAKPOINTS) : false,
        variableWidth: false,
        centerMode: isVertical ? thumbnailCount > thumbnailSlideLimit : false,
        slidesToScroll: thumbnailCount > thumbnailSlideLimit ? 1 : thumbnailCount,
        slidesToShow: isVertical ? verticalThumbnailsSlidesToShow : 3,
        arrows: true,
        focusOnSelect: true,
        centerPadding: '0rem',
        asNavFor: selImageCarousel,
        nextArrow: isVertical ? nextArrowVertical : nextArrow,
        prevArrow: isVertical ? prevArrowVertical : prevArrow
    });
}

/**
 * Updates poduct id on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateProductIdAfterAttributeSelect(product, $productContainer) {
    $('.js-product-cta-placeholder').parent()
        .data('pid', $productContainer.data('pid'));
}

/**
 * Updates carousel images on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $container - product detail section element that contains carousel
 */
function updateCarouselAfterAttributeSelect(product, $container) {
    var largeImageUrls = product.images.large;
    var smallImageUrls = product.images.small;
    var $imageCarousel = $container.find('.js-pdp-product-image-carousel');
    var $thumbnailCarousel = $container.find('.js-pdp-product-thumbnail-carousel');
    var $imageRef = $imageCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $thumbnailRef = $thumbnailCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $productCtaImage = $container.find('.js-product-cta-image-ph img');
    var imagesFragment = document.createDocumentFragment();
    var thumbnailsFragment = document.createDocumentFragment();

    // Prepare new carousel images
    if (largeImageUrls) {
        largeImageUrls.forEach(function (image) {
            var $image = $imageRef.clone();

            if ($image.length === 0) return;

            $image.attr('src', image.url);
            $image.attr('alt', image.alt);

            // Append new image to fragment
            imagesFragment.appendChild($image.get(0));
        });
    }

    // Prepare new navigation thumbnails
    if (smallImageUrls && $thumbnailRef.length) {
        smallImageUrls.forEach(function (image) {
            thumbnailsFragment.appendChild(
                $thumbnailRef.clone()
                    .attr('src', image.url)
                    .attr('alt', image.alt)
                    .get(0)
            );
        });
    }

    // Refresh image carousel content
    $imageCarousel
        .slick('unslick')
        .html(imagesFragment);

    // Refresh thumbnail carousel content
    $thumbnailCarousel
        .data('count', (thumbnailsFragment.children || thumbnailsFragment.childNodes).length + '.0')
        .slick('unslick')
        .html(thumbnailsFragment);

    // Updata cta image
    if (largeImageUrls) {
        $productCtaImage.attr('src', largeImageUrls[0].url);
        $productCtaImage.attr('alt', largeImageUrls[0].alt);
    }

    // Reinitialize carousel
    initProductImageCarousel();
}

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {string} attr.values.displayValue - Attribute display value
 * @param {Object} attr.values.images - Attribute images
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processSwatchValues(attr, $productContainer) {
    var $selectedSwatch = $productContainer.find('.js-selected-swatch');
    var $selectedSwatchName = $productContainer.find('.js-selected-swatch-name');

    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer
            .find('[data-attr="' + attr.id + '"] [data-attr-value="' + attrValue.value + '"]');
        var $swatchAnchor = $attrValue.parent();

        $attrValue.removeClass('selectable unselectable selected');
        $attrValue.addClass(attrValue.selectable ? 'selectable' : 'unselectable');
        $swatchAnchor.attr('href', 'javascript:void(0)'); //eslint-disable-line

        if (attrValue.selected) {
            $attrValue.addClass('selected');
            $selectedSwatch.removeClass();
            $selectedSwatch.attr('data-attr-value', attrValue.value);
            $selectedSwatch.addClass(attr.id + '-value swatch-circle swatch-value js-selected-swatch');
            $selectedSwatch.css('background-image', 'url("' + attrValue.images.swatch[0].url + '")');
            $selectedSwatchName.html(attrValue.displayValue);
        } else if (attrValue.url) {
            $swatchAnchor.attr('href', attrValue.url);
        }
    });
}

/**
 * Process attribute values associated with an attribute that does not have image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processNonSwatchValues(attr, $productContainer) {
    var $select = $productContainer
        .find('[data-attr="' + attr.id + '"] select.select-' + attr.id);

    // Update default option's url
    $select.find('.js-attr-default-option')
        .attr('value', attr.resetUrl);


    attr.values.forEach(function (attrValue) {
        let label = '';
        if (attrValue.selectable === null) {
            label = $select.data('soldout');
        }
        $select.find('[data-attr-value="' + attrValue.value + '"]')
            .attr('value', attrValue.url)
            .attr('disabled', !attrValue.selectable)
            .attr('data-option-label', label);
    });

    // $select.selectpicker('refresh');
}

/**
 * Updates attribute data on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAttributesAfterAttributeSelect(product, $productContainer) {
    if (product.variationAttributes) {
        // Update data-querystring in page
        var variationAttr = product.variationAttributes[0];
        var newQueryString;

        variationAttr.selectedValue.querystring.split('&').forEach(function (value, index) {
            if (index === 0) {
                newQueryString = variationAttr.id === 'color' ? value + variationAttr.selectedValue.id : value;
            } else {
                newQueryString += '&' + value;
            }
        });

        $('.page').attr('data-querystring', newQueryString.slice(1));

        // Currently, the only attribute type that has image swatches is Color.
        var attrsWithSwatches = {
            color: 1
        };

        product.variationAttributes.forEach(function (attr) {
            if (attrsWithSwatches[attr.id]) {
                processSwatchValues(attr, $productContainer);
            } else {
                processNonSwatchValues(attr, $productContainer);
            }
        });
    }
}

/**
 * Updates quantity input and buttons on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateQuantityAfterAttributeSelect(product, $productContainer) {
    if ($productContainer.parent('.bonus-product-item').length === 0) {
        var $quantityBtn = $productContainer.find('.js-quantity-btn');
        var $quantityInput = $productContainer.find('.js-quantity-input');
        var current = $quantityInput.val();
        var min = 1;
        var max = 0;

        if (product.quantities && product.quantities.length) {
            min = product.quantities[0].value;
            max = product.quantities[product.quantities.length - 1].value;
        }

        var newQuantity = quantitySelector.getQuantity(0, current, min, max);

        if (product.available) {
            $quantityInput.val(newQuantity);
            $quantityInput.removeProp('readonly');
            $quantityInput.removeClass('disabled');
            $quantityBtn.each(function () {
                var $this = $(this);
                if ($this.hasClass('disabled-limit') === false) {
                    $this.removeClass('disabled');
                }
            });
        } else {
            $quantityInput.val(0);
            $quantityInput.prop('readonly', true);
            $quantityInput.addClass('disabled');
            $quantityBtn.addClass('disabled');
        }
    }
}

/**
 * Updates availability on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAvailabilityAfterAttributeSelect(product, $productContainer) {
    var $availabilityContainer = $productContainer.find('.js-availability-container');

    // Update availability data attributes on contanier
    $availabilityContainer
        .data('ready-to-order', product.readyToOrder)
        .data('available', product.available)
        .addClass('d-none');

    if (!product.available &&
        product.readyToOrder &&
        product.availability &&
        product.availability.messages &&
        product.availability.messages.length) {
        // Prepare messages
        var msgHtmlStr = '';
        product.availability.messages.forEach(function (msg) {
            msgHtmlStr += ('<div class="btn btn-block btn-outline-secondary">' + msg + '</div>');
        });

        // Append message string to DOM
        $productContainer
            .find('.js-availability-msg')
            .html(msgHtmlStr);

        // Show availability container
        $availabilityContainer.removeClass('d-none');
    }
}

/**
 * Updates promotions on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updatePromotionsAfterAttributeSelect(product, $productContainer) {
    var html = '';
    if (product.promotions && product.promotions.length) {
        product.promotions.forEach(function (promotion) {
            if (promotion.calloutMsg) {
                html += '<div class="callout" title="' + promotion.details + '">' +
                    promotion.calloutMsg + '</div>';
            }
        });
    }
    $productContainer.find('.js-promotions').html(html);
}

/**
 * Updates title on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateTitleAfterAttributeSelect(product, $productContainer) {
    if (product && product.productName) {
        $productContainer.find('.js-name').html(product.productName);
    }
}

/**
 * Updates short description on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateDescriptionAfterAttributeSelect(product, $productContainer) {
    if (product && product.shortDescription) {
        $productContainer.find('.js-short-description').html(product.shortDescription);
    }
}

/**
 * Updates url on each attribute change
 * @param {Object} product - product object including images array
 */
function updateUrlAfterAttributeSelect(product) {
    if (product && product.selectedProductUrl) {
        history.replaceState({}, document.title, product.selectedProductUrl);
    }
}

/**
 * Updates add to cart button text
 */
function updateAddToCartBtn() {
    var $buttonAddToCart = $('button.add-to-cart, button.add-to-cart-global');
    var buttonText = $buttonAddToCart.data('text-add');
    $buttonAddToCart.text(buttonText);
}

/**
 * Refresh for custom selects
 * @param {Object} $contextEl - contextEl
 */
function refreshAfterAttributeSelectValues($contextEl) {
    var $context = $contextEl || $(document);

    $context.find('.js-attributes-container select').each(function () {
        // var $select = $(this);
        // $select.selectpicker('refresh');
    });
}

/**
 * Moves product info to header or vice versa after scrolling
 * @param {Object} el - jQuery elements object
 */
function toggleCtaOnScroll(el) {
    var currentScrollY = latestKnownScrollY;
    var detailHeight = el.$details.height() + 275;
    if (currentScrollY >= detailHeight) {
        if (el.$placeholder.parent().hasClass('d-none') === false) return;
        el.$details.height(el.$details.height());
        el.$placeholder.append(el.$details.children());
        el.$placeholder.parent().removeClass('d-none');
        el.$placeholderName.append(el.$detailsName.children());
        el.$placeholderImage.append(el.$detailsImage.children());
        el.$placeholderBtn.append(el.$infoBtn.children());
        el.$placeholderPrice.append(el.$infoPrice.children());
        el.$placeholderAttributes.append(el.$infoAttributes.children());
        el.$placeholderMainAttributes.append(el.$infoMainAttributes.children());
    } else {
        if (el.$placeholder.parent().hasClass('d-none')) return;
        el.$detailsName.append(el.$placeholderName.children());
        el.$detailsImage.append(el.$placeholderImage.children());
        el.$infoBtn.append(el.$placeholderBtn.children());
        el.$infoPrice.append(el.$placeholderPrice.children());
        el.$infoAttributes.append(el.$placeholderAttributes.children());
        el.$infoMainAttributes.append(el.$placeholderMainAttributes.children());
        el.$details.height('auto');
        el.$details.append(el.$placeholder.children());
        el.$detailsCarousel.slick('setPosition');
        el.$detailsThumbnailCarousel.slick('setPosition');
        el.$placeholder.parent().addClass('d-none');
    }
}

/**
 * Initializes product call to action box
 * that will be visible after scrolling
 */
function initProductCta() {
    var el = {
        $details: $('.js-product-details'),
        $detailsImage: $('.js-image-container'),
        $detailsName: $('.js-name-container'),
        $detailsCarousel: $('.js-pdp-product-image-carousel'),
        $detailsThumbnailCarousel: $('.js-pdp-product-thumbnail-carousel'),

        $info: $('.js-product-info'),
        $infoPrice: $('.js-price-container'),
        $infoBtn: $('.js-add-to-cart-btn-container'),
        $infoAttributes: $('.js-attributes-container'),
        $infoMainAttributes: $('.js-main-attributes-container'),

        $placeholder: $('.js-product-cta-placeholder'),
        $placeholderBtn: $('.js-product-cta-btn-ph'),
        $placeholderName: $('.js-product-cta-name-ph'),
        $placeholderImage: $('.js-product-cta-image-ph'),
        $placeholderPrice: $('.js-product-cta-price-ph'),
        $placeholderAttributes: $('.js-product-cta-attributes-ph'),
        $placeholderMainAttributes: $('.js-product-cta-main-attributes-ph')
    };

    el.$placeholder.parent().data('pid', el.$details.data('pid'));

    var throttleScroll = throttle(function () {
        toggleCtaOnScroll(el);
    }, 100);

    // Attach scroll event
    $(window).on('scroll', function () {
        // Cache latest scrollY
        latestKnownScrollY = window.scrollY || window.pageYOffset;

        // Toggle call to action box
        throttleScroll();
    });
}

base.initShippingInformationModal = function () {
    var $content = $('.js-shipping-info-content');

    // Remove shipping information button and modal if there is no content in slot
    if ($content.children().length === 0) {
        $content.closest('.js-shipping-info-container').remove();
    }
};

base.initTabsWithSlotContent = function () {
    var $content = $('.js-tab-slot-content');

    // Remove tab if there is no content in slot
    if ($content.children().length === 0) {
        var $container = $content.closest('.js-tab-slot-container');
        var $title = $('#' + $container.attr('aria-labelledby'));
        $title.remove();
        $container.remove();
    }
};


base.updateProductAfterAttributeSelect = function () {
    $('body').on('product:afterAttributeSelect', function (e, response) {
        updateProductIdAfterAttributeSelect(response.data.product, response.container);
        updateCarouselAfterAttributeSelect(response.data.product, response.container);
        updateQuantityAfterAttributeSelect(response.data.product, response.container);
        updateAttributesAfterAttributeSelect(response.data.product, response.container);
        updatePromotionsAfterAttributeSelect(response.data.product, response.container);
        updateTitleAfterAttributeSelect(response.data.product, response.container);
        updateDescriptionAfterAttributeSelect(response.data.product, response.container);
        updateUrlAfterAttributeSelect(response.data.product);
        updateAddToCartBtn();
        refreshAfterAttributeSelectValues();
    });

    $('body').on('loaded.bs.select refreshed.bs.select', '.js-quantity-select, .js-product-quantity-select', function (event) {
        // Remove duplicated class names from dynamically created parent div element for custom dropdown
        // Add custom classes set in attribute 'data-dropdown-class'
        var $selectEl = $(event.currentTarget);
        var selectElClassNames = $selectEl.attr('class');
        var selectElDropdownClass = $selectEl.data('dropdownClass');
        var selectElDropdownRemoveClass = $selectEl.data('dropdownRemoveClass');
        if (selectElDropdownClass) {
            $selectEl.parent().removeClass(selectElClassNames).addClass(selectElDropdownClass);
        }
        if (selectElDropdownRemoveClass) {
            $selectEl.parent().removeClass(selectElDropdownRemoveClass);
        }
    });

    refreshAfterAttributeSelectValues();

    $('body').on('product:updateAvailability', function (e, response) {
        updateAvailabilityAfterAttributeSelect(response.product, response.$productContainer);
    });
};

base.initViewAll = function () {
    var $doc = $(document);
    var $longDescription = $('.js-long-description');

    $doc.on('click', '.js-pdp-view-less', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.js-pdp-view-all-area').addClass('d-none');
        $(this).addClass('d-none');
        $('.js-pdp-view-all').removeClass('d-none');
    });

    $doc.on('click', '.js-pdp-view-all', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.js-pdp-view-all-area').removeClass('d-none');
        $(this).addClass('d-none');
        $('.js-pdp-view-less').removeClass('d-none');
    });

    $doc.on('click', '.js-social-icon-share', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.js-social-icons').slideToggle();
    });

    // Bonus Products Modal
    $doc.on('shown.bs.modal', '#chooseBonusProductModal', function (e) {
        refreshAfterAttributeSelectValues($(e.target).find('.js-product-container'));
    });

    if ($('.js-long-description-height').innerHeight() <= $longDescription.data('maxHeight')) {
        $longDescription.addClass('m-disabled');
    }

    $doc.on('click', '.js-product-description-more', function (e) {
        e.preventDefault();
        $longDescription.toggleClass('m-expanded');
    });

    $doc.on('click', '.js-product-zoom-image', function (e) {
        e.preventDefault();
        $('.js-product-zoom').addClass('modal modal-md fade show');
        $('.js-body-hidden').addClass('modal-open');
        $(window).trigger('scroll');
    });

    $doc.on('click', '.js-product-zoom-close', function (e) {
        e.preventDefault();
        $('.js-product-zoom').removeClass('modal modal-md fade show');
        $('.js-body-hidden').removeClass('modal-open');
        $(window).trigger('scroll');
    });
};

base.showSpinner = function () {
    $('body').on('product:beforeAttributeSelect', function () {
        $.spinner().start();
    });
};

base.sizeChart = function () {
    var sizeChartLoaded = false;
    var $sizeChartModal = $('.js-size-chart-modal');

    $('.js-size-chart-link').on('click', function (event) {
        event.preventDefault();

        if (sizeChartLoaded) {
            $sizeChartModal.modal('show');
            return;
        }

        var url = $(event.target).data('href');

        $.spinner().start();

        $.ajax({
            url: url,
            success: function (data) {
                var $modalContent = $sizeChartModal.find('.js-size-chart-modal-content');

                $modalContent.html(data.content);
                $sizeChartModal.modal('show');

                $.spinner().stop();
                sizeChartLoaded = true;
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};

// base.initDriftZoom = initDriftZoom;
base.initProductCta = initProductCta;
base.initQuantitySelector = quantitySelector.init;
base.initProductImageCarousel = initProductImageCarousel;

module.exports = base;
