import Component from '../core/Component';

export default class Form extends Component {
    /**
     * Initialize component
     */
    init() {
        super.init();

        if (this.$el.is('form')) {
            this.initCache();
            this.initEvents();
        }
    }

    /**
     * Set selectors
     */
    initCache() {
        this.$submitButton = $('button[type="submit"], input[type="submit"]', this.$el);
        this.$formInputs = $('input, select', this.$el);
    }

    /**
     * Initialize component events
     */
    initEvents() {
        this.event('click', this.onSubmit.bind(this), this.$submitButton);
        this.event('submit', this.onSubmit.bind(this));
    }

    /**
     * Displays error messages for invalid field
     */
    handleInvalidInput() {
        this.setCustomValidity('');
        if (!this.validity.valid) {
            var validationMessage = this.validationMessage;
            $(this).addClass('is-invalid');
            if ((this.validity.patternMismatch || this.validity.typeMismatch) && $(this).data('pattern-mismatch')) {
                validationMessage = $(this).data('pattern-mismatch');
            }
            if ((this.validity.rangeOverflow || this.validity.rangeUnderflow) &&
                $(this).data('range-error')) {
                validationMessage = $(this).data('range-error');
            }
            if ((this.validity.tooLong || this.validity.tooShort) &&
                $(this).data('range-error')) {
                validationMessage = $(this).data('range-error');
            }
            if (this.validity.valueMissing && $(this).data('missing-error')) {
                validationMessage = $(this).data('missing-error');
            }
            $(this).closest('.form-group').find('.invalid-feedback').text(validationMessage);
        }
    }

    /**
     * Validate whole form using native checkValidity method
     * @param {jQuery.event} event - Event to be canceled if form is invalid.
     */
    onSubmit(event) {
        // Remove old error messages
        this.$el.find('.form-control.is-invalid').removeClass('is-invalid');

        // Trigger validation check
        if (this.$el[0].checkValidity && !this.$el[0].checkValidity()) {
            if (event) {
                // Disable default HTML5 handlers, also form submit
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }

            // Display error messages
            this.$formInputs.each((index, input) => {
                if (!input.validity.valid) {
                    this.handleInvalidInput.call(input);
                }
            });
        }

        return;
    }
}
