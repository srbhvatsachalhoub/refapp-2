'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Render logic for the mosaicComponentSmall
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    var component = context.component;

    model.title = content.title || null;
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.customClasses1 = context.content.classes1 || 'col-4';
    model.customClasses2 = context.content.classes2 || 'col-4';
    model.customClasses3 = context.content.classes3 || 'col-4';
    return new Template('experience/components/commerce_layouts/mosaicComponentSmall').render(model).text;
};
