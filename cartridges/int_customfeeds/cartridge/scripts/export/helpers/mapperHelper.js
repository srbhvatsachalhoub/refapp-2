'use strict';

/**
 * Method to fetch values from enum object
 * @param {Object} enumObject    placeholder for enum type object
 * @returns {string} values      list of values
 */
function getEnumValues(enumObject) {
    var values;
    var currentElement;
    if (enumObject.length > 0) {
        for (var i = 0; i < enumObject.length; i++) {
            currentElement = '<value>' + enumObject[i].getDisplayValue() + '</value>';
            values = (!values) ? currentElement : values + currentElement;
        }
    }
    return values;
}

/**
 * Check if the the given dw object has the given custom attribute value
 * if so return this value, otherwise retun empty string
 * @param {Object} dwObject - Object that holds the attribute values. Can be Order OR Product OR Profile
 * @param {string} field - the custom field name
 * @returns {string} the value of the custom attribute if found, otherwise empty string
 */
function handleCustomAttribute(dwObject, field) {
    var value = '';
    if (field.indexOf('custom.') === 0) {
        var cname = field.substr(7);
        if ('custom' in dwObject && cname in dwObject.custom) {
            var customObject = dwObject.custom;
            value = (Object.prototype.propertyIsEnumerable.call(customObject, 'cname')) ? getEnumValues(customObject[cname]) : dwObject.custom[cname];
        }
    }
    return value;
}

module.exports = {
    handleCustomAttribute: handleCustomAttribute
};
