<isinclude template="/components/modules" sf-toolkit="off" />
<form
    action="${URLUtils.url('Subscribe-Save')}"
    class="subscribe-newsletter-form subscription-form"
    method="POST">
    <div class="row">
        <div class="col-12">
            <isslot id="subscription-above-form-m" description="Newsletter subscription page above form" context="global" />
        </div>

        <div class="card-body js-success text-success d-none">
            <span class="fa fa-check"></span>
            ${Resource.msg('msg.subscribe.success', 'forms', null)}
        </div>
        <div class="card-body js-error text-danger d-none">
            <span class="fa fa-exclamation"></span>
            ${Resource.msg('msg.subscribe.error', 'forms', null)}
        </div>

        <div class="col-12">
            <isinputtitle   inputtitle_formfield="${pdict.subscribeForm.title.titleList}"
                            inputtitle_selected="${pdict.subscribeForm.title.titleList.selectedOption}" 
                            inputtitle_separatorclass="col-4"/>
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.subscribeForm.firstname}"
                            inputtext_autocomplete="given-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.firstname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.firstname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.firstname', 'forms', null)}"
            />
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.subscribeForm.lastname}"
                            inputtext_autocomplete="family-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.lastname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.lastname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.lastname', 'forms', null)}" />
        </div>

        <div class="d-none col-md-6">
            <isinputphone   inputphone_multiregion="${true}"
                            inputphone_formfield="${pdict.subscribeForm.phoneMobile}"
                            inputphone_value="${pdict.subscribeForm.phoneMobile.htmlValue}"
                            inputphone_missingerror="${Resource.msg('error.message.required.phonemobile', 'forms', null)}"
                            inputphone_parseerror="${Resource.msg('error.message.parse.phonemobile', 'forms', null)}"
                            inputphone_rangeerror="${Resource.msg('error.message.range.phonemobile', 'forms', null)}"
            />
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.subscribeForm.email}"
                            inputtext_type="email"
                            inputtext_autocomplete="email"
                            inputtext_placeholder="email@domain.com"
                            inputtext_missingerror="${Resource.msg('error.message.required.email', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.email', 'forms', null)}"
            />
        </div>

        <div class="d-none col-md-6">
            <isselectdate   selectdate_formfield="${pdict.subscribeForm.birthday}"
                            selectdate_value="${pdict.subscribeForm.birthday.value}"
                            selectdate_label="${Resource.msg('label.register.birthday', 'forms', null)}" />
        </div>

        <div class="d-none col-md-6">
            <ischeckboxnewsletter   checkboxnewsletter_formfield="${pdict.subscribeForm.addtoemaillist}"
                                    checkboxnewsletter_disabled="${true}"
                                    checkboxnewsletter_checked="${true}" />
        </div>
        <div class="col-12">
            <label class="form-control-label hidden-md-down">&nbsp;</label>
            <div class="form-group
                ${pdict.subscribeForm.acceptTermsConditions.mandatory === true ? 'required' : ''}">
                <input type="checkbox" id="acceptTermsConditions"
                    <isprint value="${pdict.subscribeForm.acceptTermsConditions.attributes}" encoding="off" />>
                <label class="form-control-label" for="acceptTermsConditions">
                    ${Resource.msg('check.terms.conditions','forms', null)}
                </label>
                <div class="invalid-feedback"></div>
            </div>
        </div>

        <div class="d-none col-12">
            <isoptinlist    optinlist_formfield="${pdict.subscribeForm.accountpreferences.optinlist}"
                            optinlist_checkall="${true}" />
        </div>

        <div class="col-md-6"></div>

        <div class="col-12">
            <button class="btn btn-block btn-primary js-subbscribe-modal-button" type="submit" name="${pdict.subscribeForm.base.subscribe.htmlName}">
                ${pdict.formButtonDisplayName}
            </button>
        </div>
        <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
    </div>
</form>
