<div class="miniCart-item ${lineItem.productType === 'bundle' ? 'flex-column' : ''}" data-product-line-item="${lineItem.UUID}">
    <isif condition="${lineItem.productType === 'bundle'}">
        <div class="miniCart-item-name">
            <h2>
                <isif condition="${miniCart}">
                    <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                        itemprop="url"
                        <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                            data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                        </isif>>
                        ${lineItem.productName}
                    </a>
                <iselse/>
                    ${lineItem.productName}
                </isif>
            </h2>
            <div class="bundle-includes">${Resource.msg('msg.bundle.includes', 'cart', null)}</div>
        </div>

        <iscomment>individual bundled line items</iscomment>
        <isinclude template="cart/productCard/cartProductCardBundledItems" />

        <div class="d-flex justify-content-between mt-2 mb-1">
            <div class="miniCart-item-details pb-0">
                <span>${Resource.msg('label.quantity', 'cart', null)}:
                    <isprint value="${lineItem.quantity}" formatter="##" />
                </span>
            </div>
            <div class="miniCart-item-price pb-0 price">
                <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
            </div>
        </div>
    <iselse/>
        <isif condition="${lineItem.images && lineItem.images.small && lineItem.images.small.length > 0}">
            <div class="miniCart-item-image">
                <isif condition="${miniCart}">
                    <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                        <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                            data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                        </isif>>
                        <img class="product-image"
                            src="${lineItem.images.small[0].url}"
                            alt="${lineItem.images.small[0].alt}"
                            title="${lineItem.images.small[0].title}" />
                    </a>
                <iselse/>
                    <img class="product-image"
                        src="${lineItem.images.small[0].url}"
                        alt="${lineItem.images.small[0].alt}"
                        title="${lineItem.images.small[0].title}" />
                </isif>
            </div>
        </isif>
        <div class="miniCart-item-description w-100">
            <div class="miniCart-item-name">
                <h2>
                    <isif condition="${miniCart}">
                        <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                            itemprop="url"
                            <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                            </isif>>
                            ${lineItem.productName}
                        </a>
                    <iselse/>
                        ${lineItem.productName}
                    </isif>
                </h2>
            </div>
            <div class="d-flex justify-content-between align-items-end">
                <p class="miniCart-item-details">
                    <isloop items="${lineItem.variationAttributes}" var="attribute">
                        <span>${attribute.displayValue}</span>
                    </isloop>
                    <isloop items="${lineItem.options}" var="option">
                        <span>${option.displayName}</span>
                    </isloop>
                    <span>${Resource.msg('label.mini.cart.quantity', 'cart', null)}:
                        <isprint value="${lineItem.quantity}" formatter="##" />
                    </span>
                </p>
                <div class="miniCart-item-price price">
                    <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
                </div>
            </div>
        </div>
    </isif>

    <isif condition="${miniCart}">
        <div class="line-item-promo item-${lineItem.UUID}">
            <isinclude template="checkout/productCard/productCardProductPromotions" />
        </div>
    </isif>
</div>
