'use strict';

var addressHelpers = require('./address');
var shippingHelpers = require('brand_core/checkout/shipping');
var billingHelpers = require('./billing');
var summaryHelpers = require('brand_core/checkout/summary');
var formHelpers = require('brand_core/checkout/formErrors');
var StickySidebar = require('sticky-sidebar/dist/sticky-sidebar');
var clientSideValidation = require('brand_core/components/clientSideValidation');
var throttle = require('lodash/throttle');

/**
 * Create the jQuery Checkout Plugin.
 *
 * This jQuery plugin will be registered on the dom element in checkout.isml with the
 * id of "checkout-main".
 *
 * The checkout plugin will handle the different state the user interface is in as the user
 * progresses through the varying forms such as shipping and payment.
 *
 * Billing info and payment info are used a bit synonymously in this code.
 *
 */
(function ($) {
    $.fn.checkout = function () { // eslint-disable-line
        var plugin = this;

        // Collect form data from user input
        var formData = {
            // Shipping Address
            shipping: {},

            // Billing Address
            billing: {},

            // Payment
            payment: {},

            // Gift Codes
            giftCode: {}
        };

        // The different states/stages of checkout
        var checkoutStages = [
            'shipping',
            'payment'
        ];

        /**
         * Updates the URL to determine stage
         * @param {number} currentStage - The current stage the user is currently on in the checkout
         */
        function updateUrl(currentStage) {
            history.pushState(
                checkoutStages[currentStage],
                document.title,
                location.pathname
                + '?stage='
                + checkoutStages[currentStage]
                + '#'
                + checkoutStages[currentStage]
            );
        }

        // Local member methods of the Checkout plugin
        var members = {

            // initialize the currentStage variable for the first time
            currentStage: 0,

            /**
             * Shipping stage
             * @param {Object} deferred - jQuery Deferred Object
             */
            shipping: function (deferred) {
                // Clear Previous Errors
                formHelpers.clearPreviousErrors('.shipping-form');

                // Submit the Shipping Address Form
                var $isChecked = $('.js-checkbox-sync-shipping-and-billing').is(':checked');

                if ($isChecked) {
                    var $billingAddressForm = $('.js-form-billing-address');
                    var $shippingAddressForm = $('.js-form-shipping-address');
                    var comparableAddressFields = [
                        'input[name$=_firstName]',
                        'input[name$=_lastName]',
                        'input[name$=_address1]',
                        'input[name$=_address2]',
                        'input[name$=_phone]',
                        'select[name$=_country],input[name$=_country]',
                        'select[name$=_stateCode],input[name$=_stateCode]',
                        'input[name$=_city]'
                    ];

                    comparableAddressFields.forEach(function (selector) {
                        var $billingField = $billingAddressForm.find(selector);
                        var $shippingField = $shippingAddressForm.find(selector);

                        if ($billingField.length && $shippingField.length) {
                            $billingField.val($shippingField.val());
                            if ($billingField.is('select')) {
                                $billingField.trigger('change');
                                $billingField.selectpicker('refresh');
                            }
                            if ($billingField.is('input')) $billingField.trigger('input');
                        }
                    });
                }

                var isMultiShip = $('#checkout-main').hasClass('multi-ship');
                var formSelector = isMultiShip ?
                    '.multi-shipping .active form' : '.single-shipping .shipping-form';
                var form = $(formSelector);

                if (clientSideValidation.functions.validateForm(form[0]) === false) {
                    $('body').trigger('checkout:updateStage:error', form);
                    deferred.reject();
                    return;
                }

                if (isMultiShip && form.length === 0) {
                    // in case the multi ship form is already submitted
                    var url = $('#checkout-main').attr('data-checkout-get-url');
                    $.ajax({
                        url: url,
                        method: 'GET',
                        success: function (data) {
                            if (!data.error) {
                                $('body').trigger('checkout:updateCheckoutView', {
                                    order: data.order,
                                    customer: data.customer
                                });
                                $('body').trigger('gtm:enhancedEcommercePushPageDataLayer', {
                                    pageDataLayerEnhancedEcommerce: data.pageDataLayerEnhancedEcommerce
                                });
                                deferred.resolve();
                            } else if ($('.shipping-error .alert-danger').length < 1) {
                                var errorMsg = data.message;
                                var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                                    'fade show" role="alert">' +
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                    '</button>' + errorMsg + '</div>';
                                $('.shipping-error').append(errorHtml);
                                deferred.reject();
                            }
                        },
                        error: function () {
                            // Server error submitting form
                            deferred.reject();
                        }
                    });
                } else {
                    var shippingFormData = form.serialize();

                    $('body').trigger('checkout:serializeShipping', {
                        form: form,
                        data: shippingFormData,
                        callback: function (data) {
                            shippingFormData = data;
                        }
                    });

                    $.ajax({
                        url: form.attr('action'),
                        type: 'post',
                        data: shippingFormData,
                        success: function (data) {
                            shippingHelpers.methods.shippingFormResponse(deferred, data);
                        },
                        error: function (err) {
                            if (err.responseJSON.redirectUrl) {
                                window.location.href = err.responseJSON.redirectUrl;
                            }
                            // Server error submitting form
                            deferred.reject(err.responseJSON);
                        }
                    });
                }
            },

            /**
             * Payment stage
             * @param {Object} deferred - jQuery Deferred Object
             */
            payment: function (deferred) {
                // Submit the Billing Address Form
                var $paymentForm = $('#dwfrm_billing');
                var isNewPayment = $('[data-is-new-payment="true"]').length > 0;
                var hasSavedPaymentMethods = $('[data-has-payment-payment-instruments="true"]').length > 0;
                var methodId = $('.payment-information').attr('data-payment-method-id');
                var paymentData;

                formHelpers.clearPreviousErrors('.payment-form');

                if (!clientSideValidation.functions.validateForm($paymentForm[0])) {
                    var hasInvalidFields = false;
                    var validatedForms = ['.js-form-billing-address'];

                    if (isNewPayment) {
                        validatedForms.push(hasSavedPaymentMethods ?
                            '.js-form-add-new-credit-card:not(.checkout-hidden)' :
                            '.payment-method-credit_card.active .js-form-add-new-credit-card'
                        );
                    }

                    $paymentForm.find('.is-invalid').each(function () {
                        var $this = $(this);
                        if ($this.closest(validatedForms.join(',')).length) {
                            hasInvalidFields = true;
                        } else {
                            formHelpers.clearPreviousErrors($this.parent());
                        }
                    });

                    if (hasInvalidFields) {
                        $('html, body').animate({
                            scrollTop: $paymentForm.find('.is-invalid').first().offset().top - $('.js-sticky-header').height()
                        }, 1000);
                        $('body').trigger('checkout:updateStage:error', $paymentForm);
                        deferred.reject();
                        return;
                    }
                }

                $('body').trigger('checkout:serializeBilling', {
                    form: $paymentForm,
                    callback: function (data) { paymentData = data; }
                });

                if ($('.data-checkout-stage').data('customer-type') === 'registered') {
                    // if payment method is credit card
                    if (methodId === 'CREDIT_CARD') {
                        if (isNewPayment === false) {
                            var $cvvCode = $('.saved-payment-instrument.selected-payment .saved-payment-security-code');
                            var cvvCode = $cvvCode.val();

                            if (cvvCode === '' || cvvCode.length < $cvvCode.attr('maxlength')) {
                                $cvvCode.addClass('is-invalid');
                                $('html, body').animate({
                                    scrollTop: $cvvCode.parent().offset().top - $('.js-sticky-header').height()
                                }, 1000);
                                deferred.reject();
                                return;
                            }

                            var $savedPaymentInstrument = $('.saved-payment-instrument.selected-payment');

                            paymentData += '&storedPaymentUUID=' +
                                $savedPaymentInstrument.data('uuid');

                            paymentData += '&securityCode=' + cvvCode;

                            $('#dwfrm_billing .temp-cvv').remove();
                            $('<input>').attr({
                                type: 'hidden',
                                class: 'temp-cvv',
                                value: cvvCode
                            }).appendTo('#dwfrm_billing');
                        }
                    }
                }

                $.ajax({
                    url: $('#dwfrm_billing').attr('action'),
                    method: 'POST',
                    data: paymentData,
                    success: function (data) {
                        // look for field validation errors
                        if (data.error) {
                            if (data.fieldErrors.length) {
                                data.fieldErrors.forEach(function (error) {
                                    if (Object.keys(error).length) {
                                        formHelpers.loadFormErrors('.payment-form', error);
                                    }
                                });
                            }

                            if (data.serverErrors.length) {
                                data.serverErrors.forEach(function (error) {
                                    $('.error-message').show();
                                    $('.error-message-text').text(error);
                                });
                            }

                            if (data.cartError) {
                                window.location.href = data.redirectUrl;
                            }

                            $.spinner().stop();
                            deferred.reject();
                        } else {
                            // Populate the Address Summary
                            $('body').trigger('checkout:updateCheckoutView', {
                                order: data.order,
                                customer: data.customer
                            });
                            $('body').trigger('gtm:enhancedEcommercePushPageDataLayer', {
                                pageDataLayerEnhancedEcommerce: data.pageDataLayerEnhancedEcommerce
                            });

                            if (data.customer.registeredUser
                                && data.customer.customerPaymentInstruments.length
                            ) {
                                $('.cancel-new-payment').removeClass('checkout-hidden');
                            }

                            // MUFE doesn't have a place order stage,
                            // hence we're directly calling following method
                            members.placeOrder(deferred);
                        }
                    },
                    error: function (err) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        }
                    }
                });
            },

            /**
             * Place order stage
             * @param {Object} deferred - jQuery Deferred Object
             */
            placeOrder: function (deferred) {
                $.ajax({
                    url: $('.place-order').data('action'),
                    method: 'POST',
                    data: $('#dwfrm_billing .temp-cvv').length ? 'securityCode=' + $('#dwfrm_billing .temp-cvv').val() : '',
                    success: function (placeOrderData) {
                        if (placeOrderData.error) {
                            if (placeOrderData.cartError) {
                                window.location.href = placeOrderData.redirectUrl;
                                deferred.reject();
                            } else {
                                // go to appropriate stage and display error message
                                deferred.reject(placeOrderData);
                            }
                        } else {
                            $('body').trigger('checkout:placeOrderSuccess', placeOrderData);
                            deferred.resolve(placeOrderData);
                        }
                    },
                    error: function () {

                    }
                });
            },

            /**
             * Set or update the checkout stage (AKA the shipping, billing, payment, etc... steps)
             * @param {Object} deferred - jQuery Deferred Object
             */
            updateStage: function (deferred) {
                // Find current stage
                var stage = checkoutStages[members.currentStage];

                // If defined, call stage function otherwise reject deferred object
                if (members[stage]) {
                    members[stage](deferred);
                } else {
                    deferred.reject();
                }
            },

            /**
             * Initialize the checkout stage.
             *
             */
            initialize: function () {
                // set the initial state of checkout
                members.currentStage = checkoutStages
                    .indexOf($('.data-checkout-stage').data('checkout-stage'));
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);

                // Handle Payment option selection
                $('input[name$="paymentMethod"]', plugin).on('change', function () {
                    $('.credit-card-form').toggle($(this).val() === 'CREDIT_CARD');
                });

                // Handle Next State button click
                $(plugin).on('click', '.next-step-button button', function () {
                    members.nextStage();
                });

                // Handle Edit buttons on shipping and payment summary cards
                $('.shipping-summary .edit-button', plugin).on('click', function () {
                    if (!$('#checkout-main').hasClass('multi-ship')) {
                        $('body').trigger('shipping:selectSingleShipping');
                    }

                    members.gotoStage('shipping');
                });

                $('.payment-summary .edit-button', plugin).on('click', function () {
                    members.gotoStage('payment');
                });

                $('.js-show-shipping-step').on('click', function (e) {
                    e.preventDefault();
                    members.gotoStage('shipping');
                });

                // remember stage (e.g. shipping)
                updateUrl(members.currentStage);

                // Listen for foward/back button press and move to correct checkout-stage
                $(window).on('popstate', function (e) {
                    // Back button when event state less than current state in ordered
                    // checkoutStages array.
                    formHelpers.clearPreviousErrors('.shipping-form');
                    formHelpers.clearPreviousErrors('.payment-form');
                    $('.saved-payment-security-code').val('');

                    if (e.state === null ||
                         checkoutStages.indexOf(e.state) < members.currentStage) {
                        members.handlePrevStage(false);
                    } else if (checkoutStages.indexOf(e.state) > members.currentStage) {
                        // Forward button  pressed
                        members.handleNextStage(false);
                    }
                });

                // Set the form data
                plugin.data('formData', formData);
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             */
            nextStage: function () {
                var deferred = $.Deferred(); // eslint-disable-line

                // Start spinner
                $.spinner().start();

                deferred.done(function () {
                    // Update UI with new stage
                    members.handleNextStage(true);

                    // Stop spinner
                    $.spinner().stop();
                });

                deferred.fail(function (data) {
                    // Show errors
                    if (data) {
                        if (data.errorStage) {
                            members.gotoStage(data.errorStage.stage);

                            if (data.errorStage.step === 'billingAddress') {
                                var $billingAddressSameAsShipping = $(
                                    'input[name$="_shippingAddressUseAsBillingAddress"]'
                                );
                                if ($billingAddressSameAsShipping.is(':checked')) {
                                    $billingAddressSameAsShipping.prop('checked', false);
                                }
                            }
                        }

                        if (data.errorMessage) {
                            $('.js-error-message-text').text(data.errorMessage).parent().show();
                        }
                    }

                    // Stop spinner
                    $.spinner().stop();
                });

                members.updateStage(deferred);
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             *
             * @param {boolean} bPushState - boolean when true pushes state using the history api.
             */
            handleNextStage: function (bPushState) {
                if (members.currentStage < checkoutStages.length - 1) {
                    // Move stage forward
                    members.currentStage++;

                    // Show new stage in url (e.g.payment)
                    if (bPushState) {
                        updateUrl(members.currentStage);
                    }
                }

                // Set the next stage on the DOM
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Previous State
             */
            handlePrevStage: function () {
                if (members.currentStage > 0) {
                    // move state back
                    members.currentStage--;
                    updateUrl(members.currentStage);
                }

                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Use window history to go to a checkout stage
             * @param {string} stageName - the checkout state to goto
             */
            gotoStage: function (stageName) {
                var stageIndex = checkoutStages.indexOf(stageName);
                if (stageIndex !== members.currentStage) {
                    members.currentStage = stageIndex;
                    updateUrl(members.currentStage);
                    $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
                }
            }
        };

        //
        // Initialize the checkout
        //
        members.initialize();

        return this;
    };
}(jQuery));

var exports = {
    initialize: function () {
        $('#checkout-main').checkout();

        var sidebar = new StickySidebar('.js-sticky-sidebar', { // eslint-disable-line no-unused-vars
            topSpacing: 80,
            bottomSpacing: 20,
            containerSelector: '.js-sticky-sidebar-container',
            innerWrapperSelector: '.js-sticky-sidebar-inner'
        });
    },

    updateCheckoutView: function () {
        $('body').on('checkout:updateCheckoutView', function (e, data) {
            shippingHelpers.methods.updateMultiShipInformation(data.order);
            summaryHelpers.updateTotals(data.order.totals);
            data.order.shipping.forEach(function (shipping) {
                shippingHelpers.methods.updateShippingInformation(
                    shipping,
                    data.order,
                    data.customer,
                    data.options
                );
            });
            billingHelpers.methods.updateBillingInformation(
                data.order,
                data.customer,
                data.options
            );
            billingHelpers.methods.updatePaymentInformation(data.order, data.options);
            summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
        });
    },

    placeOrderSuccess: function () {
        // placeOrder Success with continueUrl
        $('body').on('checkout:placeOrderSuccess', function (e, data) {
            if (!data || !data.continueUrl) {
                return;
            }

            var continueUrl = data.continueUrl;
            var urlParams = {
                ID: data.orderID,
                token: data.orderToken
            };

            continueUrl += (continueUrl.indexOf('?') !== -1 ? '&' : '?') +
                Object.keys(urlParams).map(function (key) {
                    return key + '=' + encodeURIComponent(urlParams[key]);
                }).join('&');

            window.location.href = continueUrl;
        });

        // placeOrder Success with form post
        $('body').on('checkout:placeOrderSuccess', function (e, data) {
            if (!data || !data.form) {
                return;
            }
            $('body').append(data.form);
            $('#psp_payment_form input[type=submit]').click();
        });

        // placeOrder Success with redirect
        $('body').on('checkout:placeOrderSuccess', function (e, data) {
            if (!data || !data.redirect) {
                return;
            }
            window.location.href = data.redirectUrl;
        });
    },

    initHelpers: function () {
        [billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
            Object.keys(library).forEach(function (item) {
                if (typeof library[item] === 'function') {
                    library[item]();
                }
            });
        });
    }
};

[billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
    Object.keys(library).forEach(function (item) {
        if (typeof library[item] === 'object') {
            exports[item] = $.extend({}, exports[item], library[item]);
        }
    });
});

var $orderProductSummary = $('.order-product-summary');
var $orderProductSummaryContainer = $('.js-view-order-summary-desktop');
var $orderProductSummaryMobile = $('.js-view-order-summary-mobile');
/**
 * Moves order product summary to different places on mobile and desktop views
 */
function moveOrderProductSummary() {
    if ($(window).width() < window.RA_BREAKPOINTS.md) {
        $orderProductSummaryMobile.append($orderProductSummary);
    } else {
        $orderProductSummaryContainer.append($orderProductSummary);
    }
}

// Change order product summary location on mobile and desktop versions
moveOrderProductSummary();
$(window).on('resize', throttle(moveOrderProductSummary, 100));

module.exports = exports;
