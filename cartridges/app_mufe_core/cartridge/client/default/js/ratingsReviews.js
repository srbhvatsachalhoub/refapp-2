var countCharacters = require('brand_core/helpers/countCharacters');

var initialLoading = true;

/**
 * Gets local storage key for review
 * @param {string} reviewId Id of the review
 * @returns {string} the local storage key
 */
function getReviewVotedLocalStorageKey(reviewId) {
    return 'review-' + reviewId + '-voted';
}

/**
 * Checks if the client agent is mobile
 * @returns {boolean} true if client is mobile false otherwise
 */
function isMobile() {
    return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
}

/**
 * Requests the reviews from the server
 * @param {boolean} sortingChanged true if the sorting option is changed, false otherwise
 * @param {string} sortingCriteria sorting criteria that client asked for
 * @param {int} firstLimit how many reviews should we gather on load of the page
 */
function gatherReviews(sortingChanged, sortingCriteria, firstLimit) {
    var reviewsContainer = $('.js-reviews');
    var url = $('section.ratings-and-reviews').data('reviews-url');
    var pid = $('section.ratings-and-reviews').data('product-id');
    var start = reviewsContainer.find('.js-review').length;
    var limit = firstLimit || 4;
    if (sortingChanged) {
        limit = start;
        start = 0;
    }
    $.ajax({
        url: url,
        method: 'GET',
        data: {
            pid: pid,
            start: start,
            limit: limit,
            sortingCriteria: sortingCriteria
        },
        success: function (result) {
            var reviewsResponseObj = $(result).find('.js-reviews-response-data');
            var reviews = $(result).find('.js-review');
            reviews.each(function () {
                var review = $(this);
                var reviewVotedLocalStorageKey = getReviewVotedLocalStorageKey(review.data('review-id'));
                var reviewVoted = localStorage.getItem(reviewVotedLocalStorageKey);
                review.find('.js-rating-vote').each(function () {
                    if (reviewVoted) {
                        $(this).attr('disabled', true);
                    }
                });
            });

            if (initialLoading && reviews.length > 0) {
                $('.js-sort-reviews').toggleClass('d-none');
                $('.js-sort-label').toggleClass('d-none');
                $('.js-reviews').toggleClass('d-none');
                $('.js-show-more').toggleClass('d-none');
                initialLoading = false;
            }

            if (sortingChanged) {
                reviewsContainer.html(reviews);
            } else {
                reviewsContainer.append(reviews);
            }

            var moreExists = reviewsResponseObj.data('more-exists');
            if (!moreExists) {
                $('.js-show-more').hide();
            }
        }
    });
}

// initial review load
$(document).ready(function () {
    var firstLimit = isMobile() ? 1 : 2;
    gatherReviews(false, $('.js-sort-reviews').val(), firstLimit);
});

// show more
$('.js-show-more').on('click', function (e) {
    e.preventDefault();
    gatherReviews(false, $('.js-sort-reviews').val());
});
// sort
$('.js-sort-reviews').on('change', function () {
    gatherReviews(true, $(this).val());
});

// review voting
$(document).on('click', '.js-rating-vote', function (e) {
    e.preventDefault();
    var voteButton = $(this);
    var url = voteButton.data('vote-url');
    var reviewId = voteButton.data('review-id');
    var reviewVotedLocalStorageKey = getReviewVotedLocalStorageKey(reviewId);
    var reviewVoted = localStorage.getItem(reviewVotedLocalStorageKey);
    if (reviewVoted) {
        return;
    }

    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
            reviewId: reviewId
        },
        success: function (data) {
            if (!data.success) {
                // no need to show error message to user, it's already logged at the backend.
            } else {
                voteButton.text(data.label);
                voteButton.parent().find('.js-rating-vote').each(function () {
                    $(this).attr('disabled', true);
                });
                localStorage.setItem(reviewVotedLocalStorageKey, true);
            }
        },
        error: function (err) {
            if (err.responseJSON.redirectUrl) {
                window.location.href = err.responseJSON.redirectUrl;
            }
        }
    });
});

// review submission
var formValidation = require('base/components/formValidation');
$('form.submit-review-form').submit(function (e) {
    var $form = $(this);
    e.preventDefault();
    var url = $form.attr('action');
    $form.spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            $form.spinner().stop();
            if (!data.success) {
                formValidation($form, data);
                $('.js-submit-error').html(data.message).toggleClass('d-none');
            } else {
                if (data.reload) {
                    window.location.reload();
                    return;
                }
                $form[0].reset();
                $('#rating', $form).val(5);
                $('#recommended-yes', $form).trigger('click');
                $('#reviewSubmittedModal .js-success-message').html(data.message);
                $('#reviewSubmittedModal').modal('show');
                $('.js-write-review-section').remove();
            }
        },
        error: function (err) {
            if (err.responseJSON.redirectUrl) {
                window.location.href = err.responseJSON.redirectUrl;
            }
            $form.spinner().stop();
        }
    });
    return false;
});

/**
 * Handles stars input visually
 */
function handleStarsInputVisually() {
    var hoveredStarValue = parseInt($(this).data('value'), 10);
    var selectedStarValue = parseInt($('.js-rating-input').val(), 10);

    var value = hoveredStarValue || selectedStarValue;

    $('.js-rating-stars-input i').each(function () {
        var $star = $(this);
        if (parseInt($star.data('value'), 10) > value) {
            $star.removeClass('icon-star');
            $star.addClass('icon-star-empty');
        } else {
            $star.removeClass('icon-star-empty');
            $star.addClass('icon-star');
        }
    });
}

// open review form
$('.js-write-review').on('click', function (e) {
    e.preventDefault();
    handleStarsInputVisually();
    countCharacters();
    $('.js-write-review-section').toggleClass('d-none');
    $('.js-write-review').attr('disabled', true);
    $('html,body').animate({ scrollTop: $('.js-write-review-section').offset().top - $('.header-top').outerHeight() }, 'slow');
});

// close review form
$('.js-close-form').on('click', function (e) {
    e.preventDefault();
    $('.js-write-review-section').addClass('d-none');
    $('.js-write-review').attr('disabled', false);
});

// ratings stars input
$('.js-rating-stars-input').on('mouseleave', handleStarsInputVisually);
$('.js-rating-stars-input i').on({
    mouseenter: handleStarsInputVisually,
    click: function () {
        var clickedStarValue = parseInt($(this).data('value'), 10);
        if (clickedStarValue > 5 || clickedStarValue < 1) {
            return;
        }
        $('.js-rating-input').val(clickedStarValue);
    }
});
