'use strict';

$(document).ready(function () {
    var $storyCarousel = $('.home-story-carousel');
    var $trendingCarousel = $('.home-trending-carousel');
    var bg = '';
    if ($storyCarousel.length) {
        bg = $storyCarousel.data('mode');
        if (bg.length && bg === 'white') {
            $storyCarousel.addClass('home-story-carousel-white');
        }
    }

    if ($trendingCarousel.length) {
        bg = $trendingCarousel.data('mode');
        if (bg.length && bg === 'white') {
            $trendingCarousel.addClass('home-trending-carousel-white');
        }
    }
});
