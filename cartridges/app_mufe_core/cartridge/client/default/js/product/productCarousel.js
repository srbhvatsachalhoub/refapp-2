'use strict';

var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

/**
 * Initializes carousel
 * @param {jQuery} $container - Container element that holds carousel items
 */
function initCarousel($container) {
    carousel($container, {
        infinite: true,
        dots: false,
        nextArrow: nextArrow,
        prevArrow: prevArrow
    });
}

module.exports = function (selector) {
    selector = selector || '.js-product-carousel'; // eslint-disable-line
    $(selector).each(function () {
        initCarousel($(this));
    });

    $(selector + '-async').on('productLoader:loaded', function () {
        initCarousel($(this));
    });
};
