'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for the photoTileCustom component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.image = ImageTransformation.getScaledImage(content.image);
    model.imageTablet = content.imageTablet ? ImageTransformation.getScaledImage(content.imageTablet) : null;
    model.imageMobile = content.imageMobile ? ImageTransformation.getScaledImage(content.imageMobile) : null;
    model.fullWidth = content.fullWidth ? content.fullWidth : false;

    return new Template('experience/components/commerce_assets/photoTileCustom').render(model).text;
};
