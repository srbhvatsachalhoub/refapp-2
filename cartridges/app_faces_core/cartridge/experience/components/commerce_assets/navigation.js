'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for leftSideNavigation component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.text1 = content.text1 || null;
    model.link1 = content.link1 || '#';
    model.selected1 = content.selected1 || false;
    model.text2 = content.text2 || null;
    model.link2 = content.link2 || '#';
    model.selected2 = content.selected2 || false;
    model.text3 = content.text3 || null;
    model.link3 = content.link3 || '#';
    model.selected3 = content.selected3 || false;
    model.text4 = content.text4 || null;
    model.link4 = content.link4 || '#';
    model.selected4 = content.selected4 || false;
    model.text5 = content.text5 || null;
    model.link5 = content.link5 || '#';
    model.selected5 = content.selected5 || false;

    return new Template('experience/components/commerce_assets/navigation').render(model).text;
};
