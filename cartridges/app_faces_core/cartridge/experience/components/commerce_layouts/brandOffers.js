'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');


/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.textHeadline = content.textHeadline || '';
    model.title = content.title || '';
    model.alt = content.alt || '';
    model.backgroundColor = content.backgroundColor || '';
    model.description = content.description || '';
    model.image = ImageTransformation.getScaledImage(content.image);
    model.linkText = content.linkText1 || '';
    model.linkURL = content.link1 || '';
    model.buttonText = content.linkText2 || '';
    model.buttonLink = content.link2 || '';

    return new Template('experience/components/commerce_layouts/brandOffers').render(model).text;
};
