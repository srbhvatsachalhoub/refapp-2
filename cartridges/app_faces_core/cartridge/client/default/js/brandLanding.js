'use strict';

$(document).ready(function () {
    $('.all-brands-letter li a').on('click', function () {
        var $headerModalTop = $('.header').height();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - $headerModalTop
        });
    });
});
