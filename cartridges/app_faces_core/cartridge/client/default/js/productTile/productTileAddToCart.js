'use strict';

/**
 * Initialize swatch handler
 */
function initSwatchHandler() {
    $(document).off('click', '.js-swatch-item-link').delegate('.js-swatch-item-link', 'click', function (event) {
        event.preventDefault();

        var $currentSwatch = $(event.currentTarget);
        var url = $currentSwatch.attr('href');

        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json'
        }).done(function (response) {
            var product = response.product;
            var isAvailable = product.available;
            var isBackInStockNotificationEnabled = product.enableBackInStockNotification;

            var $productTile = $currentSwatch.closest('.js-product-tile');
            var $productTileImage = $productTile.find('.js-tile-image');
            var $notifyMeArea = $productTile.find('.js-back-in-stock-container');
            var $addToCartButton = $productTile.find('.js-pt-add-to-cart');
            var $soldOutButton = $productTile.find('.js-pt-sold-out');
            var $selectedSwatch = $productTile.find('.js-swatch-circle-wrapper.selected');
            var $priceContainer = $productTile.find('.price-container');

            $selectedSwatch.removeClass('selected');
            $currentSwatch.find('.js-swatch-circle-wrapper').addClass('selected');

            if (product.images.small && product.images.small.length) {
                $productTileImage.attr('src', product.images.small[0].url);
                $productTileImage.attr('alt', product.images.small[0].alt);
            }
            $productTile.data('pid', product.id);
            if (product && product.price && product.price.html) {
                var $priceHtml = $(product.price.html);
                $priceHtml.find('span:first').addClass('price-value');
                $priceContainer.html($priceHtml);
            }

            if (isAvailable) {
                $addToCartButton.removeClass('d-none');
                $soldOutButton.addClass('d-none');
                $notifyMeArea.addClass('d-none');

                $addToCartButton.data('pid', product.id);
            } else
            if (isBackInStockNotificationEnabled) {
                $notifyMeArea.removeClass('d-none');
                $soldOutButton.addClass('d-none');
                $addToCartButton.addClass('d-none');

                $notifyMeArea.find('.js-notify-me-form-product-id').val(product.id);
            } else {
                $soldOutButton.removeClass('d-none');
                $addToCartButton.addClass('d-none');
                $notifyMeArea.addClass('d-none');
            }

            $addToCartButton.attr('disabled', !product.available);
        });
    });
}

/**
 * Initialize back in stock
 */
function initBackInStock() {
    $(document).on('show.bs.modal', '.js-pt-notify-me-modal', function () {
        var $this = $(this);
        if (!$this.closest('.js-product-tile-container').length) {
            return;
        }

        var $container = $('<div class="back-in-stock-container"></div>');
        $container.append($this);
        $('body').append($container);
    });
}

module.exports = function () {
    $(document).ready(function () {
        initSwatchHandler();

        initBackInStock();
    });
};
