<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets');
        assets.addJs('/js/catLanding.js');
    </isscript>

    <div class="clp-breadcrumb">
        <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>
    </div>

    <isset name="category" value="${pdict.category}" scope="page" />
    <isset name="minSize" value="${category.custom.minNumberOfSubCategories ? category.custom.minNumberOfSubCategories : 6}" scope="page" />
    <isset name="maxSize" value="${category.custom.maxNumberOfSubCategories ? category.custom.maxNumberOfSubCategories : 10}" scope="page" />
    <div class="main-container main-container-clp container">
        <div class="clp-header">
            <h2 class="clp-title text-center text-uppercase">
                <isif condition="${category.custom.clpSubcategoryMenuTitle}">
                    ${category.custom.clpSubcategoryMenuTitle}
                <iselse/>
                    ${category.displayName}
                </isif>
            </h2>
        </div>
        <div class="clp-main">
            <div class="clp-info-container row">
                <div class="clp-subcategory-container col-12 col-md-6">
                    <isif condition="${category.description}">
                        <div class="clp-description">${category.description}</div>
                    </isif>
                    <div class="clp-subcategory-block">
                        <isloop items="${pdict.subCategoriesCLP}" var="subCategory" end="${minSize - 1}" status="loopstatus">
                            <isif condition="${subCategory.online}">
                                <div class="clp-subcategory">
                                    <a href="${subCategory.URL}" class="btn btn-link sc-icon-arrow">${subCategory.name}</a>
                                </div>
                            </isif>
                        </isloop>
                        <isif condition="${pdict.subCategoriesCLP.length > minSize}">
                            <div class="clp-subcategory js-btn-clp-action-more">
                                <a href="${'#'}" class="btn btn-link sc-icon-arrow"> ${Resource.msg('button.viewmore', 'common', null)}</a>
                            </div>

                            <div class="js-extra-categories extra-categories">
                                <isloop items="${pdict.subCategoriesCLP}" var="subCategory" begin ="${minSize}" end ="${maxSize - 1}" status="loopstatus">
                                    <isif condition="${subCategory.online}">
                                        <div class="clp-subcategory">
                                            <a href="${subCategory.URL}" class="btn btn-link sc-icon-arrow">${subCategory.name}</a>
                                        </div>
                                    </isif>
                                </isloop>

                                <div class="clp-subcategory js-btn-clp-action-less">
                                    <a href="${'#'}" class="btn btn-link sc-icon-arrow"> ${Resource.msg('button.viewless', 'common', null)}</a>
                                </div>
                            </div>
                        </isif>
                    </div>
                </div>
                <div class="clp-image-container d-none d-md-block col-md-6">
                    <img src="${category.custom.clpBannerImage.getURL()}" class="clp-image-right" alt="">
                </div>
            </div>
            <div class="clp-recommendation-area justify-content-center">
                <isslot id="cat-clp-recomendation-area1" context="category" description="First recomendation area with product carousel at the Category Landing Pages" context-object="${pdict.category}"/>
                <isslot id="cat-clp-recomendation-area2" context="category" description="Second recomendation area with product carousel at the Category Landing Pages" context-object="${pdict.category}"/>
                <isslot id="cat-clp-recomendation-area3" context="category" description="Third recomendation area with product carousel at the Category Landing Pages" context-object="${pdict.category}"/>
                <isslot id="cat-clp-recomendation-area4" context="category" description="Fourth recomendation area with product carousel at the Category Landing Pages" context-object="${pdict.category}"/>
            </div>
        </div>
    </div>
    <iscomment>
        This comment block is required to define following category based content slots.
        <isslot id="cat-main-menu-product" context="category" description="Product slot for mega menu" context-object="${pdict.category}"/>
        <isslot id="cat-main-menu-banner" context="category" description="Category banner slot for mega menu" context-object="${currentCategory.category}"/>
        <isslot id="cat-pdp-banner" context="category" description="Category banner slot pdp" context-object="${currentCategory.category}"/>
    </iscomment>
</isdecorate>
