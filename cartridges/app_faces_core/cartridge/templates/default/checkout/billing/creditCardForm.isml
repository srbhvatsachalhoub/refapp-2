<isinclude template="/components/modules" sf-toolkit="off" />
<isset name="creditFields" value="${pdict.forms.billingForm.creditCardFields}" scope="page"/>

<div class="form-row">

    <iscomment>Hidden input for credit card type</iscomment>
    <input type="hidden" class="form-control" id="cardType"
        name="${creditFields.cardType.htmlName}"
        value="" />

    <div class="col-md-6">
        <isinputtext    inputtext_formfield="${creditFields.cardOwner}"
                        inputtext_value="${pdict.order.billing.payment.selectedPaymentInstruments
                            && pdict.order.billing.payment.selectedPaymentInstruments.length > 0
                            ? pdict.order.billing.payment.selectedPaymentInstruments[0].owner||''
                            : ''}"
                        inputtext_missingerror="${Resource.msg('error.message.required.cardowner', 'forms', null)}"
                        inputtext_rangeerror="${Resource.msg('error.message.range.cardowner', 'forms', null)}"
                        inputtext_parseerror="${Resource.msg('error.message.parse.cardowner', 'forms', null)}"
        />
    </div>

    <div class="col-md-6">
        <div class="card-number-wrapper js-card-number-wrapper"
            data-ismada-applicable="${pdict.isMadaPaymentCardApplicable}">
            <isinputtext    inputtext_formfield="${creditFields.cardNumber}"
                            inputtext_autocomplete="cc-number"
                            inputtext_class="js-input-credit-card-number"
                            inputtext_value="${pdict.order.billing.payment.selectedPaymentInstruments
                                && pdict.order.billing.payment.selectedPaymentInstruments.length > 0
                                ? pdict.order.billing.payment.selectedPaymentInstruments[0].maskedCreditCardNumber||''
                                : ''}"
                            inputtext_missingerror="${Resource.msg('error.message.required.cardnumber', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.cardnumber', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.cardnumber', 'forms', null)}"
            />
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group
            <isif condition=${creditFields.expirationMonth.mandatory === true || creditFields.expirationYear.mandatory === true}>required</isif>
            ${creditFields.expirationMonth.htmlName} ${creditFields.expirationYear.htmlName}">
            <label class="form-control-label" for="expirationMonth">
                ${Resource.msg('field.credit.card.expiry.date','creditCard',null)}
            </label>
            <div class="row">
                <div class="col-6">
                    <select class="form-control expirationMonth custom-select" id="expirationMonth"
                        data-size="6"
                        data-width="auto"
                        data-missing-error="<isprint value="${Resource.msg('error.message.required.expirationMonth', 'forms', null)}" encoding="htmldoublequote" />"
                        <isprint value=${creditFields.expirationMonth.attributes} encoding="off"/> autocomplete="cc-exp-month">
                        <isloop items=${creditFields.expirationMonth.options} var="month">
                            <option id="${month.id}" value="${month.htmlValue}"
                                <isif condition="${pdict.order.billing.payment.selectedPaymentInstruments
                                    && pdict.order.billing.payment.selectedPaymentInstruments.length > 0
                                    && month.id == pdict.order.billing.payment.selectedPaymentInstruments[0].expirationMonth}">
                                    selected
                                </isif> >
                                <isif condition="${isNaN(parseInt(month.label)) === false}">
                                    ${month.label}
                                <iselse/>
                                    ${Resource.msg('field.credit.card.expiration.month.placeholder','creditCard',null)}
                                </isif>
                            </option>
                        </isloop>
                    </select>
                </div>
                <div class="col-6">
                    <select class="form-control expirationYear custom-select" id="expirationYear"
                        data-size="6"
                        data-width="auto"
                        data-missing-error="<isprint value="${Resource.msg('error.message.required.expirationYear', 'forms', null)}" encoding="htmldoublequote" />"
                        <isprint value=${creditFields.expirationYear.attributes} encoding="off"/> autocomplete="cc-exp-year">
                        <option value="${pdict.forms.billingForm.creditCardFields.expirationYear.options[0].htmlValue}">
                            ${Resource.msg('field.credit.card.expiration.year.placeholder','creditCard',null)}
                        </option>
                        <isloop items=${pdict.expirationYears} var="year">
                            <option id="${year.toFixed(0)}" value="${year.toFixed(0)}"
                                <isif condition="${pdict.order.billing.payment.selectedPaymentInstruments
                                    && pdict.order.billing.payment.selectedPaymentInstruments.length > 0
                                    && year == pdict.order.billing.payment.selectedPaymentInstruments[0].expirationYear}">
                                        selected
                                </isif> >
                            <isprint value="${year}" formatter="####" /></option>
                        </isloop>
                    </select>
                </div>
            </div>
            <div class="w-100">
                <div class="invalid-feedback"></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group
            <isif condition=${creditFields.securityCode.mandatory === true}>required</isif>
            ${creditFields.securityCode.htmlName}">
            <label class="form-control-label" for="securityCode">${Resource.msg('field.credit.card.security.code','creditCard',null)}</label>
            <div class="security-code-input-container">
                <div class="security-code-input">
                    <span class="security-code-input-icon info-icon">
                        <i class="sc-icon sc-icon-payment-methods"></i>
                        <div class="tooltip d-none">
                            ${Resource.msg('tooltip.security.code','creditCard',null)}
                        </div>
                    </span>
                    <input  type="text"
                            class="form-control securityCode" id="securityCode"
                            <isprint value=${creditFields.securityCode.attributes} encoding="off"/>
                            aria-describedby="securityCodeInvalidMessage"
                            data-missing-error="<isprint value="${Resource.msg('error.message.required.securityCode', 'forms', null)}" encoding="htmldoublequote" />"
                            data-range-error="<isprint value="${Resource.msg('error.message.range.securityCode', 'forms', null)}" encoding="htmldoublequote" />"
                            data-pattern-mismatch="<isprint value="${Resource.msg('error.message.parse.securityCode', 'forms', null)}" encoding="htmldoublequote" />"
                    />
                </div>
                <small class="form-text">${Resource.msg('tooltip.security.code','creditCard',null)}</small>
            </div>
            <div class="invalid-feedback" id="securityCodeInvalidMessage"></div>
        </div>
    </div>

    <iscomment>Billing Address</iscomment>
    <fieldset class="billing-address col-12">
        <div class="js-form-billing-address d-none">
            <h2 class="billing-address-title mb-3 text-uppercase">${Resource.msg('heading.billing.address', 'checkout', null)}</h2>
            <isinclude template="checkout/billing/billingAddress" />
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <input type="checkbox"
                        id="useShippingAddressAsBillingAddress"
                        class="js-checkbox-sync-shipping-and-billing" />
                    <label class="form-control-label" for="useShippingAddressAsBillingAddress">
                        ${Resource.msg('check.shipping.billing.same', 'checkout', null)}
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <isif condition="${!pdict.order.billing.payment.isSaveCardDisabled}">
        <isif condition="${pdict.customer.registeredUser}">
            <div class="col-12">
                <div class="form-group mb-0">
                    <div class="save-credit-card">
                        <input type="checkbox" class="form-control" id="saveCreditCard" name="${creditFields.saveCard.htmlName}" value="true" />
                        <label class="form-control-label" for="saveCreditCard">
                            ${creditFields.saveCard.label}
                        </label>
                    </div>
                </div>
            </div>
        </isif>
    </isif>
</div>
