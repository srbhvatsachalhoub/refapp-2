'use strict';

module.exports = {
    option: {
        mobileConfig:
            {
                slidesToShow: 2,
                slidesToScroll: 2,
                rows: 1,
                dots: false,
                arrows: true
            },
        tabletConfig:
            {
                slidesToShow: 2,
                slidesToScroll: 2,
                rows: 1,
                dots: false,
                arrows: true
            },
        desktopConfig:
            {
                slidesToShow: 4,
                slidesToScroll: 4,
                rows: 1,
                dots: false,
                arrows: true
            }
    }
};
