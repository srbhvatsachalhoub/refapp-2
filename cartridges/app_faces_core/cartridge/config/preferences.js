'use strict';

module.exports = {
    maxOrderQty: 10,
    defaultPageSize: 8
};
