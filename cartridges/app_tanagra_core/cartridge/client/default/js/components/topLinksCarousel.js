'use strict';

var carousel = require('brand_core/components/carousel');

module.exports = function () {
    if (window.innerWidth < window.RA_BREAKPOINTS.md) {
        carousel($('.owl-carousel'), {
            infinite: true,
            slidesToShow: 1,
            autoplay: true
        });
    }
};
