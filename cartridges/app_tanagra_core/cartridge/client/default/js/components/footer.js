'use strict';

module.exports = function () {
    $('.actions-row .actions').click(function () {
        $(this).toggleClass('open');
    });

    $('.page').css('padding-bottom', $('footer').height() + 35);
};
