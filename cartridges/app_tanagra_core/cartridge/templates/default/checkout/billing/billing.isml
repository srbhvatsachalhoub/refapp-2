<div class="card payment-form">
    <div class="card-body">
        <form autocomplete="on" method="POST" action="${URLUtils.url('CheckoutServices-SubmitPayment')}"
            data-address-mode="${!pdict.order.billing.billingAddress.address ? 'new' : 'edit'}"
            <isprint value=${pdict.forms.billingForm.attributes} encoding="off" /> novalidate>

            <iscomment>Payment Options</iscomment>
            <fieldset class="billing-payment-options-block ${pdict.order && pdict.order.totals && pdict.order.totals.nonComplimentaryPaymentAmount && pdict.order.totals.nonComplimentaryPaymentAmount.value <= 0 ? 'd-none' : ''}">
                <isinclude template="checkout/billing/paymentOptions" />
            </fieldset>

            <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')}">
                <isinclude template="giftCard/checkout/billing/giftCardCodeForm" />
            </isif>

            <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>

            <iscomment>Billing Address Selector</iscomment>
            <fieldset class="address-selector-block">
                <div class="form-group">
                    <h4 class="hidden-sm-down">${Resource.msg('heading.billing.address', 'checkout', null)}</h4>
                    <h4 class="container-title d-md-none">${Resource.msg('heading.billing.address', 'checkout', null)}</h4>

                    <isinclude template="checkout/billing/addressSelector" />
                    <div class="invalid-feedback"></div>
                    <div class="row d-none">
                        <a class="col-6 text-center btn-show-details" href="javascript:;">${Resource.msg('action.update.address', 'checkout', null)}</a>
                        <a class="col-6 text-center btn-add-new"  href="javascript:;">${Resource.msg('action.add.new', 'checkout', null)}</a>
                    </div>
                </div>
            </fieldset>

            <iscomment>Billing Address</iscomment>
            <fieldset class="billing-address">
                <div class="fieldset-content">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="checkbox"
                                    id="useShippingAddressAsBillingAddress"
                                    class="js-checkbox-sync-shipping-and-billing" />
                                <label class="form-control-label font-weight-light" for="useShippingAddressAsBillingAddress">
                                    ${Resource.msg('check.shipping.billing.same', 'checkout', null)}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="js-form-billing-address d-none">
                        <isinclude template="checkout/billing/billingAddress" />
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <div class="order-total-summary-mobile d-md-none">
        <h2 class="container-title">${Resource.msg('heading.order.summary', 'checkout', null)}</h2>
        <div class="order-total-summary">
            <isinclude template="checkout/orderTotalSummary" />
        </div>
    </div>

    <isinclude template="checkout/orderProductSummary" />

    <div class="card shipping-summary">
        <h4 class="hidden-sm-down">${Resource.msg('label.shipping.address', 'checkout', null)}</h4>
        <h4 class="container-title d-md-none">
            ${Resource.msg('label.shipping.address', 'checkout', null)}
        </h4>
        <div class="card-body">
            <isinclude template="checkout/shipping/shippingSummary" />
        </div>
    </div>
</div>
