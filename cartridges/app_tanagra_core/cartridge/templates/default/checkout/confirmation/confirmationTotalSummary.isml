<iscomment>Subtotal</iscomment>
<div class="row">
    <div class="col-6">
        <p class="order-receipt-label">
            <span>${Resource.msg('label.order.subtotal','confirmation', null)}</span>
        </p>
    </div>
    <div class="col-6">
        <p class="text-right">
            <span class="sub-total">${pdict.order.totals.subTotalExcludingVAT}</span>
        </p>
    </div>
</div>

<iscomment>Order Discount</iscomment>
<div class="row <isif condition="${pdict.order.totals.orderLevelDiscountTotal.value === 0}">hide-order-discount</isif>">
    <div class="col-6">
        <p class="order-receipt-label">
            <span>${Resource.msg('label.order.discount', 'common', null)}</span>
        </p>
    </div>
    <div class="col-6">
        <p class="text-right">
            <span class="order-discount-total">- ${pdict.order.totals.orderLevelDiscountTotal.formatted}</span>
        </p>
    </div>
</div>

<iscomment>Shipping Cost</iscomment>
<div class="row">
    <div class="col-6">
        <p class="order-receipt-label">
            <span>${Resource.msg('label.order.shipping.cost', 'confirmation', null)}</span>
        </p>
    </div>
    <div class="col-6">
        <p class="text-right">
            <span class="shipping-total-cost">${pdict.order.totals.totalShippingCost}</span>
        </p>
    </div>
</div>

<iscomment>Shipping Discount</iscomment>
<isif condition="${pdict.order.totals.shippingLevelDiscountTotal && pdict.order.totals.shippingLevelDiscountTotal.value > 0 && pdict.order.totals.shippingLevelDiscountTotal.value < pdict.order.totals.shippingLevelDiscountTotal.shippingTotalValue}">
    <div class="row">
        <div class="col-6">
            <p class="order-receipt-label">
                <span>${Resource.msg('label.shipping.discount', 'common', null)}</span>
            </p>
        </div>
        <div class="col-6">
            <p class="text-right">
                <span class="shipping-discount-total">- ${pdict.order.totals.shippingLevelDiscountTotal.formatted}</span>
            </p>
        </div>
    </div>
</isif>

<iscomment>Payment Service Fee</iscomment>
<isif condition="${pdict.order.totals.serviceFee && pdict.order.totals.serviceFee.value > 0}">
    <div class="row">
        <div class="col-6">
            <p class="order-receipt-label">
                <span>${Resource.msg('payment.service.fee', 'order', 'Payment Service Fee')}</span>
            </p>
        </div>
        <div class="col-6">
            <p class="text-right">
                <span class="payment-service-fee-total">${ pdict.order.totals.serviceFee && pdict.order.totals.serviceFee.formatted ? pdict.order.totals.serviceFee.formatted : '' }</span>
            </p>
        </div>
    </div>
</isif>

<iscomment>Total Tax</iscomment>
<div class="totaltax-item">
    <div class="row">
        <div class="col-6">
            <p class="order-receipt-label"><span>${Resource.msgf('label.order.sales.tax','confirmation', null, pdict.order.totals.taxRate)}</span></p>
        </div>
        <div class="col-6">
            <p class="text-right"><span class="tax-total">${pdict.order.totals.totalTax}</span></p>
        </div>
    </div>
</div>

<hr/>

<iscomment>Grand Total</iscomment>
<div class="row">
    <div class="col-6">
        <p class="order-receipt-label confirmation-total-line">
            <span class="total-label">${Resource.msg('label.order.grand.total','confirmation', null)}</span>
        </p>
        <p class="confirmation-total-include">
            <isif condition="${'taxRate' in pdict.order.totals}">
                (${Resource.msgf('label.including.x.percent.vat', 'account', null, pdict.order.totals.taxRate)})
            <iselse/>
                (${Resource.msg('label.including.vat', 'account', null)})
            </isif>
        </p>
    </div>
    <div class="col-6">
        <p class="text-right">
            <span class="grand-total-sum">${pdict.order.totals.grandTotal}</span>
        </p>
    </div>
</div>
