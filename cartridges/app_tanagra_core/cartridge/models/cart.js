'use strict';

var base = module.superModule;

/**
 * Gets the free shipping approaching product tile params
 * @returns {Object} the product tile parameters object
 */
function getFreeShippingApproachingProductTileParams() {
    return {
        pview: 'tile',
        ratings: 'true',
        swatches: 'false',
        showBadges: 'true',
        showQuickView: 'false',
        showReviewCount: 'true',
        showShopNowButton: 'false',
        showAddToBagButton: 'true'
    };
}

base.getFreeShippingApproachingProductTileParams = getFreeShippingApproachingProductTileParams;
module.exports = base;
