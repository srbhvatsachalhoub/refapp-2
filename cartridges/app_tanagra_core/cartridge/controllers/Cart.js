'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Calculates the unit and total prices of items
 * that are just added to cart.
 * Extended to disable to provide bonusDiscountLineItem
 */
server.append('AddProduct', function (req, res, next) {
    var viewData = res.getViewData();

    // disable pdp bonus items popup in loccitane
    viewData.newBonusDiscountLineItem = {};

    res.setViewData(viewData);

    next();
});


/**
 * Extended with breadcrumbs object
 *
 */
server.append(
    'Show',
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');
        var Resource = require('dw/web/Resource');
        var viewData = res.getViewData();

        // extend view data
        viewData.breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('title.cart', 'cart', null)
            }
        ];

        // set view data
        res.setViewData(viewData);

        next();
    }
);

module.exports = server.exports();
