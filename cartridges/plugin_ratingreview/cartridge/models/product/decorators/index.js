'use strict';

var base = module.superModule;
base.ratingSummary = require('*/cartridge/models/product/decorators/ratingSummary');

module.exports = base;
