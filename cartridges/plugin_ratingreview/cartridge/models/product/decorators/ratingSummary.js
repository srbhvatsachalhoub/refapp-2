'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'ratingSummary', {
        enumerable: true,
        value: (function () {
            return {
                averageRating: apiProduct.custom.averageRating ? apiProduct.custom.averageRating : 0,
                ratingCount: apiProduct.custom.ratingCount ? apiProduct.custom.ratingCount : 0
            };
        }())
    });
};
