'use strict';

var isGiftCard = require('*/cartridge/models/product/decorators/isGiftCard');
var base = module.superModule;

/**
 * Decorate product with full product information
 * Extend the base model and add the giftCard related attributes
 *
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
function fullProduct(product, apiProduct, options) {
    base.call(this, product, apiProduct, options);
    isGiftCard(product, apiProduct);
    return product;
}

module.exports = fullProduct;
