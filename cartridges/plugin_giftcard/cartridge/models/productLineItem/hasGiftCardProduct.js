'use strict';

var collections = require('*/cartridge/scripts/util/collections');

/**
 * Checks productLineItems if any of the product is gift card product
 * @param {dw.util.Collection} productLineItems - the productLineItems for order/basket/shipment
 * @returns {boolean} true if any of them is gift card product, false otherwise
 */
function hasGiftCardProduct(productLineItems) {
    if (!productLineItems) {
        return false;
    }

    // check current (basket/order/shipment)'s productlineitems
    // if there is any product as marked with gift card(giftCard product)
    return collections.some(productLineItems, function (pli) {
        return pli.gift;
    });
}

module.exports = function (object, productLineItems) {
    Object.defineProperty(object, 'hasGiftCardProduct', {
        enumerable: true,
        writable: true,
        value: hasGiftCardProduct(productLineItems)
    });
};
