'use strict';

var base = module.superModule;

var hasGiftCardProduct = require('*/cartridge/models/productLineItem/hasGiftCardProduct');

/**
 * Order class that represents the current order
 * Extend with hasGiftCardProduct
 * Check and set the any of productlineitems is the gift card product
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    base.call(this, lineItemContainer, options);

    // define hasGiftCardProduct
    hasGiftCardProduct(this, lineItemContainer ? lineItemContainer.productLineItems : null);
}

module.exports = OrderModel;
