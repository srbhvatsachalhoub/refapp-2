'use strict';
/* global request */

/**
 * Gets the product availability model for the given locale inventory
 * @param {dw.catalog.Product} product - the product to get availabilitymodel
 * @returns {dw.catalog.ProductAvailabilityModel} the product availability model for the given locale
 */
function getProductAvailabilityModel(product) {
    return product.getAvailabilityModel();
}

module.exports = {
    getProductAvailabilityModel: getProductAvailabilityModel
};
