'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var Money = require('dw/value/Money');

/**
 * get the total price for the product line item
 * @param {dw.value.Money} money - Money
 * @returns {string} Formatted money
 */
function getFormattedMoney(money) {
    return renderTemplateHelper.getRenderedHtml({ price: money }, 'product/components/pricing/currencyFormatted');
}

/**
 * get the total price for the product line item
 * @param {dw.order.ProductLineItem} lineItem - API ProductLineItem instance
 * @returns {Object} an object containing the product line item total info.
 */
function getInvoicePrice(lineItem) {
    var context;
    var totalPriceInclVat;
    var result = {};
    var template = 'checkout/productCard/productCardProductRenderedTotalPrice';

    totalPriceInclVat = lineItem.adjustedPrice;

    // The platform does not include prices for selected option values in a line item product's
    // price by default.  So, we must add the option price to get the correct line item total price.
    collections.forEach(lineItem.optionProductLineItems, function (item) {
        totalPriceInclVat = totalPriceInclVat.add(item.adjustedNetPrice);
    });

    result.unitPriceExclVat = getFormattedMoney(new Money((totalPriceInclVat - lineItem.adjustedTax) / lineItem.quantityValue, totalPriceInclVat.currencyCode));
    result.totalPriceExclVat = getFormattedMoney(new Money(totalPriceInclVat - lineItem.adjustedTax, totalPriceInclVat.currencyCode));
    result.totalVatAmount = getFormattedMoney(lineItem.adjustedTax);
    result.totalPriceInclVat = getFormattedMoney(totalPriceInclVat);

    context = {
        lineItem: {
            UUID: lineItem.UUID,
            priceTotal: result
        }
    };

    result.decimalPrice = totalPriceInclVat.decimalValue;
    result.renderedPrice = renderTemplateHelper.getRenderedHtml(context, template);

    return result;
}


module.exports = function (object, lineItem) {
    Object.defineProperty(object, 'invoicePrice', {
        enumerable: true,
        value: getInvoicePrice(lineItem)
    });
};
