'use strict';

var collections = require('*/cartridge/scripts/util/collections');

/**
 * Checks if the productLineItems has bonus products in it
 * @param {dw.util.Collection} productLineItems - the productLineItems for order/basket/shipment
 * @returns {boolean} true if productLineItems has bonus products, false otherwise
 */
function hasBonusProduct(productLineItems) {
    if (!productLineItems) {
        return false;
    }

    return collections.some(productLineItems, function (pli) {
        return pli.bonusProductLineItem;
    });
}

module.exports = function (object, productLineItems) {
    Object.defineProperty(object, 'hasBonusProduct', {
        enumerable: true,
        value: hasBonusProduct(productLineItems)
    });
};
