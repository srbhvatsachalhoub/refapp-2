'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var ProductFactory = require('*/cartridge/scripts/factories/product');
var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * Generates the count of the discount line item to determine if it is full
 * @param {dw.order.BonusDiscountLineItem} item - a product line item
 * @returns {number} the total number of bonus products from within one bonus discount line item
 */
function countBonusProductLineItems(item) {
    var count = 0;

    collections.forEach(item.bonusProductLineItems, function (bonusDiscountLineItem) {
        count += bonusDiscountLineItem.quantityValue;
    });

    return count;
}

/**
 * Creates an array of bonus discount line items
 * @param {dw.util.Collection<dw.order.BonusDiscountLineItem>} bonusDiscountLineItems - All bonus discount line items of the basket
 * @returns {Array} an array of bonus discount line items.
 */
function createBonusDiscountLineItemsObject(bonusDiscountLineItems) {
    var lineItems = [];

    collections.forEach(bonusDiscountLineItems, function (item) {
        if (!item.promotion || !item.bonusProducts || item.bonusProducts.isEmpty()) { return; }

        var bonusDiscountLineItem = {
            name: item.promotion.name,
            full: countBonusProductLineItems(item) === item.maxBonusItems,
            maxpids: item.maxBonusItems,
            bonusProducts: [],
            addToCartUrl: URLUtils.url('Cart-UpdateNonNestedBonusProducts').toString(),
            uuid: item.UUID,
            labels: {
                select: Resource.msg('bonus.discount.item.label.select', 'cart', null),
                selected: Resource.msg('bonus.discount.item.label.selected', 'cart', null)
            }
        };

        collections.forEach(item.bonusProducts, function (bonusProduct) {
            var availabilityModel = inventoryHelpers.getProductAvailabilityModel(bonusProduct);
            if (bonusProduct && availabilityModel && !availabilityModel.orderable) {
                return;
            }

            var product = ProductFactory.get({
                pid: bonusProduct.ID,
                pview: 'bonus',
                duuid: item.UUID
            });
            product.description = bonusProduct.shortDescription && bonusProduct.shortDescription.markup
                ? bonusProduct.shortDescription.markup : '';

            product.selected = !!collections.find(item.bonusProductLineItems, function (bpli) {
                return bpli.productID === bonusProduct.ID;
            });

            bonusDiscountLineItem.bonusProducts.push(product);
        });

        lineItems.push(bonusDiscountLineItem);
    });

    return lineItems;
}

/**
 * @constructor
 * @classdesc class that represents a collection of bonus discount line items
 * bonus discount line items in current basket
 *
 * @param {dw.order.LineItemCtnr} lineItemCtnr - the current lineItemCtnr (basket or order)
 */
function BonusDiscountLineItems(lineItemCtnr) {
    if (lineItemCtnr) {
        return createBonusDiscountLineItemsObject(lineItemCtnr.getBonusDiscountLineItems());
    }

    return [];
}

module.exports = BonusDiscountLineItems;
