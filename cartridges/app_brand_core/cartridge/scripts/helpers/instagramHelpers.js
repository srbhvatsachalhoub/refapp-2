'use strict';
var instagramService = require('*/cartridge/scripts/services/instagramService');

/**
 * Gets Instagram Media
 * @param {string} user username
 * @param {string} maxId next page cursor.
 * @returns {Object} ig result user node.
 */
function getInstagramMedia(user, maxId) {
    var payload = {
        user: user
    };
    if (maxId) {
        payload.max_id = maxId;
    }
    var instagramServiceResult = instagramService.call(payload);
    if (instagramServiceResult.isOk()) {
        var result = instagramServiceResult.object;
        if ('graphql' in result && 'user' in result.graphql) {
            return result.graphql.user;
        }
    }
    return null;
}

module.exports = {
    getInstagramMedia: getInstagramMedia
};
