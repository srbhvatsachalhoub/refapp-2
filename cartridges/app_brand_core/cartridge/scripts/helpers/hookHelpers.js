'use strict';

/**
 * Finds backinstock flag for a giving product id
 * @param {string} pid product id to check if backinstock functionality is enabled
 * @returns {string} backinstock flag true/false
 */
function isBackInStockEnabled(pid) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var isEnabled = false;

    if (!pid) {
        return isEnabled;
    }

    var product = ProductMgr.getProduct(pid);
    if (!product) {
        return isEnabled;
    }
    var availabilityModel = inventoryHelpers.getProductAvailabilityModel(product);
    if (availabilityModel && !availabilityModel.inStock) {
        if ('enableBackInStockNotification' in product.custom && product.custom.enableBackInStockNotification) {
            isEnabled = true;
        }

        if (!isEnabled) {
            // if this is enabled at category level
            var categories = product.getAllCategories();
            if (categories) {
                var collections = require('*/cartridge/scripts/util/collections');
                collections.forEach(categories, function (category) {
                    if ('enableBackInStockNotification' in category.custom &&
                    category.custom.enableBackInStockNotification) {
                        isEnabled = true;
                    }
                });
            }
        }
    }

    return isEnabled;
}

/**
 * Adds swatch information
 * @param {dw.catalog.Product} apiProduct  product to be used for information
 * @param {Object} productItem productItem to be extended
 */
function addSwatches(apiProduct, productItem) {
    var HashMap = require('dw/util/HashMap');
    var collections = require('*/cartridge/scripts/util/collections');
    var ProductImageDIS = require('*/cartridge/scripts/helpers/productImageDIS');

    var variationModel = apiProduct.variationModel;
    var copyVariationAttributes = [];

    if ('variationAttributes' in productItem && productItem.variationAttributes) {
        for (var i = 0; i < productItem.variationAttributes.length; i++) {
            var variationAttribute = productItem.variationAttributes[i];
            var copyVariationAttribute = { id: variationAttribute.id, name: variationAttribute.name, values: [] };

            if (!variationModel || !variationAttribute || !variationAttribute.values) {
                return;
            }

            var filter = new HashMap();
            for (var j = 0; j < variationAttribute.values.length; j++) {
                var value = variationAttribute.values[j];

                filter.put(variationAttribute.id, value.value);
                var variants = variationModel.getVariants(filter);
                filter.remove(variationAttribute.id);

                if (variants.length) {
                    collections.forEach(variants, function (currentVariant) { // eslint-disable-line
                        var apiImages = ProductImageDIS.getImages(currentVariant, 'swatch');
                        if (!apiImages || apiImages.length === 0) {
                            return;
                        }
                        copyVariationAttribute.values.push({
                            httpsURL: apiImages[0].getHttpsURL(),
                            alt: apiImages[0].getAlt(),
                            title: apiImages[0].getTitle(),
                            orderable: value.orderable,
                            name: value.name,
                            value: value.value
                        });
                    });
                }
            }
            copyVariationAttributes.push(copyVariationAttribute);
        }
        productItem.c_variationAttributes = copyVariationAttributes; // eslint-disable-line
    }
}

/**
 * Adjusts given object
 * @param {String} productId product id
 * @param {Object} productItem product factory
 * @param {Object} productType productType
 */
/* eslint-disable */
function adjustProductAttributes(productId, productItem, productType) {

    var ProductMgr = require('dw/catalog/ProductMgr');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var ProductImageDIS = require('*/cartridge/scripts/helpers/productImageDIS');

    var params = productType ? { pview: productType } : {};
    params.pid = productId;
    var factoryProduct = ProductFactory.get(params);
    if (!factoryProduct) {
        return;
    }

    var apiProduct = ProductMgr.getProduct(productId);
    if (!apiProduct) {
        return;
    }

    if (!('productName' in productItem)) {
        productItem.c_name = apiProduct.name;
    }
    productItem.c_assignedToSiteCatalog = apiProduct.assignedToSiteCatalog;

    if ('secondaryName' in apiProduct.custom) {
        productItem.c_secondaryName = apiProduct.custom.secondaryName;
    }
    // inventory
    productItem.c_ATS = apiProduct.availabilityModel && apiProduct.availabilityModel.inventoryRecord &&
        apiProduct.availabilityModel.inventoryRecord.ATS ? apiProduct.availabilityModel.inventoryRecord.ATS.value : null;

    productItem.c_allocation = apiProduct.availabilityModel && apiProduct.availabilityModel.inventoryRecord &&
        apiProduct.availabilityModel.inventoryRecord.allocation ? apiProduct.availabilityModel.inventoryRecord.allocation.value : null;

    productItem.c_maxOrderQuantity = factoryProduct.maxOrderQuantity ? factoryProduct.maxOrderQuantity : null;

    var disImage = ProductImageDIS.getImage(apiProduct, 'small');
    if (disImage) {
        disImage = disImage.getURL();
    }
    productItem.c_image = disImage;

    if (!('productName' in productItem) && (!('name' in productItem) || !productItem.name)) {
        productItem.name = apiProduct.name;
    }

    if (apiProduct.variant) {
        productItem.c_master_id = apiProduct.masterProduct && apiProduct.masterProduct.ID ?  apiProduct.masterProduct.ID : null;

        if (factoryProduct.selectedVariationAttributes) {
            for (var i = 0; i < factoryProduct.selectedVariationAttributes.length; i++) {
                var selectedAttribute = factoryProduct.selectedVariationAttributes[i];
                if (selectedAttribute.selectedValue && selectedAttribute.selectedValue) {
                    productItem['c_' + selectedAttribute.id] = selectedAttribute.selectedValue && 'displayValue' in selectedAttribute.selectedValue ? selectedAttribute.selectedValue.displayValue : null;
                }
            }
        }
    }

    if (factoryProduct.variationAttributes) {
        for (var i = 0; i < factoryProduct.variationAttributes.length; i++) {
            var selectedAttribute = factoryProduct.variationAttributes[i];
            if (selectedAttribute.selectedValue && selectedAttribute.selectedValue.displayValue) {
                productItem['c_' + selectedAttribute.id] = selectedAttribute.selectedValue && 'displayValue' in selectedAttribute.selectedValue ? selectedAttribute.selectedValue.displayValue : null;
            }
        }
    }

    addSwatches(apiProduct, productItem);

    productItem.c_backInStockEnabled = isBackInStockEnabled(productId);
    productItem.c_brand = factoryProduct.brand;
    productItem.c_badges = JSON.stringify(factoryProduct.badges);
    productItem.c_price = factoryProduct.price;
    productItem.c_listPrice = null;
    if (factoryProduct.price && factoryProduct.price.type && factoryProduct.price.type  === 'range') {
        productItem.c_listPrice = factoryProduct.price.min && factoryProduct.price.min.list && factoryProduct.price.min.list.decimalPrice ? factoryProduct.price.min.list.decimalPrice : null;
    }
    else if (factoryProduct.price && factoryProduct.price.list && factoryProduct.price.list.decimalPrice) {
        productItem.c_listPrice = factoryProduct.price.list.decimalPrice;
    }
}
/* eslint-enable */

/**
 * Adds extra product information
 * @param {Object[]} productItems  productItems to be extended
 * @param {Object[]} productType  view type
 */
function addProductInformation(productItems, productType) {
    for (var i = 0; i < productItems.length; i++) {
        var productItem = productItems[i];
        var productId = 'productId' in productItem ? productItem.productId : productItem.id;
        adjustProductAttributes(productId, productItem, productType);
    }
}

/**
 * Adds extra order information
 * @param {Object[]} order  productItems to be extended
 * @param {Object[]} orderResponse  view type
 */
/* eslint-disable */
function addOrderInformation(order, orderResponse) {
    var siteId = orderResponse.site_id;
    var parts = siteId.split('_');
    var siteIdWIthoutCountry = parts[0];
    orderResponse.c_siteId = orderResponse.site_id;
    orderResponse.site_id = siteIdWIthoutCountry;
    orderResponse.customer_info.c_phone_mobile = order.getBillingAddress().getPhone();
    orderResponse.customer_info.c_first_name = order.billingAddress.firstName;
    orderResponse.customer_info.c_last_name = order.billingAddress.lastName;
}
/* eslint-enable */

/**
 * Authorizes hook payments.
 * @param {dw.order.Order} order - the order
 * @return {dw.system.Status} returns a status code
 */
function authorizePayment(order) {
    var Status = require('dw/system/Status');
    var Resource = require('dw/web/Resource');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

    var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo);

    if (handlePaymentResult.error) {
        return new Status(Status.ERROR, Resource.msg('error.technical', 'checkout', null));
    }

    if (handlePaymentResult.redirect && handlePaymentResult.redirectUrl) {
        return new Status(Status.ERROR, handlePaymentResult.redirectUrl);
    }

    var fraudDetectionStatus = hooksHelper('app.fraud.detection', 'fraudDetection', order, require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection);
    COHelpers.placeOrder(order, fraudDetectionStatus);

    return new Status(Status.OK);
}

module.exports = {
    addProductInformation: addProductInformation,
    adjustProductAttributes: adjustProductAttributes,
    authorizePayment: authorizePayment,
    addOrderInformation: addOrderInformation
};

