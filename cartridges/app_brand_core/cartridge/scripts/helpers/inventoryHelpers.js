'use strict';
/* global request */

var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Locale = require('dw/util/Locale');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');

var inventoriesJSON = null;
var inventoryList = null;

/**
 * Gets the defined inventories json object
 * if not defined returns null
 * sample:
 * {
 * 	    "locale1": "inventory-list-id1",
	    "locale2": "inventory-list-id2",
	    ...
 * }
 *
 * sample:
 * {
 *      "en_AE": "loccitane-inventory_en-AE",
 *      "ar_AE": "loccitane-inventory_ar-AE",
 * 	    ...
 * }
 *
 * sample:
 * {
 *    	"*_AE": "loccitane-inventory_AE",
 *  	"*_SA": "loccitane-inventory_SA",
 *  	...
 * }
 *
 * @returns {Object} the defined inventories JSON object
 */
function getInventoriesObject() {
    if (inventoriesJSON) {
        return inventoriesJSON;
    }

    var inventories = Site.current.getCustomPreferenceValue('inventories');
    if (!inventories) {
        return null;
    }

    inventoriesJSON = JSON.parse(inventories);

    return inventoriesJSON;
}

/**
 * Checks if the site use multiple inventory list
 * returns true if site uses multiple inventory list, false otherwise
 * @returns {boolean} true if current site use multiple inventory list, false otherwise
 */
function hasSiteMultipleInventory() {
    var inventories = getInventoriesObject();
    return (!!inventories && Object.keys(inventories).length > 1);
}

/**
 * Gets the locale inventory list id if defined
 * if not defined, returns the current site's assigned inventory list id
 * @param {string} localeId - the locale id string -> en_US, en_AE, ar_SA...
 * @returns {string} the inventory list id defined for the given locale
 */
function getInventoryListId(localeId) {
    if (!localeId) {
        return null;
    }

    var inventories = getInventoriesObject();
    if (!inventories || !Object.keys(inventories).length) {
        return null;
    }

    if (inventories[localeId]) {
        return inventories[localeId];
    }

    var locale = Locale.getLocale(localeId);
    if (!locale) {
        return null;
    }

    var wildcardCountry = StringUtils.format('*_{0}', locale.country);
    if (inventories[wildcardCountry]) {
        return inventories[wildcardCountry];
    }

    return null;
}

/**
 * Gets the locale inventory list if defined
 * if not defined, returns the current site's assigned inventory list
 * @param {string} localeId - the locale id string -> en_US, en_AE, ar_SA...
 * @param {boolean} forceToLoad - the boolean indicator to force to load the inventory list
 * @returns {dw.catalog.ProductInventoryList} the product inventory list defined for the given locale
 */
function getInventoryList(localeId, forceToLoad) {
    if (inventoryList && !forceToLoad) {
        return inventoryList;
    }

    if (!hasSiteMultipleInventory()) {
        inventoryList = ProductInventoryMgr.getInventoryList();
        return inventoryList;
    }

    if (!localeId) {
        localeId = request.locale; // eslint-disable-line no-param-reassign
    }

    var inventoryListId = getInventoryListId(localeId);
    if (!inventoryListId) {
        inventoryList = ProductInventoryMgr.getInventoryList();
        return inventoryList;
    }

    var tempInventoryList = ProductInventoryMgr.getInventoryList(inventoryListId);
    if (!tempInventoryList) {
        inventoryList = ProductInventoryMgr.getInventoryList();
        return inventoryList;
    }

    inventoryList = tempInventoryList;
    return inventoryList;
}

/**
 * Gets the product availability model for the given locale inventory
 * @param {dw.catalog.Product} product - the product to get availabilitymodel
 * @param {string} localeId - the locale id string -> en_US, en_AE, ar_SA...
 * @param {boolean} forceToLoad - the boolean indicator to force to load the inventory list
 * @returns {dw.catalog.ProductAvailabilityModel} the product availability model for the given locale
 */
function getProductAvailabilityModel(product, localeId, forceToLoad) {
    if (!localeId) {
        localeId = request.locale; // eslint-disable-line no-param-reassign
    }

    var productInventoryList = getInventoryList(localeId, forceToLoad);
    if (!productInventoryList) {
        return product.getAvailabilityModel();
    }

    return product.getAvailabilityModel(productInventoryList);
}

/**
 * Sets the site/locale inventory list as the product line item inventory context.
 * @param {dw.order.ProductLineItem} productLineItem - the current product line item
 */
function setProductInventoryList(productLineItem) {
    if (!productLineItem || !hasSiteMultipleInventory()) {
        return;
    }

    var pliProductInventoryListID = productLineItem.getProductInventoryListID();
    var localeInventoryList = getInventoryList();
    if (!localeInventoryList || localeInventoryList.ID === pliProductInventoryListID) {
        return;
    }

    Transaction.wrap(function () {
        productLineItem.setProductInventoryList(localeInventoryList);
    });
}

/**
 * Gets the current basket
 * Checks the all product line items in the current basket
 * Checks if all plis have correct product inventory list
 * If not then sets the correct one
 */
function checkBasketProductInventoryList() {
    if (!hasSiteMultipleInventory()) {
        return;
    }

    var BasketMgr = require('dw/order/BasketMgr');

    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        return;
    }

    var collections = require('*/cartridge/scripts/util/collections');

    collections.forEach(currentBasket.allProductLineItems, function (productLineItem) {
        setProductInventoryList(productLineItem);
    });
}

module.exports = {
    getInventoryList: getInventoryList,
    getProductAvailabilityModel: getProductAvailabilityModel,
    setProductInventoryList: setProductInventoryList,
    checkBasketProductInventoryList: checkBasketProductInventoryList,
    hasSiteMultipleInventory: hasSiteMultipleInventory
};
