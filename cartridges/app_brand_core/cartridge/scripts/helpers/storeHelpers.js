'use strict';
/* global request */

var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

/**
 * Gets the current local's country code
 * @returns {string} the current country code
 */
function getCountryCode() {
    var Locale = require('dw/util/Locale');

    var currentLocale = Locale.getLocale(request.locale);
    return currentLocale ? currentLocale.country : '';
}

/**
 * Gets all countries specific Google Map Options
 * sample object:
 * {
 *      "AE": {
 *	        "latitude": 23.4241,
 *		    "longitude": 53.8478,
 *		    "zoom": 4,
 *		    "scrollwheel": false
 *      },
 *      "SA": {...
 *      }
 * }
 *
 * @returns {Object} angmap options object if found, null otherwise
 */
function getCountriesGmapOptions() {
    var Site = require('dw/system/Site');

    var siteGmapOptions = Site.current.getCustomPreferenceValue('googleMapOptions');
    if (!siteGmapOptions) {
        return null;
    }

    if (siteGmapOptions) {
        try {
            return JSON.parse(siteGmapOptions);
        } catch (e) {} // eslint-disable-line no-empty
    }

    return null;
}

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @param {string} selectedCountryCode - provided country code
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url, selectedCountryCode) {
    var StoresModel = require('*/cartridge/models/stores');
    var StoreMgr = require('dw/catalog/StoreMgr');
    var URLUtils = require('dw/web/URLUtils');

    var countryCode = selectedCountryCode || geolocation.countryCode;
    var distanceUnit = countryCode === 'US' ? 'mi' : 'km';
    var resolvedRadius = radius ? parseInt(radius, 10) : 15;

    var searchKey = {};
    var storeMgrResult = null;
    var location = {};

    if (postalCode && postalCode !== '') {
        // find by postal code
        searchKey = postalCode;
        storeMgrResult = StoreMgr.searchStoresByPostalCode(
            countryCode,
            searchKey,
            distanceUnit,
            resolvedRadius
        );
        searchKey = { postalCode: searchKey };
    } else {
        // find by coordinates (detect location)
        location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
        location.long = long && lat ? parseFloat(long) : geolocation.longitude;

        if (selectedCountryCode) {
            storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius, 'countryCode={0}', selectedCountryCode);
            searchKey = { lat: location.lat, long: location.long, country: selectedCountryCode };
        } else {
            storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
            searchKey = { lat: location.lat, long: location.long };
        }
    }

    var actionUrl = url || URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();

    var stores = new StoresModel(storeMgrResult.keySet(), searchKey, resolvedRadius, actionUrl);

    return stores;
}

/**
 * Searches for a store and creates a plain object of the store
 * @param {storeId} storeId - provided store identifier
 * @returns {Object} a plain object of the store
 */
function getStore(storeId) {
    var StoreModel = require('*/cartridge/models/store');
    var StoreMgr = require('dw/catalog/StoreMgr');

    if (!storeId) {
        return null;
    }

    var store = StoreMgr.getStore(storeId);
    if (!store) {
        return null;
    }

    return new StoreModel(store);
}

/**
 * If there is an api key creates the url to include the google maps api else returns null
 * @param {Array} libraries - google map api libraries, ex: places,geometry
 * @returns {string|Null} return the api
 */
function getGoogleMapsApi(libraries) {
    var Site = require('dw/system/Site');
    var StringUtils = require('dw/util/StringUtils');

    var apiKey = Site.current.getCustomPreferenceValue('mapAPI');
    var siteLanguage = require('dw/util/Locale').getLocale(request.getLocale()).getLanguage();

    var googleMapsApi;
    if (apiKey) {
        googleMapsApi =
            StringUtils.format('https://maps.googleapis.com/maps/api/js?key={0}{1}&language={2}',
                apiKey,
                libraries && libraries.length
                    ? StringUtils.format('&libraries={0}', libraries.join(','))
                    : '',
                siteLanguage
            );
    } else {
        googleMapsApi = null;
    }

    return googleMapsApi;
}

base.getCountryCode = getCountryCode;
base.getStores = getStores;
base.getStore = getStore;
base.getCountriesGmapOptions = getCountriesGmapOptions;
base.getGoogleMapsApi = getGoogleMapsApi;
module.exports = base;
