'use strict';
/* global request */

/**
 * This module encapsulates of SEO content like URLs or meta tags
 * @module util/SEO
 */
var URLUtils = require('dw/web/URLUtils');
var URLAction = require('dw/web/URLAction');
var URLParameter = require('dw/web/URLParameter');
var Locale = require('dw/util/Locale');
var CurrentSite = require('dw/system/Site').current;
var collections = require('*/cartridge/scripts/util/collections');

var parameterNames = [
    'pid',
    'cgid',
    'q',
    'fdid',
    'cid',
    'id'
];

/**
 * Get Url Paramaters AsNamesAndParams
 * @returns {Array} array of the url parameters
 */
function getUrlParametersAsNamesAndParams() {
    var urlParameters = [];
    collections.forEach(request.httpParameterMap.getParameterNames(), function (key) {
        var value = request.httpParameterMap[key];
        if (value && parameterNames.indexOf(key) > -1) {
            urlParameters.push(key);
            urlParameters.push(value);
        }
    });

    return urlParameters;
}

/**
 * Get Url Paramaters AsURLParameter
 * @returns {Array} array of the url parameters
 */
function getUrlParametersAsURLParameter() {
    var urlParameters = [];
    collections.forEach(request.httpParameterMap.getParameterNames(), function (key) {
        var value = request.httpParameterMap[key];
        if (value && parameterNames.indexOf(key) > -1) {
            urlParameters.push(new URLParameter(key, value));
        }
    });

    return urlParameters;
}

/**
 * SEO utils
 */
module.exports = {
    /**
     * Get Canonical URL
     * @param {PipelineDictionary} pdict pipeline dictionary
     * @return {string} return page url
     */
    getCanonicalUrl: function (pdict) {
        return URLUtils.http(pdict.action || 'Home-Show', getUrlParametersAsNamesAndParams()).toString();
    },

    /**
     * Get Alternate Links
     * @param {PipelineDictionary} pdict pipeline dictionary
     * @return {Array} the alternate links array
     */
    getAlternateLinks: function (pdict) {
        var Site = require('dw/system/Site');
        var countries = require('*/cartridge/config/countries');

        var action = pdict.action || 'Home-Show';
        var urlParameters = getUrlParametersAsURLParameter();
        var currentLocale = Locale.getLocale(request.locale);
        var defaultAlternateLink = Site.current.getCustomPreferenceValue('defaultSeoAlternateLink');

        var alternateLinks = [];
        countries.forEach(function (countryLocale) {
            var siteId = 'siteId' in countryLocale ? countryLocale.siteId : CurrentSite.ID;
            var locale = Locale.getLocale(countryLocale.id);
            var urlAction = new URLAction(action, siteId, countryLocale.id);
            var url = URLUtils.http(urlAction, urlParameters);
            if (countryLocale.host) {
                url = url.host(countryLocale.host);
            }
            var stringUrl = url.toString();
            alternateLinks.push({
                url: stringUrl,
                hreflang: countryLocale.id.toLowerCase().replace('_', '-')
            });

            if (!defaultAlternateLink && countryLocale.default && locale.country === currentLocale.country) {
                alternateLinks.push({
                    url: stringUrl,
                    hreflang: 'x-default'
                });
            }
        });
        if (defaultAlternateLink) {
            alternateLinks.push({
                url: defaultAlternateLink,
                hreflang: 'x-default'
            });
        }
        return alternateLinks;
    }
};
