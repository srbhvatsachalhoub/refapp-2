'use strict';
var logger = require('dw/system/Logger').getLogger('monitor.order');

var Site = require('dw/system/Site');
var Calendar = require('dw/util/Calendar');

/**
 * Sends notification to the receipents to inform there is no orders in the system
 * @param {dw.util.Calendar} calendar - the calendar object that the query is done
 * @param {string} emailAddresses comma seperated email addresses
 * @returns {boolean} true if successful
 */
function sendAsMail(calendar, emailAddresses) {
    var Mail = require('dw/net/Mail');
    var Resource = require('dw/web/Resource');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var email = new Mail();

    try {
        emailAddresses.split(',').forEach(function (emailAddress) {
            email.addTo(emailAddress);
        });

        email.setSubject(Resource.msgf('noorder.mail.subject', 'ordermonitor', null, Site.current.ID));
        email.setFrom(Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com');
        email.setContent(renderTemplateHelper.getRenderedHtml({ calendar: calendar }, 'jobs/noOrderNotification'), 'text/html', 'UTF-8');
        email.send();

        return true;
    } catch (e) {
        logger.error('Error on sendAsMail function in jobs/order/orderMonitor.js, message: {0}', e.message);
    }

    return false;
}

/**
 * Gets the site calendar object by using given date object which has GMT timezone
 * @param {Date} gmtDate - the date object
 * @returns {dw.util.Calendar} calendar object
 */
function getCalendar(gmtDate) {
    if (!gmtDate) {
        return null;
    }

    var calendar = Site.current.calendar;
    calendar.setTime(gmtDate);
    calendar.add(Calendar.MILLISECOND, -1 * Site.current.timezoneOffset);

    return calendar;
}

/**
 * Iterates orders within given period to check lastPaymentStatus is error
 * to report/send them into given email list
 * @param {dw.util.HashMap} args - inputs from the BM
 * @returns {dw.system.Status} status object
 */
function execute(args) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Status = require('dw/system/Status');

    var intervalInHours = args.IntervalInHours;
    var emailAddresses = args.EmailAddresses;

    var midnightStartCalendar = getCalendar(args.MidnightStartTime);
    var midnightEndCalendar = getCalendar(args.MidnightEndTime);
    var calculateMidnight = false;

    // check if the start and end timed provided correctly
    if ((midnightStartCalendar && !midnightEndCalendar) || (!midnightStartCalendar && midnightEndCalendar)) {
        return new Status(Status.ERROR, '500', 'Midnight Start/End times are not well formed!');
    } else if (midnightStartCalendar && midnightEndCalendar) {
        calculateMidnight = true;
    }

    // current calendar/time
    var currentCalendar = Site.current.calendar;

    // interval calendar/time
    var intervalCalendar = Site.current.calendar;
    intervalCalendar.add(Calendar.HOUR, -1 * intervalInHours);

    // arrange the midnight start and end dates if provided
    if (calculateMidnight) {
        if (intervalCalendar.before(midnightEndCalendar) && midnightStartCalendar.after(midnightEndCalendar)) {
            midnightStartCalendar.add(Calendar.DAY_OF_YEAR, -1);
        } else if (midnightStartCalendar.after(midnightEndCalendar)) {
            midnightEndCalendar.add(Calendar.DAY_OF_YEAR, 1);
        }

        // check if the interval time is midnight
        if (intervalCalendar.after(midnightStartCalendar) && intervalCalendar.before(midnightEndCalendar)) {
            // if so extend interval (exclude midnight time)
            intervalCalendar.add(Calendar.MILLISECOND, midnightStartCalendar.time - currentCalendar.time);
        }
    }

    var ordersIterator = OrderMgr.queryOrders(
        'creationDate >= {0}',
        null,
        intervalCalendar.time
    );

    if (ordersIterator.count === 0) {
        if (!sendAsMail(intervalCalendar, emailAddresses)) {
            return new Status(Status.ERROR, '500', 'Notification mail could not be sent!');
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
