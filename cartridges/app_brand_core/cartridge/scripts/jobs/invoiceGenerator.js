var File = require('dw/io/File');
var Site = require('dw/system/Site');
var Order = require('dw/order/Order');
var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');

var INVOICES = 'src/invoices';

/**
 * Collects orders without invoices
 *
 * @returns {dw.order.Order[]} array of orders
 */
function collectOrdersWithoutInvoices() {
    var orderIterator = OrderMgr.searchOrders('NOT(custom.isInvoiceGenerated = {0}) AND (shippingStatus = {1} OR custom.additionalOrderShipmentStatus = {2} OR custom.additionalOrderShipmentStatus = {3})',
        'creationDate desc', true, Order.SHIPPING_STATUS_SHIPPED, 'packed', 'delivered');
    var ordersWithoutInvoices = [];
    while (orderIterator.hasNext()) {
        var order = orderIterator.next();
        ordersWithoutInvoices.push(order);
    }
    orderIterator.close();

    return ordersWithoutInvoices;
}

/**
 * Compares the country of order with locales country, returns true if matches
 *
 * @param {dw.order.Order} order order to compare locale of
 * @param {string} locale locale string (eg. en_SA)
 *
 * @returns {boolean} true if locale and order has the same country, false otherwise
 */
function orderMatchesLocale(order, locale) {
    return locale.split('_')[1] === order.getCustomerLocaleID().split('_')[1];
}

/**
 * Generates a request object to send to pdf generation service
 *
 * @param {dw.order.Order[]} orders array of orders
 * @param {Object} options pdf options
 *
 * @returns {Object[]} array of url-fileName tuples
 */
function generateInvoiceRequestObjects(orders, options) {
    var URLUtils = require('dw/web/URLUtils');
    var UrlAction = require('dw/web/URLAction');
    var URLParameter = require('dw/web/URLParameter');
    var InvoiceHelper = require('../helpers/invoiceHelper');

    var siteID = Site.getCurrent().getID();
    var locales = InvoiceHelper.getLocalesOrdered();
    var invoiceUrlFileNameList = [];
    orders.forEach(function (order) {
        locales.forEach(function (locale) {
            if (!orderMatchesLocale(order, locale)) {
                return;
            }
            var urlAction = new UrlAction('Invoice-Show', siteID, locale);

            var orderNoParameter = new URLParameter('orderNo', order.getOrderNo());
            var orderTokenParameter = new URLParameter('orderToken', order.getOrderToken());
            var langParameter = new URLParameter('lang', locale);

            var url = URLUtils.https(urlAction, orderNoParameter, orderTokenParameter, langParameter);
            url = url.toString();

            if (options.storefrontUser && options.storefrontPassword) {
                url = url.slice(8, url.length);
                url = StringUtils.format('https://{0}:{1}@{2}', options.storefrontUser, options.storefrontPassword, url);
            }

            invoiceUrlFileNameList.push({
                url: url,
                directoryName: order.getOrderNo(),
                fileName: InvoiceHelper.getInvoiceName(order.getOrderNo(), locale)
            });
        });
    });

    if (options.library && options.height && options.width) {
        return {
            invoiceRequests: invoiceUrlFileNameList,
            options: {
                libraryType: options.library,
                directPassParams: {
                    pageHeight: options.height,
                    pageWidth: options.width,
                    marginTop: '4',
                    marginRight: '0',
                    marginBottom: '4',
                    marginLeft: '0'
                }
            }
        };
    }
    return {
        invoiceRequests: invoiceUrlFileNameList
    };
}

/**
 * Creates the invoices directory and the directory with the current sites id
 *
 * @returns {dw.io.File} SiteDirectory
 */
function createAndGetSiteDirectory() {
    var impex = File.getRootDirectory(File.IMPEX);

    var currentSiteId = Site.getCurrent().getID();
    var siteDirectory = new File(impex, StringUtils.format('{0}{1}{2}', INVOICES, File.SEPARATOR, currentSiteId));
    siteDirectory.mkdirs();

    return siteDirectory;
}

/**
 * Creates the empty zip file
 *
 * @param {dw.io.File} siteDirectory directory to create the zip into
 *
 * @returns {dw.io.File} ZipFile
 */
function createAndGetZipFile(siteDirectory) {
    var UUIDUtils = require('dw/util/UUIDUtils');
    var zipFile = new File(StringUtils.format('{0}{1}{2}.zip',
        siteDirectory.getFullPath(), File.SEPARATOR, UUIDUtils.createUUID()));
    zipFile.createNewFile();
    return zipFile;
}

/**
 * Gets number of locale count available for the country of the order
 *
 * @param {dw.order.Order} order order
 * @param {string[]} locales array of locales
 *
 * @returns {int} locale count
 */
function getLocaleCount(order, locales) {
    var localeCount = 0;
    locales.forEach(function (locale) {
        if (orderMatchesLocale(order, locale)) {
            localeCount++;
        }
    });
    return localeCount;
}

/**
 * Checks if there are invoices generated for the given orders, and sets isInvoiceGenerated flag for each order
 *
 * @param {dw.order.Order[]} orders array of orders that has just been processed
 * @param {dw.io.File} siteDirectory directory to check if invoices exist in
 * @param {boolean} mergedPdfs flag that tells if the generated pdf is merged or seperated
 */
function setInvoiceGeneratedFlags(orders, siteDirectory, mergedPdfs) {
    var locales = Site.getCurrent().getAllowedLocales().toArray();

    orders.forEach(function (order) {
        var orderNo = order.getOrderNo();
        var requiredFileCount = mergedPdfs ? 1 : getLocaleCount(order, locales);

        var orderDirectory = new File(StringUtils.format('{0}{1}{2}',
            siteDirectory.getFullPath(), File.SEPARATOR, orderNo));
        if (orderDirectory.exists()) {
            var invoices = orderDirectory.listFiles();
            // set isInvoiceGenerated flag if there are at least 2 invoices for the given order
            Transaction.wrap(function () {
                order.custom.isInvoiceGenerated = invoices.length >= requiredFileCount; // eslint-disable-line no-param-reassign
            });
        }
    });
}

/**
 * Makes request to pdf service with given batch
 * @param {Object[]} batch array of requests
 * @param {boolean} mergedPdfs flag to specify if the different languages of invoices will be merged
 * @param {Object} options pdf options
 */
function handleBatch(batch, mergedPdfs, options) {
    var requestObject = generateInvoiceRequestObjects(batch, options);
    requestObject.mergedPdfs = mergedPdfs;

    var siteDirectory = createAndGetSiteDirectory();
    var zipFile = createAndGetZipFile(siteDirectory);

    var pdfServiceMgr = require('../helpers/pdfServiceMgr');
    var result = pdfServiceMgr.getInvoicePDFsZipped().call(JSON.stringify(requestObject), zipFile);
    if (result.status === 'OK') {
        zipFile.unzip(siteDirectory);
        zipFile.remove();
    } else {
        Logger.error('PdfService did not respond successfully.\n\tStatus: {0}\n\terror: {1}\n\tmessage: {2}',
            result.status, result.error, result.errorMessage);
    }
    setInvoiceGeneratedFlags(batch, siteDirectory, mergedPdfs);
}

/**
 * @param {dw.util.HashMap} args inputs from the BM
 *
 * Entry function for the job step generatePdfInvoice: generates invoices and sets related flag
 *
 * @returns {dw.system.Status} status of the job step
 */
function generate(args) {
    var ordersWithoutInvoice = collectOrdersWithoutInvoices();
    if (ordersWithoutInvoice.length === 0) {
        return new Status(Status.OK);
    }

    var mergedPdfs = args.MergedPDF;
    var batchSize = args.BatchSize;
    var options = {
        library: args.Library,
        width: args.Width,
        height: args.Height,
        storefrontUser: args.StorefrontUser,
        storefrontPassword: args.StorefrontPassword
    };

    while (ordersWithoutInvoice.length > 0) {
        handleBatch(ordersWithoutInvoice.splice(0, batchSize), mergedPdfs, options);
    }

    return new Status(Status.OK);
}

module.exports = {
    generate: generate
};
