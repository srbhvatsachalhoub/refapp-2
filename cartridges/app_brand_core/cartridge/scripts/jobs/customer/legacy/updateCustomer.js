'use strict';
/* global request */

var logger = require('dw/system/Logger').getLogger('legacy.customer.update'); // eslint-disable-line no-unused-vars

/**
 * Iterates all newly created imported legacy customers
 * to send them password reset email
 * @returns {dw.system.Status} status object
 */
function execute() {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');
    var Status = require('dw/system/Status');

    /**
     * updates profiles
     * @param {Object} profile - the profile
     */
    function callback(profile) {
        Transaction.wrap(function () { // eslint-disable-line no-loop-func
            profile.custom.sscSyncStatus = 'created'; // eslint-disable-line no-param-reassign
        });
    }
    CustomerMgr.processProfiles(callback, 'customerNo like \'L*\'');


    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
