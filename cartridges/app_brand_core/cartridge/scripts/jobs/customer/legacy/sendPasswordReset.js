'use strict';
/* global request */

var logger = require('dw/system/Logger').getLogger('legacy.customer.passwordreset'); // eslint-disable-line no-unused-vars

/**
 * Helper that sends an email to a customer. This will only get called if hook handler is not registered
 * @param {obj} emailObj - An object that contains information about email that will be sent
 * @param {string} emailObj.to - Email address to send the message to (required)
 * @param {string} emailObj.subject - Subject of the message to be sent (required)
 * @param {string} emailObj.from - Email address to be used as a "from" address in the email (required)
 * @param {int} emailObj.type - Integer that specifies the type of the email being sent out. See export from emailHelpers for values.
 * @param {string} template - Location of the ISML template to be rendered in the email.
 * @param {obj} context - Object with context to be passed as pdict into ISML template.
 */
function send(emailObj, template, context) {
    var Mail = require('dw/net/Mail');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var email = new Mail();
    email.addTo(emailObj.to);
    email.setSubject(emailObj.subject);
    email.setFrom(emailObj.from);
    email.setContent(renderTemplateHelper.getRenderedHtml(context, template), 'text/html', 'UTF-8');
    email.send();
}

/**
 * Iterates all newly created imported legacy customers
 * to send them password reset email
 * @returns {dw.system.Status} status object
 */
function execute() {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var Status = require('dw/system/Status');
    var Site = require('dw/system/Site');
    var Resource = require('dw/web/Resource');
    var urlHelpers = require('*/cartridge/scripts/helpers/urlHelpers');

    // query can be made more accurate, ex: not send if one was sent before
    var profilesIterator = CustomerMgr.queryProfiles(
        'customerNo like \'L*\' or customerNo like \'FM*\'',
        null,
        null
    );
    var subject = Resource.msg('subject.profile.resetpassword.email', 'login', null);
    var from = Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com';

    while (profilesIterator.hasNext()) {
        var profile = profilesIterator.next();

        var passwordResetToken;
        Transaction.wrap(function () { // eslint-disable-line no-loop-func
            passwordResetToken = profile.credentials.createResetPasswordToken();
        });

        if (!passwordResetToken) {
            continue;
        }
        var localeId = request.locale;

        // set current profile preferred locale to the request locale for the email
        if (profile.preferredLocale && request.locale !== profile.preferredLocale) {
            if (request.setLocale(profile.preferredLocale)) {
                localeId = profile.preferredLocale;
            }
        }

        var objectForEmail = {
            passwordResetToken: passwordResetToken,
            firstName: profile.firstName,
            lastName: profile.lastName,
            url: urlHelpers.setHostToUrl(URLUtils.https('Account-SetNewPassword', 'Token', passwordResetToken), localeId),
            contactusUrl: urlHelpers.setHostToUrl(URLUtils.https('Case-Create'), localeId),
            homeUrl: urlHelpers.setHostToUrl(URLUtils.https('Home-Show'), localeId)
        };

        var emailObj = {
            to: profile.email,
            subject: subject,
            from: from
        };

        send(emailObj, 'account/password/legacy/passwordResetEmail', objectForEmail);
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
