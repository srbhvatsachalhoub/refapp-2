/**
 * ©2013-2017 salesforce.com, inc. All rights reserved.
 *
 * payment_instrument_scripts.js
 *
 * Handles OCAPI hooks for order payment instruments
 */

var Status = require('dw/system/Status');

exports.beforePOST = function (order, paymentInstrument) { // eslint-disable-line no-unused-vars
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var collections = require('*/cartridge/scripts/util/collections');

    collections.forEach(order.getPaymentInstruments(), function (item) {
        if (item.paymentMethod !== PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            order.removePaymentInstrument(item);
        }
    });
    return new Status(Status.OK);
};
