'use strict';
var Status = require('dw/system/Status');

exports.modifyGETResponse_v2 = function (basket, paymentMethodResultResponse) {
    if (paymentMethodResultResponse && paymentMethodResultResponse.applicablePaymentMethods) {
        var applicablePaymentMethods = paymentMethodResultResponse.applicablePaymentMethods;
        for (var i = 0; i < applicablePaymentMethods.length; i++) {
            var paymentMethod = applicablePaymentMethods[i];
            if (paymentMethod.id === 'COD') {
                var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
                var isVerificationRequired = verificationHelpers.isVerificationRequired();
                var customerVerificationStatus = verificationHelpers.getSmsVerificationStatus();
                paymentMethod.c_isOTPRequired = !customerVerificationStatus || isVerificationRequired;
            } else {
                paymentMethod.c_isOTPRequired = false;
            }
        }
    }
    return new Status(Status.OK);
};
