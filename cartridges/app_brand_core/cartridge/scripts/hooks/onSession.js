'use strict';

/* global request, response, session */

/**
 * Returns true if script executing in Business Manager context (Sites-Site)
 * @returns {boolean} true if script executed from Business Manager
 */
function isBM() {
    // if Sites-Site, we're in Business Manager
    return (require('dw/system/Site').current.ID === 'Sites-Site') || (request.httpPath && request.httpPath.indexOf('Sites-Site') > -1);
}

/**
 * onSession Hook:localeRedirect
 * This hook will check to see if the user was browsed the site before
 * if so then this will set the last locale if found, and redirect user to action with new locale
 */
function localeRedirect() {
    // check if the request is Business Manager request
    // if so, no check
    if (isBM()) {
        return;
    }

    var paramMap = request.getHttpParameterMap();
    var localeQueryString = paramMap.get('locale');


    // If Request has a value for the locale query param, return
    // because this request is the GoogleBot
    if (localeQueryString.value !== null) {
        return;
    }

    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
    var cookieHelpers = require('*/cartridge/scripts/helpers/cookieHelpers');

    var cookie = cookieHelpers.getSiteLocaleCookie();
    var locale = cookie ? cookie.value : '';
    var countryCode = '';

    // if cookie not set before,
    // then try to get gelocation countryCode
    if (!locale && request.geolocation && request.geolocation.countryCode) {
        countryCode = request.geolocation.countryCode;
    }

    var result = localeHelpers.setNewLocale(locale, countryCode);
    if (result.success && result.redirectURL) {
        response.redirect(result.redirectURL);
    }

    localeHelpers.setCountryLocale(result.locale || request.locale);
}

/**
 * onSession Hook
 * Calls relevant methods on starting session
 */
function onSession() {
    // checks locale redirect on session start
    localeRedirect();
}

module.exports = {
    onSession: onSession
};
