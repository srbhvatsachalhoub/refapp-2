'use strict';

var server = require('server');
server.extend(module.superModule);

var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

server.append('History', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.orderhistory', 'account', null));
    next();
});

server.append('Details', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('title.receipt', 'confirmation', null));
    next();
});

server.append('Confirm', function (req, res, next) {
    res.setViewData({
        passwordConstraints: ProfileHelper.getPasswordConstraints()
    });
    next();
});

/**
 * Runs after Order-CreateAccount from app_storefront_base cartridge.
 * Sets the PreferredLocale to the recently created customer's profile
 */
server.append(
    'CreateAccount',
    function (req, res, next) {
        var StringUtils = require('dw/util/StringUtils');
        var registrationData = res.getViewData();

        var emailOK = ProfileHelper.customerWithFieldValueDoesNotExists('email', registrationData.email);
        var mobilePhoneOK = ProfileHelper.customerWithFieldValueDoesNotExists('phoneMobile', registrationData.phone);
        var errorMessageType;
        if (!emailOK) {
            errorMessageType = 'email';
        } else if (!mobilePhoneOK) {
            errorMessageType = 'phoneMobile';
        }

        if (errorMessageType) {
            res.json({
                success: false,
                nonUniqueIdentifier: true,
                message: Resource.msg(StringUtils.format('error.message.{0}.alreadyexists', errorMessageType), 'forms', null)
            });

            this.emit('route:Complete', req, res);
            return;
        }
        var passwordForm = server.forms.getForm('newPasswords');
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var viewData = res.getViewData();

            // if success and customer authenticated
            // set the current locale into Profile PreferredLocale
            if (viewData.success) {
                var OrderMgr = require('dw/order/OrderMgr');

                var order = OrderMgr.getOrder(req.querystring.ID);
                if (order && order.customer && order.customer.authenticated) {
                    var Transaction = require('dw/system/Transaction');

                    Transaction.wrap(function () {
                        order.customer.profile.setPreferredLocale(req.locale.id);
                    });

                    // set recently authenticated customer to viewdata
                    viewData.authenticatedCustomer = order.customer;
                    res.setViewData(viewData);
                    if (passwordForm.addtoemaillist.checked) {
                        var currentCustomer = order.customer;
                        ProfileHelper.saveOptinInformation(currentCustomer.profile, passwordForm.accountpreferences.optinlist);
                        var serviceResult = ProfileHelper.updateNewsletter(currentCustomer.profile, req.locale.id);
                        if (!serviceResult.isOk()) {
                            require('dw/system/Logger')
                                .getLogger('speedbus')
                                .error('An unexpected error while subscribing to newsletter occured {0}', serviceResult.getErrorMessage());
                        }
                    }
                }
            }
        });

        next();
        return;
    }
);

server.replace(
    'Track',
    consentTracking.consent,
    server.middleware.https,
    server.middleware.get,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    function (req, res, next) {
        var OrderMgr = require('dw/order/OrderMgr');
        var OrderModel = require('*/cartridge/models/order');
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

        var order;
        var validForm = true;
        var target = req.querystring.rurl || 1;
        var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
        var profileForm = server.forms.getForm('profile');
        profileForm.clear();

        if (req.querystring.trackOrderEmail
            && req.querystring.trackOrderNumber) {
            order = OrderMgr.getOrder(req.querystring.trackOrderNumber);
        } else {
            validForm = false;
        }

        if (!order) {
            res.render('/account/login', {
                navTabValue: 'login',
                orderTrackFormError: validForm,
                profileForm: profileForm,
                userName: '',
                actionUrl: actionUrl
            });
            next();
        } else {
            var config = {
                numberOfLineItems: '*'
            };

            var orderModel = new OrderModel(
                order,
                { config: config, countryCode: localeHelpers.getCurrentCountryCode(), containerView: 'order' }
            );

            // check the email and postal code of the form
            if (req.querystring.trackOrderEmail.toLowerCase()
                !== orderModel.orderEmail.toLowerCase()) {
                validForm = false;
            }

            if (validForm) {
                var exitLinkText;
                var exitLinkUrl;

                exitLinkText = !req.currentCustomer.profile
                    ? Resource.msg('link.continue.shop', 'order', null)
                    : Resource.msg('link.orderdetails.myaccount', 'account', null);

                exitLinkUrl = !req.currentCustomer.profile
                    ? URLUtils.url('Home-Show')
                    : URLUtils.https('Account-Show');

                res.render('account/orderDetails', {
                    order: orderModel,
                    exitLinkText: exitLinkText,
                    exitLinkUrl: exitLinkUrl,
                    breadcrumbs: [
                        {
                            htmlValue: Resource.msg('global.home', 'common', null),
                            url: URLUtils.home().toString()
                        },
                        {
                            htmlValue: Resource.msg('label.order.details', 'account', null)
                        }
                    ]
                });
            } else {
                res.render('/account/login', {
                    navTabValue: 'login',
                    profileForm: profileForm,
                    orderTrackFormError: !validForm,
                    userName: '',
                    actionUrl: actionUrl
                });
            }

            next();
        }
    }
);

server.get(
    'DataLayer',
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
        var OrderMgr = require('dw/order/OrderMgr');
        var OrderModel = require('*/cartridge/models/order');
        var Locale = require('dw/util/Locale');

        var order = OrderMgr.getOrder(req.querystring.ID);
        var token = req.querystring.token ? req.querystring.token : null;

        if (!order
            || !token
            || token !== 'dataLayerForce'
        ) {
            res.render('/error', {
                message: Resource.msg('error.confirmation.error', 'confirmation', null)
            });

            return next();
        }

        var config = {
            numberOfLineItems: '*'
        };

        var currentLocale = Locale.getLocale(req.locale.id);

        var orderModel = new OrderModel(
            order,
            { config: config, countryCode: currentLocale.country, containerView: 'order' }
        );
        var passwordForm;

        var reportingURLs = reportingUrlsHelper.getOrderReportingURLs(order);

        if (!req.currentCustomer.profile) {
            passwordForm = server.forms.getForm('newPasswords');
            passwordForm.clear();
            res.render('checkout/confirmation/confirmation', {
                order: orderModel,
                returningCustomer: false,
                passwordForm: passwordForm,
                reportingURLs: reportingURLs
            });
        } else {
            res.render('checkout/confirmation/confirmation', {
                order: orderModel,
                returningCustomer: true,
                reportingURLs: reportingURLs
            });
        }
        return next();
    }
);

server.get('GetHistoryCount', function (req, res, next) {
    var OrderHelpers = require('*/cartridge/scripts/order/orderHelpers');
    var ordersResult = OrderHelpers.getOrders(
        req.currentCustomer,
        req.querystring,
        req.locale.id
    );
    res.render('components/count', { count: ordersResult && ordersResult.orders ? ordersResult.orders.length : 0 });
    next();
});

module.exports = server.exports();
