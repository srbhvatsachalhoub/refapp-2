'use strict';

var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');
var Resource = require('dw/web/Resource');

var server = require('server');

/**
 * Opens the form that let's the customer to enter his/her mobile phone number
 */
server.get('MobilePhone', server.middleware.https, function (req, res, next) {
    var form = server.forms.getForm('profile');
    form.clear();

    res.render('/socialAccount/mobilePhone', {
        profileForm: form
    });
    return next();
});

/**
 * Action that handles the actual registration
 */
server.post('OAuthRegister', server.middleware.https, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');

    var form = server.forms.getForm('profile');

    var session = req.session.raw.custom;
    var oAuthReentryResult = session.OAuthReentryResult;
    if (!oAuthReentryResult) {
        res.json({
            success: false,
            redirectUrl: URLUtils.url('Login-Show', 'error', 1, 'case', 'NO_PROFILE')
        });
        return next();
    }

    oAuthReentryResult = JSON.parse(oAuthReentryResult);

    // Check if mobile phone is unique
    var mobilePhoneOK = ProfileHelper.checkUniquenessOfField(this, req, res, form, 'phoneMobile', false);
    if (!mobilePhoneOK) {
        var formErrors = require('*/cartridge/scripts/formErrors');
        res.json({
            success: false,
            fields: formErrors.getFormErrors(form)
        });
        return next();
    }

    var authenticatedCustomerProfile = {};

    // Create new profile
    Transaction.wrap(function () {
        var newCustomer = CustomerMgr.createExternallyAuthenticatedCustomer(
            oAuthReentryResult.oauthProviderID,
            oAuthReentryResult.userID
        );

        authenticatedCustomerProfile = newCustomer.getProfile();
        var firstName;
        var lastName;

        // Google comes with a 'name' property that holds first and last name.
        if (typeof oAuthReentryResult.externalProfile.name === 'object') {
            firstName = oAuthReentryResult.externalProfile.name.givenName;
            lastName = oAuthReentryResult.externalProfile.name.familyName;
        } else {
            // The other providers use one of these, GitHub has just a 'name'.
            firstName = oAuthReentryResult.externalProfile['first-name']
                || oAuthReentryResult.externalProfile.first_name
                || oAuthReentryResult.externalProfile.name;

            lastName = oAuthReentryResult.externalProfile['last-name']
                || oAuthReentryResult.externalProfile.last_name
                || oAuthReentryResult.externalProfile.name;
        }

        var email = ProfileHelper.getEmailFromExternalProfile(oAuthReentryResult.externalProfile);

        authenticatedCustomerProfile.setFirstName(firstName);
        authenticatedCustomerProfile.setLastName(lastName);
        authenticatedCustomerProfile.setEmail(email);
        authenticatedCustomerProfile.setPhoneMobile(form.customer.phoneMobile.value);
        authenticatedCustomerProfile.setPreferredLocale(req.locale.id);
    });


    // user is registered, attempting to log him/her in
    var credentials = authenticatedCustomerProfile.getCredentials();
    if (credentials.isEnabled()) {
        Transaction.wrap(function () {
            CustomerMgr.loginExternallyAuthenticatedCustomer(oAuthReentryResult.oauthProviderID, oAuthReentryResult.userID, false);
        });
    } else {
        res.render('/error', {
            message: Resource.msg('error.oauth.login.failure', 'login', null)
        });

        return next();
    }

    req.session.privacyCache.clear();

    res.json({
        success: true,
        redirectUrl: URLUtils.url(oAuthReentryResult.destination).toString()
    });

    return next();
});

module.exports = server.exports();
