
'use strict';
/* global customer */
var server = require('server');

var Resource = require('dw/web/Resource');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

/**
 * Show Track Order Page
 */
server.get('Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');

        var profileForm = server.forms.getForm('profile');
        profileForm.clear();

        var breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            }
        ];
        if (customer.authenticated) {
            breadcrumbs.push({
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.url('Account-Show').toString()
            });
        }
        breadcrumbs.push({
            htmlValue: Resource.msg('link.track.your.order', 'account', null)
        });
        var template = 'account/trackOrder';

        res.render(template, {
            breadcrumbs: breadcrumbs,
            redirectTemplate: template,
            profileForm: profileForm
        });
        next();
    });

module.exports = server.exports();
