'use strict';
/* global session */
var server = require('server');

server.get('Start', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var redirectUrl = (
        session.sourceCodeInfo &&
        session.sourceCodeInfo.redirect &&
        session.sourceCodeInfo.redirect.location
    ) || URLUtils.url('Home-Show');
    res.setStatusCode(301);
    res.redirect(redirectUrl);
    return next();
});

module.exports = server.exports();
