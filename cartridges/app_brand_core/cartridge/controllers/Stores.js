'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');

server.replace('Find', server.middleware.https, server.middleware.get, cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');

    var countriesGmapOptions = storeHelpers.getCountriesGmapOptions() || {};
    var currentCountryCode = storeHelpers.getCountryCode();
    var currentCountryGmapOptions = countriesGmapOptions[currentCountryCode] || {};

    var radius = req.querystring.radius || currentCountryGmapOptions.radius || 300;
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat || currentCountryGmapOptions.latitude;
    var long = req.querystring.long || currentCountryGmapOptions.longitude;
    var isForm = req.querystring.isForm || false;


    var stores = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, true, null, currentCountryCode);
    var viewData = {
        stores: stores,
        isForm: isForm,
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('title.hero.text', 'storeLocator', null)
            }
        ],
        form: server.forms.getForm('storeLocator'),
        countryCode: currentCountryCode,
        countriesGmapOptions: countriesGmapOptions
    };

    res.render('storeLocator/storeLocator', viewData);
    next();
});

// The req parameter in the unnamed callback function is a local instance of the request object.
// The req parameter has a property called querystring. In this use case the querystring could
// have the following:
// lat - The latitude of the users position.
// long - The longitude of the users position.
// radius - The radius that the user selected to refine the search
// or
// postalCode - The postal code that the user used to search.
// radius - The radius that the user selected to refine the search
server.replace('FindStores', server.middleware.get, function (req, res, next) {
    var radius = req.querystring.radius;
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat;
    var long = req.querystring.long;
    var selectedCountryCode = req.querystring.country || storeHelpers.getCountryCode();

    if (lat === '-1' || long === '-1' || radius === '-1') {
        var countriesGmapOptions = storeHelpers.getCountriesGmapOptions() || {};
        var currentCountryGmapOptions = countriesGmapOptions[selectedCountryCode] || {};

        radius = currentCountryGmapOptions.radius;
        lat = currentCountryGmapOptions.latitude;
        long = currentCountryGmapOptions.longitude;
    }

    var stores = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, true, null, selectedCountryCode);

    res.json(stores);
    next();
});

server.get('Detail', server.middleware.https, cache.applyDefaultCache, consentTracking.consent, function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');

    var storeId = req.querystring.id;

    if (!storeId) {
        res.redirect(URLUtils.https('Stores-Find'));
        return next();
    }

    var storeModel = storeHelpers.getStore(storeId);
    if (!storeModel) {
        res.setStatusCode(410);
        res.render('error/notFound');
        return next();
    }

    var viewData = {
        store: storeModel,
        locations: JSON.stringify([{
            name: storeModel.name,
            latitude: storeModel.latitude,
            longitude: storeModel.longitude
        }]),
        googleMapsApi: storeHelpers.getGoogleMapsApi(),
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('title.hero.text', 'storeLocator', null),
                url: URLUtils.url('Stores-Find').toString()
            },
            {
                htmlValue: storeModel.name
            }
        ]
    };

    res.render('storeLocator/storeDetail', viewData);
    return next();
});


module.exports = server.exports();
