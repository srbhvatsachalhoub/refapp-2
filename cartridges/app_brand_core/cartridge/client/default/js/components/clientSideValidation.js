'use strict';

var util = require('brand_core/util');
var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');

/**
 * Validate whole form inputs for the custom fieldMismatch error. Requires `this` to be set to form object
 * @returns {boolean} - Flag to indicate if form is valid, invalid if any fieldMismatch error
 */
function checkFieldMismatch() {
    var valid = true;
    $(this).find('input').each(function () {
        var $this = $(this);
        if ($this.data('mismatch-field-id')) {
            var $fieldMismatch = $('#' + $this.data('mismatch-field-id'));
            if ($fieldMismatch.length) {
                if ($this.val() && $fieldMismatch.val() && $fieldMismatch.val() !== $this.val()) {
                    valid = false;
                    this.validity.fieldMismatch = true;
                    $this.trigger('invalid', this.validity);
                }
            }
        }
    });
    return valid;
}

/**
 * Validate whole form. Requires `this` to be set to form object
 * @param {jQuery.event} event - Event to be canceled if form is invalid.
 * @returns {boolean} - Flag to indicate if form is valid
 */
function validateForm(event) {
    var valid = true;
    if (this.checkValidity) {
        if (!this.checkValidity()) {
            // safari
            valid = false;
            if (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }
            $(this).find('input, select, textarea').each(function () {
                if (!this.validity.valid) {
                    $(this).trigger('invalid', this.validity);
                }
            });
        } else if (!checkFieldMismatch.call(this)) {
            // safari
            valid = false;
            if (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
            }
        }
    }
    return valid;
}

/**
 * Remove all validation. Should be called every time before revalidating form
 * @param {element} form - Form to be cleared
 * @returns {void}
 */
function clearForm(form) {
    $(form).find('.form-control.is-invalid, .form-group.is-invalid').removeClass('is-invalid');
}

module.exports = {
    invalid: function () {
        $('body').on('invalid', 'form input, form select, form textarea', function (e) {
            e.preventDefault();
            this.setCustomValidity('');
            if (!this.validity.valid || this.validity.fieldMismatch) {
                var validationMessage = this.validationMessage;
                $(this).addClass('is-invalid');
                $(this).parents('.form-group').addClass('is-invalid');
                if (this.validity.patternMismatch && $(this).data('pattern-mismatch')) {
                    validationMessage = $(this).data('pattern-mismatch');
                }
                if ((this.validity.rangeOverflow || this.validity.rangeUnderflow)
                    && $(this).data('range-error')) {
                    validationMessage = $(this).data('range-error');
                }
                if ((this.validity.tooLong || this.validity.tooShort)
                    && $(this).data('range-error')) {
                    validationMessage = $(this).data('range-error');
                }
                if (this.validity.valueMissing && $(this).data('missing-error')) {
                    validationMessage = $(this).data('missing-error');
                }
                // check customly added fieldMismatch error
                if (this.validity.fieldMismatch && $(this).data('mismatch-error')) {
                    validationMessage = $(this).data('mismatch-error');
                }
                $(this).parents('.form-group').find('.invalid-feedback')
                    .text(validationMessage);
            }
        });
    },

    submit: function () {
        $('body').on('submit', 'form', function (e) {
            return validateForm.call(this, e);
        });
    },

    buttonClick: function () {
        $('body').on('click', 'form button[type="submit"], form input[type="submit"]', function (e) {
            // clear all errors when trying to submit the form and tries to revalidate the form
            var $form = $(this).parents('form');
            if ($form.length) {
                clearForm($form);
                var isValid = validateForm.call($form[0], e);
                var width = $(window).width();

                if (!isValid && (commonHelpers.isMobileView(width, window.RA_BREAKPOINTS) || commonHelpers.isTabletView(width, window.RA_BREAKPOINTS))) {
                    var $firstInvalidField = $form.find('.form-control:invalid, .js-hidden-input:invalid').first();

                    if ($firstInvalidField.hasClass('js-hidden-input')) {
                        $firstInvalidField = $firstInvalidField.siblings('.form-control:visible').first();
                    }

                    util.scrollBrowser($firstInvalidField.offset().top);
                }

                return isValid;
            }
            return true;
        });
    },

    functions: {
        validateForm: function (form, event) {
            // clear all errors when trying to validate the form
            clearForm($(form));
            return validateForm.call(form, event || null);
        },
        clearForm: clearForm
    }
};
