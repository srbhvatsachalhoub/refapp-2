'use strict';

var util = require('../util');
var mapDefaultOptions = require('../config/googleMap-config-default.json');

/**
 * @description Create Google map
 * @param {string} selector - selector for DOM-element where will be initialized map
 * @param {Object} params - additional options for the map
 * @param {string} formSelector - additional options for the map
 * @returns {Void} - Void
 */
function GoogleMap(selector, params, formSelector) {
    if (!selector || !window.google) return false;

    this.mapElement = $(selector);
    this.mapContainer = this.mapElement.closest('.js-map');
    this.settings = $.extend(true, mapDefaultOptions, params || {});
    this.formSelector = formSelector;
    this.markers = [];

    var customSettings = this.settings.mapSettings.customSettings;
    if (customSettings.setCenterBasedOnLocale && customSettings.centerLocalized && customSettings.centerLocalized[window.SiteSettings.country]) {
        // Center map based on country
        this.settings.mapSettings.center = {
            lat: customSettings.centerLocalized[window.SiteSettings.country].latitude,
            lng: customSettings.centerLocalized[window.SiteSettings.country].longitude
        };
    }

    this.map = new window.google.maps.Map(this.mapElement[0], this.settings.mapSettings);

    // Pick location from map
    this.map.addListener('click', this.onMarkerUpdate.bind(this));

    this.mapElement.data('mapInstance', this);
}

/**
 * Save selected address
 * @param {Object} place - Object form Google
 */
// eslint-disable-next-line consistent-return
GoogleMap.prototype.saveSelectedAddress = function (place) {
    var siteAddressFormFieldsMapping = this.settings.mapping.siteAddressFormFieldsMapping[window.SiteSettings.country];
    var googleMapAddressResultsMapping = window.googleMapAddressResultsMapping && window.googleMapAddressResultsMapping[window.SiteSettings.country] ? window.googleMapAddressResultsMapping[window.SiteSettings.country] : null;
    var componentForm = this.settings.mapping.componentForm;

    if (!siteAddressFormFieldsMapping) {
        return false;
    }

    var addressObj = {};
    var resultAddressComponents = place.address_components.reverse();

    // Get each component of the address from the place details
    for (var i = 0; i < resultAddressComponents.length; i++) {
        if (resultAddressComponents[i].types) {
            for (var j = 0; j < resultAddressComponents[i].types.length; j++) {
                var addressType = resultAddressComponents[i].types[j];

                if (componentForm[addressType] && siteAddressFormFieldsMapping[addressType]) {
                    var addressFieldValue = resultAddressComponents[i][componentForm[addressType]];

                    addressObj[siteAddressFormFieldsMapping[addressType]] = addressFieldValue;
                }
            }
        }
    }

    Object.keys(addressObj).forEach(function (fieldName) {
        // Unite building number with address1 field
        var fieldValue = addressObj[fieldName];
        if (fieldName === 'address1' && addressObj.street_number) {
            addressObj[fieldName] = fieldValue + ' ' + addressObj.street_number;
        }

        // Custom mapping for SFCC states/cities field values
        if (fieldName === 'state' &&
            googleMapAddressResultsMapping &&
            Object.hasOwnProperty.call(googleMapAddressResultsMapping, 'states') &&
            Object.hasOwnProperty.call(googleMapAddressResultsMapping.states, window.SiteSettings.language) &&
            Object.hasOwnProperty.call(googleMapAddressResultsMapping.states[window.SiteSettings.language], fieldValue)
        ) {
            addressObj[fieldName] = googleMapAddressResultsMapping.states[window.SiteSettings.language][fieldValue];
        }

        if (fieldName === 'city') {
            if (window.countryCities) {
                window.countryCities.some(function (countryCity) {
                    // Compare city label based on localized google address result
                    var countryCityLabel = countryCity['city_' + window.SiteSettings.language];
                    var exist = countryCityLabel && (countryCityLabel.indexOf(fieldValue) > -1 || fieldValue.indexOf(countryCityLabel) > -1);

                    if (exist) {
                        addressObj[fieldName] = countryCityLabel;
                    }

                    return exist;
                });
            }
        }
    });

    if (place.types && place.types.indexOf('lodging') !== -1 && place.name) {
        addressObj.address1 = place.name;
    }

    addressObj.location = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
    };

    // take  data-saved-address in address.js
    this.mapContainer.attr('data-saved-address', JSON.stringify(addressObj));
    this.populateAddress(addressObj);
};

/**
 * Reset selected address
 * @param {Object} options - options
 */
GoogleMap.prototype.resetSelectedAddress = function () {
    var mapComponent = this;

    mapComponent.mapContainer.attr('data-saved-address', null);
    mapComponent.clearMarkers();
};

/**
 * Create markers on the map
 * @param {Object} geolocation - geolocation
 * @param {string} markerLabel - markerLabel
 */
GoogleMap.prototype.createMarker = function (geolocation, markerLabel) {
    var mapComponent = this;
    var image = {
        // todo markerIconSettings
        // should be done by FE team
        // url: staticUrl + mapComponent.settings.mapSettings.customSettings.markerIconSettings.iconPath, // custom image
        size: new window.google.maps.Size(
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.width,
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.height
        ),
        origin: new window.google.maps.Point(0, 0),
        anchor: new window.google.maps.Point(
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.leftWhitespace,
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.height - mapComponent.settings.mapSettings.customSettings.markerIconSettings.bottomWhitespace
        ),
        scaledSize: new window.google.maps.Size(
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.width,
            mapComponent.settings.mapSettings.customSettings.markerIconSettings.height
        )
    };

    var marker = new window.google.maps.Marker({
        position: geolocation,
        map: mapComponent.map,
        draggable: true,
        title: markerLabel,
        icon: image
    });

    marker.addListener('dragend', mapComponent.onMarkerUpdate.bind(this));

    mapComponent.markers.push(marker);

    this.marker = marker;
};

/**
 * Correct new marker position
 * @param {google.maps.MouseEvent} event - Google Map mouse event
 */
GoogleMap.prototype.onMarkerUpdate = function (event) {
    var geolocation = {
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
    };

    this.findAddressByLatLng(geolocation, event.placeId);
};

/**
 * Remove all markers from the map
 */
GoogleMap.prototype.clearMarkers = function () {
    this.markers.map(function (marker) {
        return marker.setMap(null);
    });

    this.markers = [];
};

/**
 * Create markers on the map
 * @param {Object} geolocation - geolocation
 * @param {string} markerLabel - markerLabel
 */
GoogleMap.prototype.moveMarker = function (geolocation, markerLabel) {
    if (!this.marker) {
        this.createMarker(geolocation, markerLabel);

        return;
    }

    var newPosition = new window.google.maps.LatLng(geolocation.lat, geolocation.lng);

    this.marker.setPosition(newPosition);
};


/**
 * Set Place On Map
 * @param {Object} place - place
 */
GoogleMap.prototype.setPlaceOnMap = function (place) {
    var mapComponent = this;

    var placeGeolocation = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
    };

    mapComponent.map.setCenter(placeGeolocation);
    mapComponent.moveMarker(placeGeolocation, place.name);

    mapComponent.saveSelectedAddress(place);
};

/**
 * Find address based on LatLng using Geocoder
 * @param {Object} geolocation - geolocation
 * @param {string} placeId - placeId
 */
GoogleMap.prototype.findAddressByLatLng = function (geolocation, placeId) {
    var mapComponent = this;

    var geocoder = new window.google.maps.Geocoder();
    var placesService = null;

    if (placeId) {
        placesService = new window.google.maps.places.PlacesService(mapComponent.map);
        var placeDetailsRequest = {
            placeId: placeId,
            fields: ['name', 'address_component', 'geometry', 'types']
        };
        placesService.getDetails(placeDetailsRequest, function (placesServicePlace, status) {
            if (status === window.google.maps.places.PlacesServiceStatus.OK) {
                mapComponent.setPlaceOnMap(placesServicePlace);
            }
        });
    } else {
        geocoder.geocode({ location: geolocation }, function (results, status) {
            if (status === 'OK') {
                var place = results[0];
                if (place) {
                    mapComponent.setPlaceOnMap(place);
                }
            }
            // add error handling here, if needed
        });
    }
};

/**
 * Updates city field on shipping form
 * @param {string} str - string to be converted to title case
 * @returns {string} - title case
 */
function toTitleCase(str) {
    return str
        .toLowerCase()
        .split(' ')
        .map(function (word) {
            return word.charAt(0).toUpperCase() + word.slice(1);
        })
        .join(' ');
}

/**
 * Updates city field on shipping form
 * @param {jQuery} $shippingForm - shipping form object
 * @param {string} address - address object
 */
function updateCitySelect($shippingForm, address) {
    var $citySelect = $shippingForm.find('select.js-selectbox-city');
    var city = toTitleCase((address.city || '').replace('/-/g', ' '));
    var area = toTitleCase((address.area || '').replace('/-/g', ' '));
    var isCityAvailable = $citySelect.find('option[value="' + city + '"]').length;
    var isAreaAvailable = $citySelect.find('option[value="' + area + '"]').length;

    if (isCityAvailable) {
        $citySelect.val(city);
    } else if (isAreaAvailable) {
        $citySelect.val(area);
    } else {
        $citySelect.val('');
    }
    $citySelect.selectpicker('refresh');
    $citySelect.trigger('change');
}
/**
 * Updates city field on shipping form
 * @param {jQuery} $shippingForm - shipping form object
 * @param {string} value - new province value
 */
function updateProvinceSelect($shippingForm, value) {
    var $citySelect = $shippingForm.find('select.js-selectbox-state');

    $citySelect.val(value);
    $citySelect.selectpicker('refresh');
    $citySelect.trigger('change');
}

/**
 * Populate shipping form address
 * @param {Object} address - address data
 * @param {string} selector - selector for form
 */
GoogleMap.prototype.populateAddress = function (address) {
    var $shippingForm = $(this.formSelector);

    if (address.state) {
        updateProvinceSelect($shippingForm, address.state);
    }
    Object.keys(address).forEach(function (field) {
        if (field === 'city') {
            updateCitySelect($shippingForm, address);

            return;
        }

        if (field === 'address1') {
            var $addressInput = $shippingForm.find('.js-input-address-one');
            $addressInput.val(address[field]);

            return;
        }

        var $formField = $shippingForm.find('.js-input-' + field);

        if ($formField.length === 0) {
            return;
        }

        $formField.val(address[field]);
    });
};

/**
 * @description update DOM
 * @param {boolean} showMap - showMap?
 */
function updateDOM(showMap) {
    var dataRemoveSelector;
    var dataAddSelector;
    if (showMap) {
        $('.js-google-map-area').removeClass('d-none');
        dataRemoveSelector = 'classesMapHide';
        dataAddSelector = 'classesMapShown';
    } else {
        $('.js-google-map-area').addClass('d-none');
        dataRemoveSelector = 'classesMapShown';
        dataAddSelector = 'classesMapHide';
    }
    // update DOM to change form layuot in case when map present on the page
    var $targets = $('.js-update-map-view');
    $targets.each(function () {
        var $el = $(this);
        $el.removeClass($el.data(dataRemoveSelector));
        $el.addClass($el.data(dataAddSelector));
    });
}
/**
 * @description Init events
 */
function initEvents() {
    // 'Find my location' button event
    $(document).on('click', '.js-map-find-my-location', function (event) {
        event.preventDefault();
        var $mapHolder = $('.js-map').find('.js-map-holder');
        var mapComponent = $mapHolder.data('mapInstance');

        if (!mapComponent) return;

        var $locationErrorContainer = mapComponent.mapContainer.find('.js-map-location-error');

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                mapComponent.findAddressByLatLng(geolocation, null);
            }, function () {
                util.addAndRemoveClass($locationErrorContainer, 'd-block');
            });
        }

        // update DOM
        updateDOM(true);
    });
}

/**
 * Initialize Address Map
 * can be reuse on Check Out and My Account pages
 * @param {string} formSelector - selector for form
 */
function initAddressMap(formSelector) {
    var $mapHolder = $(formSelector).find('.js-map-holder');
    if ($(formSelector).data('initGoogleMap') === false || $mapHolder.length === 0) {
        return;
    }

    var mapComponent = $mapHolder.data('mapInstance');
    if (!mapComponent) {
        initEvents();
        updateDOM(false);
        var params;
        var countries = $mapHolder.data('gmapParams');
        if (countries) {
            params = {
                mapSettings: {
                    customSettings: {
                        centerLocalized: countries
                    }
                }
            };
        }
        // eslint-disable-next-line no-unused-vars
        var findAddressMap = new GoogleMap('.js-map-holder', params, formSelector);
    } else {
        mapComponent.resetSelectedAddress({
            resetSearchPlaceInput: true
        });
        mapComponent.map.setCenter(mapComponent.settings.mapSettings.customSettings.centerLocalized[window.SiteSettings.country]);
    }
}

module.exports = {
    Map: GoogleMap,
    initAddressMap: initAddressMap
};
