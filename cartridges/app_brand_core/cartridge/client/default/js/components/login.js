'use strict';

var formValidation = require('base/components/formValidation');
var urlHelper = require('brand_core/helpers/urlHelper');
var notificationHelper = require('brand_core/helpers/notificationHelper')('.error-messaging');

module.exports = {
    login: function () {
        /**
         * Add wishlistpid to login form action as querystring parameter
         */
        $('#loginFormModal').on('show.bs.modal', function (e) {
            // Get wishlist-pid attribute of the clicked element
            var pid = $(e.relatedTarget).data('wishlist-pid');

            // Find form element
            var $form = $(this).find('.js-login-form');

            // Don't go further if pid is not supplied
            if (pid) {
                $form.data('wishlist-pid', pid);
            } else {
                $form.removeData('wishlist-pid');
            }
        });

        $(document)
            // Do ajax request when login form is submitted
            .on('submit', '.js-login-form', function (e) {
                var $form = $(this);
                var $spinnerContainer = $form.closest('.js-login-modal');
                var spinner = ($spinnerContainer.length ? $spinnerContainer : $).spinner();

                // Do not make an http request
                e.preventDefault();

                // Get url from action attribute
                var url = $form.attr('action');

                // Start spinner
                spinner.start();

                // Remove error messages from previous login attempts
                $form.find('.alert-danger').remove();

                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: url,

                    // Serialize form data
                    data: $form.serialize(),

                    success: function (response) {
                        if (response && response.success) {
                            var redirectUrl = response.redirectUrl || window.location.href;
                            var wishlistPid = $form.data('wishlist-pid');

                            // Add wishlistpid to redirected url
                            if (wishlistPid) {
                                redirectUrl = urlHelper.setParameter('wishlistpid', wishlistPid, redirectUrl);
                            }
                            $(document).trigger('gtm:customerLoginEvent', {
                                customerID: response.authenticatedCustomerID,
                                customerEmail: response.authenticatedCustomerProfileEmail
                            });

                            // Reload page if customer is logged in successfully
                            window.location.href = redirectUrl;
                            return;
                        }

                        // Stop spinner
                        spinner.stop();

                        // Show error details
                        formValidation($form, response);
                    },

                    error: function (err) {
                        if (err.responseJSON && err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else {
                            spinner.stop();
                        }
                    }
                });
            });
    },

    register: function () {
        $('form.registration').submit(function (e) {
            var form = $(this);
            e.preventDefault();
            var url = form.attr('action');
            form.spinner().start();
            $('form.registration').trigger('login:register', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function (data) {
                    form.spinner().stop();
                    if (!data.success) {
                        formValidation(form, data);
                    } else {
                        $(document).trigger('gtm:customerRegisterEvent', {
                            customerID: data.authenticatedCustomerID,
                            customerEmail: data.authenticatedCustomerProfileEmail
                        });
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err && err.responseJSON) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else {
                            notificationHelper.error(err.responseJSON.errorMessage);
                        }
                    } else {
                        notificationHelper.generalError();
                    }
                    form.spinner().stop();
                }
            });
            return false;
        });
    },

    resetPassword: function () {
        $('.reset-password-form').submit(function (e) {
            var form = $(this);
            e.preventDefault();
            var url = form.attr('action');
            form.spinner().start();
            $('.reset-password-form').trigger('login:register', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function (data) {
                    form.spinner().stop();
                    if (!data.success) {
                        formValidation(form, data);
                    } else {
                        $('.js-reset-password-form-container').addClass('d-none');

                        var resultTitle = $('.js-reset-password-result').data('dialog-title');
                        if (resultTitle) {
                            $('.js-request-password-reset-modal-header').html(resultTitle);
                        }

                        $('.js-reset-password-result').removeClass('d-none');
                    }
                },
                error: function () {
                    form.spinner().stop();
                }
            });
            return false;
        });

        $('.js-reset-password-hide-modal').on('click', function () {
            var $loginModal = $('.js-login-modal');
            if ($loginModal.length === 0) {
                $('#requestPasswordResetModal').modal('hide');
            }
        });
    },

    clearResetForm: function () {
        $('#login .modal,#requestPasswordResetModal').on('hidden.bs.modal', function () {
            $('#reset-password-email').val('');
            $('.modal-dialog .form-control.is-invalid').removeClass('is-invalid');
        });
    },

    resetPasswordModalTitleRefresh: function () {
        $('#requestPasswordResetModal').on('show.bs.modal', function () {
            var modal = $(this);
            var defaultTitle = modal.data('default-title');
            $('.js-request-password-reset-modal-header').html(defaultTitle);
        });
    },

    showPassword: function () {
        $('.js-password-show-toggle').click(function () {
            var passwordField = $(this).parent().find($('.js-input-password'));

            if (passwordField.attr('type') === 'password') {
                passwordField.attr('type', 'text');
            } else {
                passwordField.attr('type', 'password');
            }
        });
    }
};
