'use strict';

var PREVIOUS_COOKIE_WARNING_TIMESTAMP = 'pcwt_' + (window.SiteSettings.siteBrand || window.SiteSettings.siteId);
var DEFAULT_COOKIE_WARNING_DURATION = 1; // Default warning duration is 1 day
var Cookies = require('js-cookie');

/**
 * saves the cookie warning close time
 */
function saveCookieWarningTime() {
    Cookies.set(PREVIOUS_COOKIE_WARNING_TIMESTAMP, new Date().getTime());
}

/**
 * gets the cookie warning close time
 * @returns {number} last cookie warning close time
 */
function getCookieWarningTime() {
    return Cookies.get(PREVIOUS_COOKIE_WARNING_TIMESTAMP);
}

/**
 * checks that cookie warning time came
 * @returns {boolean} whether the cookie warning duration expired or not
 */
function cookieWarningDurationExpired() {
    var cookieWarningDuration = window.SiteSettings.cookieWarningDuration;
    cookieWarningDuration = (cookieWarningDuration === undefined || cookieWarningDuration === null) ? DEFAULT_COOKIE_WARNING_DURATION : cookieWarningDuration;
    var previousCookieWarningTimeStamp = getCookieWarningTime();

    if (previousCookieWarningTimeStamp === undefined || previousCookieWarningTimeStamp === null) {
        return true;
    }

    var currentTimestamp = new Date().getTime();
    let previousTimeStamp = parseInt(previousCookieWarningTimeStamp, 10);

    return (currentTimestamp - previousTimeStamp) > (cookieWarningDuration * 24 * 60 * 60 * 1000);
}

module.exports = function () {
    var $content = $('.js-cookie-warning-content');
    var $container = $('.js-cookie-warning-container');
    // If content asset is not defined or offline, don't go further
    if ($content.find('.content-asset').length === 0) {
        $container.addClass('d-none');
        return;
    }

    if (!cookieWarningDurationExpired()) {
        // Hide the cookie warning since the duration has not expired yet
        $container.addClass('d-none');
    } else {
        // Show the cookie warning since the duration has not expired yet
        $container.removeClass('d-none');
    }

    // Attach close event
    $('.js-cookie-cancel-btn').on('click', function () {
        // Hide the cookie warning
        $container.addClass('d-none');
        $('.js-sticky-header').sticky('resize');
        saveCookieWarningTime();
    });
};
