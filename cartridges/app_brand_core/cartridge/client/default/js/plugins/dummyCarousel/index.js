/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var pluginator = require('../pluginator');
var pluginPrefix = 'dummyCarousel';

/**
 * Plugin constructor
 * Overrides Bootstrap Carousel Plugin that is using in app_storefront_base but NOT IN
 * app_loccitane_core
 * app_ysl_core
 * @param {JQuery} $el - Search input element
 * @param {string} id - Id of the current plugin
 * @param {Object} options - Plugin opitons
 */
function DummyCarousel($el, id, options) {
    this.$el = $el;
    this.id = id;
    this.options = options;
}

DummyCarousel.prototype.init = function () {

};

DummyCarousel.prototype.destroy = function () {

};

DummyCarousel.prototype.dispose = function () {

};

module.exports = function () {
    pluginator({
        prefix: pluginPrefix,
        name: 'carousel',
        Constructor: DummyCarousel,
        exports: ['dispose'],
        defaultOptions: {}
    });
};
