'use strict';

$(document).ready(function () {
    $('.js-engraving-checkbox').on('click', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            $('.js-engraving-text').removeClass('d-none');
        } else {
            $('.js-engraving-text input').val('');
            $('.js-engraving-text').addClass('d-none');
        }
    });

    $('body').on('updateAddToCartFormData', function (context, form) {
        form.customizationSettings = JSON.stringify({ // eslint-disable-line no-param-reassign
            personalizationRequested: $('.js-personalization-checkbox').prop('checked'),
            engravingRequested: $('.js-engraving-checkbox').prop('checked'),
            engravingText: $('.js-engraving-text input').val()
        });
    });
});
