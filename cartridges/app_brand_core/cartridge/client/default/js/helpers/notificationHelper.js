'use strict';

var privateSelPlaceholder = 'body';

/**
 * Create an alert to display the error message
 * @param {Object} message - Error message to display
 * @param {Object} type - Type of the message; danger, success, warning, info, light, dark
 */
function msg(message, type) {
    $(privateSelPlaceholder).append(
        '<div class="alert alert-' + type + ' alert-dismissible fade show" role="alert">' +
        '   <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '       <span aria-hidden="true">&times;</span>' +
        '   </button>' +
        '   <span>' + message + '</span>' +
        '</div>'
    );
}

/**
 * Create general error message
 */
function generalError() {
    msg(window.RA_RESOURCE['common.error.general'], 'danger');
}

/**
 * Create error message
 * @param {Object} message - Error message to display
 */
function error(message) {
    msg(message, 'danger');
}

/**
 * Create success message
 * @param {Object} message - Error message to display
 */
function success(message) {
    msg(message, 'success');
}

/**
 * Create warning message
 * @param {Object} message - Error message to display
 */
function warning(message) {
    msg(message, 'warning');
}

/**
 * Create info message
 * @param {Object} message - Error message to display
 */
function info(message) {
    msg(message, 'info');
}

/**
 * Create light message
 * @param {Object} message - Error message to display
 */
function light(message) {
    msg(message, 'light');
}

/**
 * Create dark message
 * @param {Object} message - Error message to display
 */
function dark(message) {
    msg(message, 'dark');
}

/**
 * Create an alert to display the error message
 * @param {jQuery} selPlaceholder - The selector for placeholder element to which error message will be appended
 * @returns {Object} - Exportted methods
 */
module.exports = function (selPlaceholder) {
    privateSelPlaceholder = selPlaceholder;
    return {
        generalError: generalError,
        error: error,
        success: success,
        warning: warning,
        info: info,
        light: light,
        dark: dark
    };
};
