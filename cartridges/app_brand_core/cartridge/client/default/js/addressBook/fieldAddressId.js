'use strict';

module.exports = {
    init: function () {
        $('.js-button-save-address').on('click', function () {
            var $form = $(this.form);
            var $addressId = $form.find('.js-input-address-id');
            var address = $form.find('.js-input-address-one').val();
            var countryCode = $form.find('.js-input-country-code').val();
            var addressId = address ? (countryCode + '-' + address).substring(0, 20) : '';
            $addressId.val(addressId);
        });
    }
};
