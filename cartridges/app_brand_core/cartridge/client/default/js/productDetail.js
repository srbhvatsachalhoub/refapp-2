'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('base/product/detail'));
    processInclude(require('./product/wishlist'));
    processInclude(require('plugin_applepay/product/detail'));
    processInclude(require('./product/backInStock'));
});
