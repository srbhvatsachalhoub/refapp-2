window.jQuery = window.$ = require('jquery');
var processInclude = require('base/util');
window.RA_BREAKPOINTS = require('./config/breakpoints.json').breakpoints;

$(document).ready(function () {
    processInclude(require('brand_core/dataStates'));
    processInclude(require('brand_core/components/tooltip'));
    processInclude(require('brand_core/components/cookieWarning'));
    processInclude(require('brand_core/header'));
    processInclude(require('./product/addToCartNotification'));
    processInclude(require('base/components/footer'));
    processInclude(require('./components/miniCart'));
    processInclude(require('base/components/collapsibleItem'));
    processInclude(require('brand_core/components/clientSideValidation'));
    processInclude(require('base/components/countrySelector'));
    processInclude(require('brand_core/newsletterSubscribe/newsletter'));
    processInclude(require('brand_core/components/cleave'));
    processInclude(require('brand_core/components/selectdate'));
    processInclude(require('brand_core/components/password'));
    processInclude(require('brand_core/components/modal'));
    processInclude(require('brand_core/components/multilineEllipsis'));
    processInclude(require('./components/video'));
    processInclude(require('./product/productCarousel'));
    processInclude(require('./product/attributeCarousel'));
    processInclude(require('./product/productTileAttributes'));
    processInclude(require('./components/bannerCarousel'));
    processInclude(require('./components/mobileNavigation'));
    processInclude(require('base/components/toolTip'));
    processInclude(require('./components/common'));
    processInclude(require('./components/login'));
    processInclude(require('./product/productTileAddToCart'));
});

/* Third party scripts */
require('bootstrap/js/src/util.js');
require('bootstrap/js/src/alert.js');
require('bootstrap/js/src/button.js');
require('bootstrap/js/src/collapse.js');
require('bootstrap/js/src/modal.js');
require('bootstrap/js/src/tab.js');
require('bootstrap/js/src/tooltip.js');
require('slick-carousel');
require('base/components/spinner');

/* jQuery Plugins */
processInclude(require('brand_core/plugins/sticky'));
processInclude(require('brand_core/plugins/productLoader'));
processInclude(require('brand_core/plugins/consentTracking'));
processInclude(require('brand_core/plugins/searchWithSuggestions'));
processInclude(require('brand_core/plugins/dummyCarousel'));
