'use strict';

var Drift = require('drift-zoom').default;
var throttle = require('lodash/throttle');
var base = require('brand_core/product/detail');
var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var nextArrowVertical = require('../templates/slickArrowNextVertical');
var prevArrowVertical = require('../templates/slickArrowPrevVertical');
var carousel = require('brand_core/components/carousel');
var quantitySelector = require('./quantitySelector');
var latestKnownScrollY = 0;
var $win = $(window);

/**
 * Initializes product image carousel
 */
function initProductImageCarousel() {
    var selImageCarousel = '.js-pdp-product-image-carousel';
    var selThumbnailCarousel = '.js-pdp-product-thumbnail-carousel';
    var $imageCarousel = $(selImageCarousel);
    var $thumbnailCarousel = $(selThumbnailCarousel);
    var thumbnailCount = $thumbnailCarousel.find('img').length;
    var thumbnailSlideLimit = 4;

    carousel($imageCarousel, {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: false,
        asNavFor: selThumbnailCarousel,
        nextArrow: nextArrow,
        prevArrow: prevArrow
    });

    carousel($thumbnailCarousel, {
        infinite: thumbnailCount > thumbnailSlideLimit,
        centerMode: thumbnailCount > thumbnailSlideLimit,
        slidesToScroll: thumbnailCount > thumbnailSlideLimit ? 1 : thumbnailCount,
        slidesToShow: thumbnailCount > thumbnailSlideLimit ? thumbnailSlideLimit : thumbnailCount,
        arrows: true,
        focusOnSelect: true,
        vertical: true,
        centerPadding: '0.625rem',
        asNavFor: selImageCarousel,
        nextArrow: nextArrowVertical,
        prevArrow: prevArrowVertical
    });
}

/**
 * Close review notification modal
 */
function closeReviewModalNotification() {
    $('.review-modal-notification').on('click', function () {
        $('#reviewSubmittedModal').modal('hide');
    });
}

/**
 * Initializes drift zoom
 */
function initDriftZoom() {
    $('.js-zoom-image').each(function (index, imageElement) {
        new Drift(this, { //eslint-disable-line
            inlinePane: true,
            containInline: true,
            hoverBoundingBox: true,
            zoomFactor: 2,
            touchDelay: 500
        });

        // Fix for instant inpane-zooming. No scrolling possible on touch devices
        imageElement.addEventListener('touchforcechange', function (event) { // eslint-disable-line
            if (event.changedTouches[0].force > 0.1) {
                event.preventDefault();
                return false;
            }
        });
    });
}

/**
 * Updates poduct id on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateProductIdAfterAttributeSelect(product, $productContainer) {
    var pid = $productContainer.data('pid');
    $('.js-product-cta-placeholder').parent().data('pid', pid);
}

/**
 * Updates carousel images on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $container - product detail section element that contains carousel
 */
function updateCarouselAfterAttributeSelect(product, $container) {
    var largeImageUrls = product.images.large || [];
    var smallImageUrls = product.images.small || [];
    var hiresImageUrls = product.images['hi-res'] || [];
    var $imageCarousel = $container.find('.js-pdp-product-image-carousel');
    var $thumbnailCarousel = $container.find('.js-pdp-product-thumbnail-carousel');
    var $imageRef = $imageCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $thumbnailRef = $thumbnailCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $productCtaImage = $container.find('.js-product-cta-image-ph img');
    var imagesFragment = document.createDocumentFragment();
    var thumbnailsFragment = document.createDocumentFragment();

    if (largeImageUrls.length !== smallImageUrls.length) {
        smallImageUrls = largeImageUrls;
    }

    if (largeImageUrls.length !== hiresImageUrls.length) {
        hiresImageUrls = largeImageUrls;
    }

    // Prepare new carousel images
    largeImageUrls.forEach(function (image, i) {
        var $image = $imageRef.clone();

        if ($image.length === 0) return;

        $image.attr('src', image.url);
        $image.attr('alt', image.alt);

        // Update zoom images
        $image.attr('data-zoom', hiresImageUrls[i].url);

        // Append new image to fragment
        imagesFragment.appendChild($image.get(0));
    });

    // Prepare new navigation thumbnails
    if (smallImageUrls && $thumbnailRef.length) {
        smallImageUrls.forEach(function (image) {
            thumbnailsFragment.appendChild(
                $thumbnailRef.clone()
                    .attr('src', image.url)
                    .attr('alt', image.alt)
                    .get(0)
            );
        });
    }

    // Refresh image carousel content
    $imageCarousel
        .slick('unslick')
        .html(imagesFragment);

    // Refresh thumbnail carousel content
    $thumbnailCarousel
        .data('count', (thumbnailsFragment.children || thumbnailsFragment.childNodes).length + '.0')
        .slick('unslick')
        .html(thumbnailsFragment);

    // Updata cta image
    if (largeImageUrls) {
        $productCtaImage.attr('src', largeImageUrls[0].url);
        $productCtaImage.attr('alt', largeImageUrls[0].alt);
    }

    // Reinitialize carousel
    initProductImageCarousel();

    // Reinitalize zoom
    initDriftZoom();
}

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {string} attr.values.displayValue - Attribute display value
 * @param {Object} attr.values.images - Attribute images
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processSwatchValues(attr, $productContainer) {
    var $selectedSwatch = $productContainer.find('.js-selected-swatch');
    var $selectedSwatchName = $productContainer.find('.js-selected-swatch-name');

    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer
            .find('[data-attr="' + attr.id + '"] [data-attr-value="' + attrValue.value + '"]');
        var $swatchAnchor = $attrValue.parent();

        $attrValue.removeClass('selectable unselectable selected');
        $attrValue.addClass(attrValue.selectable ? 'selectable' : 'unselectable');
        $swatchAnchor.attr('href', 'javascript:void(0)'); //eslint-disable-line

        if (attrValue.selected) {
            $attrValue.addClass('selected');
            $attrValue.closest('.js-attribute-container').removeClass('open');
            $selectedSwatch.removeClass();
            $selectedSwatch.attr('data-attr-value', attrValue.value);
            $selectedSwatch.addClass(attr.id + '-value swatch-circle swatch-value js-selected-swatch');
            $selectedSwatch.css('background-image', 'url("' + attrValue.images.swatch[0].url + '")');
            $selectedSwatchName.html(attrValue.displayValue);
        } else if (attrValue.url) {
            $swatchAnchor.attr('href', attrValue.url);
        }
    });
}

/**
 * Process attribute values associated with an attribute that does not have image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processNonSwatchValues(attr, $productContainer) {
    var $select = $productContainer
        .find('[data-attr="' + attr.id + '"] select.select-' + attr.id);

    // Update default option's url
    $select.find('.js-attr-default-option')
        .attr('value', attr.resetUrl);


    attr.values.forEach(function (attrValue) {
        $select.find('[data-attr-value="' + attrValue.value + '"]')
            .attr('value', attrValue.url)
            .attr('disabled', !attrValue.selectable);

        var $attrButton = $('.js-select-attribute[data-attr-id="' + attr.id + '"][data-attr-value="' + attrValue.value + '"]');
        $attrButton[attrValue.selected ? 'addClass' : 'removeClass']('selected');
        $attrButton[attrValue.selectable ? 'removeClass' : 'addClass']('disabled');
    });

    // $select.selectpicker('refresh');
}

/**
 * Updates attribute data on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAttributesAfterAttributeSelect(product, $productContainer) {
    if (product.variationAttributes) {
        // Currently, the only attribute type that has image swatches is Color.
        var attrsWithSwatches = {
            color: 1
        };

        product.variationAttributes.forEach(function (attr) {
            if (attrsWithSwatches[attr.id]) {
                processSwatchValues(attr, $productContainer);
            } else {
                processNonSwatchValues(attr, $productContainer);
            }
        });
    }
}

/**
 * Updates quantity input and buttons on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateQuantityAfterAttributeSelect(product, $productContainer) {
    if ($productContainer.parent('.bonus-product-item').length === 0) {
        var $quantityBtn = $productContainer.find('.js-quantity-btn');
        var $quantityInput = $productContainer.find('.js-quantity-input');
        var current = $quantityInput.val();
        var min = 1;
        var max = 0;

        if (product.quantities && product.quantities.length) {
            min = product.quantities[0].value;
            max = product.quantities[product.quantities.length - 1].value;
        }

        var newQuantity = quantitySelector.getQuantity(0, current, min, max);

        if (product.available) {
            $quantityInput.val(newQuantity);
            $quantityInput.removeClass('disabled');
            $quantityBtn.filter('.js-quantity-decrease-btn')[min >= newQuantity ? 'addClass' : 'removeClass']('disabled');
            $quantityBtn.filter('.js-quantity-increase-btn')[max <= newQuantity ? 'addClass' : 'removeClass']('disabled');
        } else {
            $quantityInput.val(0);
            $quantityInput.addClass('disabled');
            $quantityBtn.addClass('disabled');
        }
    }
}

/**
 * Updates availability on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAvailabilityAfterAttributeSelect(product, $productContainer) {
    var $availabilityContainer = $productContainer.find('.js-availability-container');

    // Update availability data attributes on contanier
    $availabilityContainer
        .data('ready-to-order', product.readyToOrder)
        .data('available', product.available);

    if (product.availability &&
        product.availability.messages &&
        product.availability.messages.length) {
        // Insert message string to DOM
        $productContainer
            .find('.js-availability-msg')
            .html(product.availability.messages.join(', '));
    }

    var $addToCArtContainer = $productContainer.find('.js-add-to-cart-btn-container');
    $addToCArtContainer.removeClass('product-available product-unavailable');
    $addToCArtContainer.addClass(product.available ? 'product-available' : 'product-unavailable');
}

/**
 * Updates promotions on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updatePromotionsAfterAttributeSelect(product, $productContainer) {
    var html = '';
    if (product.promotions && product.promotions.length) {
        product.promotions.forEach(function (promotion) {
            if (promotion.calloutMsg) {
                var learnMoreBtn = promotion.details ?
                    '<button type="button" class="link js-show-promotion-details"' +
                        'data-details="' + promotion.details + '">' +
                        window.RA_RESOURCE['common.button.learn.more'] +
                    '</button>' : '';
                html +=
                    '<div class="callout">' +
                        promotion.calloutMsg +
                        learnMoreBtn +
                    '</div>';
            }
        });
    }
    $productContainer.find('.js-promotions').html(html);
}

/**
 * Updates title on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateTitleAfterAttributeSelect(product, $productContainer) {
    if (product && product.productName) {
        $productContainer.find('.js-name').html(product.productName);
    }
}

/**
 * Updates short description on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateDescriptionAfterAttributeSelect(product, $productContainer) {
    if (product && product.shortDescription) {
        $productContainer.find('.js-short-description').html(product.shortDescription);
    }
}

/**
 * Updates url on each attribute change
 * @param {Object} product - product object including images array
 */
function updateUrlAfterAttributeSelect(product) {
    if (product && product.selectedProductUrl) {
        history.replaceState({}, document.title, product.selectedProductUrl);
    }
}

/**
 * Updates badges on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateBadgesAfterAttributeSelect(product, $productContainer) {
    var $badges = $productContainer.find('.js-badges');
    var $badgeTemplate = $productContainer.find('.js-badge-template');
    if (product.badges && product.badges.length) {
        $badges.html('');
        product.badges.forEach(function (badge) {
            $badges.append($badgeTemplate.clone()
                .attr('data-badge-value', badge.value)
                .text(badge.displayValue)
                .removeClass('js-badge-template d-none'));
        });
        $badges.append($badgeTemplate);
        $badges.removeClass('d-none');
    } else {
        $badges.addClass('d-none');
    }
}

/**
 * Moves product info to header or vice versa after scrolling
 * @param {Object} el - jQuery elements object
 */
function toggleCtaOnScroll(el) {
    var currentScrollY = latestKnownScrollY;
    var detailHeight = el.$details.height();

    if (currentScrollY >= detailHeight && $win.width() >= window.RA_BREAKPOINTS.xl) {
        if (el.$placeholder.parent().hasClass('d-none') === false) return;
        el.$details.height(el.$details.height());
        el.$placeholder.append(el.$details.children());
        el.$placeholder.parent().removeClass('d-none');
        el.$placeholderName.append(el.$detailsName.children());
        el.$placeholderDescription.append(el.$detailsDescription.children().length ? el.$detailsDescription.children() : el.$detailsDescription.contents());
        el.$placeholderAddToWishlist.append(el.$detailsAddToWishlist.children());
        el.$placeholderImage.append(el.$detailsImage.children());
        el.$placeholderBtn.append(el.$infoBtn.children());
        el.$placeholderPrice.append(el.$infoPrice.children());
        el.$placeholderAttributes.append(el.$infoAttributes.children());
    } else {
        if (el.$placeholder.parent().hasClass('d-none')) return;
        el.$detailsName.append(el.$placeholderName.children());
        el.$detailsDescription.append(el.$placeholderDescription.children().length ? el.$placeholderDescription.children() : el.$placeholderDescription.contents());
        el.$detailsAddToWishlist.append(el.$placeholderAddToWishlist.children());
        el.$detailsImage.append(el.$placeholderImage.children());
        el.$infoBtn.append(el.$placeholderBtn.children());
        el.$infoPrice.append(el.$placeholderPrice.children());
        el.$infoAttributes.append(el.$placeholderAttributes.children());
        el.$details.height('auto');
        el.$details.append(el.$placeholder.children());
        el.$detailsCarousel.slick('setPosition');
        el.$detailsThumbnailCarousel.slick('setPosition');
        el.$placeholder.parent().addClass('d-none');
    }
}

/**
 * Initializes product call to action box
 * that will be visible after scrolling
 */
function initProductCta() {
    var el = {
        $details: $('.js-product-detail-info'),
        $detailsImage: $('.js-image-container'),
        $detailsName: $('.js-name-container'),
        $detailsDescription: $('.js-short-description'),
        $detailsAddToWishlist: $('.js-add-to-wishlist-contianer'),
        $detailsCarousel: $('.js-pdp-product-image-carousel'),
        $detailsThumbnailCarousel: $('.js-pdp-product-thumbnail-carousel'),

        $info: $('.js-product-info'),
        $infoPrice: $('.js-price-container'),
        $infoBtn: $('.js-add-to-cart-btn-container'),
        $infoAttributes: $('.js-attributes-container'),

        $placeholder: $('.js-product-cta-placeholder'),
        $placeholderBtn: $('.js-product-cta-btn-ph'),
        $placeholderName: $('.js-product-cta-name-ph'),
        $placeholderImage: $('.js-product-cta-image-ph'),
        $placeholderDescription: $('.js-short-description-ph'),
        $placeholderAddToWishlist: $('.js-product-cta-wishlist-ph'),
        $placeholderPrice: $('.js-product-cta-price-ph'),
        $placeholderAttributes: $('.js-product-cta-attributes-ph')
    };

    el.$placeholder.parent().data('pid', el.$details.data('pid'));

    var throttleScroll = throttle(function () {
        toggleCtaOnScroll(el);
    }, 100);

    // Attach scroll event
    $(window).on('scroll', function () {
        // Cache latest scrollY
        latestKnownScrollY = window.scrollY || window.pageYOffset;

        // Toggle call to action box
        throttleScroll();
    });
}

/**
 * Updates wishlist button on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateWishlistButtonAfterAttributeSelect(product, $productContainer) {
    $.get(window.RA_URL['Wishlist-AddToWishlistButton'], { pid: product.id }, function (res) {
        $productContainer.find('.js-add-to-wishlist-contianer').replaceWith(res);
    });
}

base.initShippingInformationModal = function () {
    var $content = $('.js-shipping-info-content');

    // Remove shipping information button and modal if there is no content in slot
    if ($content.children().length === 0) {
        $content.closest('.js-shipping-info-container').remove();
    }
};

base.initTabsWithSlotContent = function () {
    var $content = $('.js-tab-slot-content');

    // Remove tab if there is no content in slot
    if ($content.children().length === 0) {
        var $container = $content.closest('.js-tab-slot-container');
        var $title = $('#' + $container.attr('aria-labelledby'));
        $title.remove();
        $container.remove();
    }
};


base.updateProductAfterAttributeSelect = function () {
    $('body').on('product:afterAttributeSelect', function (e, response) {
        updateProductIdAfterAttributeSelect(response.data.product, response.container);
        updateCarouselAfterAttributeSelect(response.data.product, response.container);
        updateQuantityAfterAttributeSelect(response.data.product, response.container);
        updateAttributesAfterAttributeSelect(response.data.product, response.container);
        updatePromotionsAfterAttributeSelect(response.data.product, response.container);
        updateTitleAfterAttributeSelect(response.data.product, response.container);
        updateDescriptionAfterAttributeSelect(response.data.product, response.container);
        updateBadgesAfterAttributeSelect(response.data.product, response.container);
        updateWishlistButtonAfterAttributeSelect(response.data.product, response.container);
        updateUrlAfterAttributeSelect(response.data.product);
        if (response.data.pageDataLayerEnhancedEcommerce) {
            window.dataLayer = window.dataLayer || [];
            response.data.pageDataLayerEnhancedEcommerce.forEach(function (p) {
                window.dataLayer.push(p);
            });
        }
    });

    $('body').on('product:updateAvailability', function (e, response) {
        updateAvailabilityAfterAttributeSelect(response.product, response.$productContainer);
    });

    $('.js-attributes-container select:not(.js-quantity-select)').each(function () {
        var $select = $(this);
        $select.val($select.find('[selected]').val());
        // $select.selectpicker('refresh');
    });
};

base.initActions = function () {
    $(document)
        .on('click', '.js-select-attribute', function () {
            var $this = $(this);

            // Don't perform a http request if clicked attribute is already selected
            if ($this.hasClass('selected')) {
                return;
            }

            var $parent = $this.closest('.js-attribute-container');
            var $option = $parent.find('select option[data-attr-value="' + $this.attr('data-attr-value') + '"]');
            $option.parent().val($option.val()).trigger('change');
        })
        .on('click', '.js-show-promotion-details', function () {
            var $this = $(this);
            var details = $this.data('details');
            var $details = $('<div class="callout box-details"><i class="icon-close js-hide-promotion-details"></i>' + details + '</div>');
            $this.parent().after($details);
            $this.fadeOut();
            $details.slideDown();
        })
        .on('click', '.js-hide-promotion-details', function () {
            var $this = $(this);
            var $details = $this.parent();
            $details.slideUp(function () {
                $details.prev().find('.js-show-promotion-details').fadeIn();
                $details.remove();
            });
        })
        .on('click', '.js-attribute-colorfamily-tab', function () {
            var $this = $(this);
            var familyID = $this.data('colorFamily');
            var $attributesContainer = $this.closest('.js-attributes-container');

            if (familyID === 'all') {
                $attributesContainer.find('.js-attribute-item-link').removeClass('d-none');
            } else {
                $attributesContainer.find('.js-attribute-item-link').addClass('d-none');
                $attributesContainer.find('.js-attribute-item-link[data-color-family="' + familyID + '"]').removeClass('d-none');
            }
        })
        .on('mouseup', function (e) {
            var $container = $('.js-attribute-container');

            // if the target of the click isn't the container nor a descendant of the container
            if ($container.is(e.target) === false && $container.has(e.target).length === 0) {
                $container.removeClass('open');
            }
        })
        .on('show.bs.modal', '.js-notify-me-modal', function () {
            var $this = $(this);

            if (!$this.closest('.js-bis').length) {
                return;
            }

            $('body').append($this);
        });
};

base.initDriftZoom = initDriftZoom;
base.initProductCta = initProductCta;
base.initQuantitySelector = quantitySelector.init;
base.initProductImageCarousel = initProductImageCarousel;
base.closeReviewModalNotification = closeReviewModalNotification;

module.exports = base;
