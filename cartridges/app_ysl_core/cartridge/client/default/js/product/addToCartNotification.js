'use strict';

var autoClose;
var autoCloseWaitBefore = 4000; // 4 secs

/**
 * Closes product notification and backdrop elements
 */
function closeProductNotification() {
    var $addedProdNotification = $('.js-added-product-notification');
    var $addedProdNotificationBackdrop = $('.js-added-product-notification-backdrop');
    var $addedProdNotificationExtras = $('.js-added-product-extras');
    $addedProdNotification.fadeOut('fast').removeClass('d-flex');
    $addedProdNotificationBackdrop.fadeOut('fast');
    $addedProdNotificationExtras.removeClass('d-flex');
    $addedProdNotificationExtras.addClass('d-none');
}
/**
 * Prepares html string of an image
 * @param {Object} image - Image object
 * @return {string} Html string of image
 */
function prepareImageHtmlStr(image) {
    return '<img src="' + image.url + '" alt"' + image.alt + '" title="' + image.title + '" />';
}

/**
 * Updates the Mini-Cart, shows new added notification after the customer has pressed the "Add to Cart" button
 * @param {Event} e - event object
 * @param {string} response - ajax response from clicking the add to cart button
 */
function handlePostCartAdd(e, response) {
    // Clear auto close timeout
    clearTimeout(autoClose);

    // Update item count in cart
    $('.minicart').trigger('count:update', response);

    // Assign html containers to variables
    var $addedProdNotification = $('.js-added-product-notification');
    var $addedProdNotificationBackdrop = $('.js-added-product-notification-backdrop');
    var $addedProdNotificationDescription = $('.js-added-product-details');
    var $addedProdNotificationExtras = $('.js-added-product-extras');
    var $addedProdNotificationMultipleProducts = $('.js-added-product-multiple-products');
    var $addedProdNotificationMultipleProductImages = $('.js-added-product-multiple-product-images').empty();
    var $addedProdNotificationImg = $('.js-added-product-image');
    var $addedProdNotificationImgLink = $('.js-added-product-image a').empty();
    var $addedProdNotificationName = $('.js-added-product-name a').empty();
    var $addedProdNotificationOptions = $('.js-added-product-quantity').empty();
    var $addedProdNotificationPrice = $('.js-added-product-price').empty();
    var $addedProdNotificationListPrice = $('.js-added-product-list-price').empty();
    var $addedProdNotificationSubtotal = $('.js-added-product-subtotal').empty();
    var $addedProdNotificationSingular = $('.js-added-product-notification-singluar');
    var $addedProdNotificationPlural = $('.js-added-product-notification-plural');
    var qtyResourceStr = $addedProdNotificationOptions.data('resource-qty');
    var multipleImagesHtmlArr = [];
    var image;

    // Clear product options before assigning them with response
    var optionsStr = '';

    // check if product is out of stock
    if (!response.error && response.addedProducts && response.addedProducts.length) {
        var addedProducts = response.addedProducts.map(function (addedProductObj) {
            return addedProductObj.pid;
        });

        // Check response for clicked product to use product detail data in new added product notification
        for (var i = 0; i < response.cart.items.length; i++) {
            var item = response.cart.items[i];

            if (addedProducts.indexOf(item.id) > -1) {
                if (addedProducts.length === 1) {
                    // Create product image dynamically with response data
                    if (Object.keys(item.images.small).length) {
                        image = item.images.small[0];
                        $addedProdNotificationImgLink
                            .attr('href', window.RA_URL['Product-Show'].replace('PRODUCT_ID', item.id))
                            .html(prepareImageHtmlStr(image));
                    } else {
                        $addedProdNotificationImg.empty();
                    }

                    // Set product name with response
                    $addedProdNotificationName
                        .html(item.productName)
                        .attr('href', window.RA_URL['Product-Show'].replace('PRODUCT_ID', item.id));

                    // Loop trough product details like size, volume etc. Add them to new added product notification
                    $addedProdNotificationOptions.hide();
                    if (qtyResourceStr) {
                        optionsStr += '<span>' + qtyResourceStr.replace('{0}', item.instantQuantity) + '</span>';
                    }

                    if (item.variationAttributes) {
                        for (var j = 0; j < item.variationAttributes.length; j++) {
                            optionsStr += '<span>' + item.variationAttributes[j].displayValue + '</span>';
                        }
                    }

                    $addedProdNotificationOptions.html(optionsStr).show();

                    // Add prod price to new added product notification
                    if (item.instantPriceTotal && item.instantPriceTotal.list) {
                        $addedProdNotificationListPrice.html(item.instantPriceTotal.list.formatted);
                    }
                    if (item.instantPriceTotal && item.instantPriceTotal.sales) {
                        $addedProdNotificationPrice.html(item.instantPriceTotal.sales.formatted);
                    }

                    break;
                } else if (Object.keys(item.images.small).length) {
                    // Create multiple product images dynamically
                    image = item.images.small[0];
                    multipleImagesHtmlArr.push(prepareImageHtmlStr(image));
                }
            }
        }

        if (multipleImagesHtmlArr.length) {
            // Insert images into DOM
            $addedProdNotificationMultipleProductImages
                .html(multipleImagesHtmlArr.join('<i class="icon-plus"></i>'));

            // Insert "x items selected" text into DOM
            $addedProdNotificationPlural
                .html($addedProdNotificationPlural
                    .data('text').replace('{0}', multipleImagesHtmlArr.length))
                .removeClass('d-none');

            $addedProdNotification.addClass('miniCart-multiple-products');
            $addedProdNotificationMultipleProducts.removeClass('d-none');
            $addedProdNotificationImg.addClass('d-none');
            $addedProdNotificationDescription.addClass('d-none').removeClass('d-flex');
            $addedProdNotificationSingular.addClass('d-none');
        } else {
            $addedProdNotification.removeClass('miniCart-multiple-products');
            $addedProdNotificationMultipleProducts.addClass('d-none');
            $addedProdNotificationImg.removeClass('d-none');
            $addedProdNotificationDescription.removeClass('d-none').addClass('d-flex');
            $addedProdNotificationSingular.removeClass('d-none');
            $addedProdNotificationPlural.addClass('d-none');
        }

        // Add item subtotal
        $addedProdNotificationSubtotal.html(response.cart.totals.subTotal);

        // Show new added product notification backdrop
        $addedProdNotificationBackdrop.fadeIn('fast');

        // Show new added product notification
        $addedProdNotification.addClass('d-flex');

        // Show discounts if avalible
        if (response.cart.isQualifiedForFreeShipping) {
            $addedProdNotificationExtras.removeClass('d-none');
            $addedProdNotificationExtras.addClass('d-flex');
        }
    }

    // Auto-close new added product notification & backdrop after 4sec
    autoClose = setTimeout(closeProductNotification, autoCloseWaitBefore);
}

module.exports = function () {
    var $body = $('body');
    var $addedProdNotification = $('.js-added-product-notification');

    $body.on('product:afterAddToCart', handlePostCartAdd);

    // Close new added product notification & backdrop clicking outside
    $body.on('click', function (e) {
        if (!$(e.target).is('.js-added-product-notification, .js-added-product-notification *')) {
            closeProductNotification();
        }
    });

    // Close new added product notification & backdrop with close button
    $body.on('click', '.js-added-product-close', closeProductNotification);

    // Close new added product notification & backdrop clicking backdrop
    $body.on('click', '.js-added-product-notification-backdrop', closeProductNotification);

    // Close new added product notification hovering minicart icon
    $('.js-minicart-trigger').on('mouseenter', closeProductNotification);

    // Do not close new added product notification while mouse is on it
    $addedProdNotification.on('mouseenter', function () {
        clearTimeout(autoClose);
    });

    // Auto-close new added product notification & backdrop after 4sec
    $addedProdNotification.on('mouseleave', function () {
        autoClose = setTimeout(closeProductNotification, autoCloseWaitBefore);
    });
};
