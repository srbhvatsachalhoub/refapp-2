'use strict';

var base = require('brand_core/checkout/address');
var addressCustom = require('./addressCustom');

base.addressCustom = addressCustom.initialize;

module.exports = base;
