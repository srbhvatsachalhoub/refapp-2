'use strict';

module.exports = function () {
    var $closeCategoryButton = $('.js-close-category');

    $closeCategoryButton.on('click', function () {
        var $dropdownToggle = $(this);
        var $dropdownMenu = $dropdownToggle.closest('.dropdown-menu');
        var $categoryTrigger = $dropdownMenu.parent('.js-dropdown-click');
        var $categoryTriggerLink = $categoryTrigger.find('.js-dropdown-toggle-btn.expanded');

        $categoryTrigger.removeClass('show');
        $categoryTriggerLink.removeClass('expanded');
    });

    $('.js-main-menu-close-overlay').on('click', function () {
        $('.js-toggle-navbar').first().click();
    });
};
