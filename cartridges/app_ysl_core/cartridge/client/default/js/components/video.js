'use strict';

var sources = {
    youtube: function (id) {
        return 'https://www.youtube.com/embed/' + id + '?autoplay=1&rel=0';
    }
};

module.exports = function () {
    $('.js-video').on('load', function () {
        $('.js-load-video.loading')
            .removeClass('loading')
            .css('opacity', 0)
            .css('z-index', -1)
            .spinner()
            .stop();
    });
    $('.js-load-video').on('click', function () {
        var $this = $(this);
        var id = $this.data('id');
        var $playlist = $this.closest('.js-video-playlist');
        var $btn = $playlist.find('.js-select-video[data-id="' + id + '"]');
        var source = sources[$btn.data('source')];
        if (source) {
            $this.spinner().start();
            $this.addClass('loading');
            $this.closest('.js-video-playlist').find('.js-video')
                .attr('src', source(id));
        }
    });
    $(document).on('click', '.js-select-video', function () {
        var $this = $(this);
        var $playlist = $this.closest('.js-video-playlist');
        var $cover = $playlist.find('.js-load-video');
        var $img = $cover.find('img');
        var cover = $this.data('cover');
        $cover.data('id', $this.data('id'));
        $playlist.find('.js-video-title').html($this.data('title'));
        $playlist.find('.js-video-description').html($this.data('description'));
        if ($img.length === 0) {
            $cover.append('<img src="' + cover + '" />');
        } else {
            $img.one('load', function () {
                $cover.css('opacity', 1).css('z-index', 1);
                $playlist.find('.js-video').removeAttr('src');
            }).attr('src', cover);
        }
    });
    $('.js-video-playlist .js-select-video').each(function () {
        var $this = $(this);
        var $thumbnails = $this.closest('.js-video-thumbnails');
        if ($thumbnails.children().first().has($this).length) {
            $this.click();
        }
    });
};
