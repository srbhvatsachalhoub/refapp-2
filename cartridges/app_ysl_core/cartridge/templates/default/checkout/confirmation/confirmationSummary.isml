<isinclude template="/components/modules" sf-toolkit="off" />
<isloop var="shippingModel" items="${pdict.order.shipping}" status="shippingLoop">
    <isif condition="${shippingLoop.first}">

        <div class="row confirmation-bg flex-fill">
            <div class="col-12 confirmation-header font-condensed text-white confirmation-bg-dark text-left text-uppercase">${Resource.msg('title.order.shipping.and.billing', 'confirmation', null)}</div>
            <iscomment>Contact Details</iscomment>
            <div class="col-lg-4 confirmation-contact-border py-3 py-lg-0 my-lg-3">
                <p class="confirmation-shipping-inline-title">
                    ${Resource.msg('title.contact.details', 'confirmation', null)}
                </p>
                <div>
                    <isif condition="${pdict.order.billing.billingAddress.address.firstName !== null && pdict.order.billing.billingAddress.address.lastName !== null}">
                        <span>
                            ${pdict.order.billing.billingAddress.address.firstName}
                            ${pdict.order.billing.billingAddress.address.lastName}
                        </span>
                    </isif>
                </div>
                <div>
                    <span>${pdict.order.orderEmail}</span>
                </div>
            </div>

            <iscomment>Shipping Address</iscomment>
            <div class="col-lg-4 confirmation-shipped confirmation-shipped-border py-3 py-lg-0 my-lg-3 px-lg-4 px-xl-5">
                <div class="single-shipping" data-shipment-summary="${shippingModel.UUID}">
                    <isif condition="${shippingModel.shippingAddress !== null}">
                        <isset name="address" value="${shippingModel.shippingAddress}" scope="page"/>
                    <iselse/>
                        <isset name="address" value="{}" scope="page"/>
                    </isif>
                    <div class="summary-details shipping">
                        <p class="confirmation-shipping-inline-title">
                            ${Resource.msg('title.order.shipping.address', 'confirmation', null)}
                        </p>
                        <isinclude template="checkout/addressSummary" />
                        <span>
                            ${shippingModel.shippingAddress && shippingModel.shippingAddress.phone ? shippingModel.shippingAddress.phone : ''}
                        </span>
                    </div>
                </div>
            </div>

            <iscomment>Billing Address</iscomment>
            <div class="col-lg-4 confirmation-sold confirmation-sold-border py-3 py-lg-0 my-lg-3 px-lg-4 px-xl-5">
                <p class="confirmation-shipping-inline-title">
                    ${Resource.msg('title.order.billing.address', 'confirmation', null)}
                </p>
                <isif condition="${pdict.order.billing.billingAddress.address.firstName !== null && pdict.order.billing.billingAddress.address.lastName !== null}">
                    <span class="confirmation-billing-line">
                        ${pdict.order.billing.billingAddress.address.firstName}
                        ${pdict.order.billing.billingAddress.address.lastName}
                    </span>
                </isif>
                <isaddress address_value="${pdict.order.billing.billingAddress.address}" />
                <isif condition="${pdict.order.billing.billingAddress.address.phone !== null}">
                    <span class="confirmation-billing-line" dir="ltr">
                        ${pdict.order.billing.billingAddress.address.phone}
                    </span>
                </isif>
            </div>
        </div>

        <div class="row confirmation-bg flex-fill mt-4">
            <div class="col-12 confirmation-subheader text-white confirmation-bg-dark text-left text-uppercase">
                ${Resource.msg('title.order.shipping.and.payment', 'confirmation', null)}
            </div>

            <div class="col-6 my-3">
                <iscomment>Shipping information</iscomment>
                <p class="confirmation-shipping-inline-title">
                    ${Resource.msg('title.order.shipping.method', 'confirmation', null)}
                </p>
                <div class="confirmation-shipping-method">
                    <div class="confirmation-shipping-name">
                        ${shippingModel.selectedShippingMethod.displayName}
                        ${Resource.msg('confirmation.shipping', 'confirmation', null)},
                        ${pdict.order.totals.totalShippingCost}
                    </div>
                    <div class="confirmation-shipping-arrival-time">
                        <isif condition="${shippingModel.selectedShippingMethod.estimatedArrivalTime}">
                            ${shippingModel.selectedShippingMethod.estimatedArrivalTime}
                        </isif>
                    </div>
                </div>
            </div>

            <iscomment>Payment information</iscomment>
            <div class="col-6 my-3">
                <div class="confirmation-payment-info">
                    <div class="credit-card-type">
                        <p class="confirmation-shipping-inline-title">
                            ${Resource.msg('title.order.payment.method', 'confirmation', null)}
                        </p>
                        <isloop items="${pdict.order.billing.payment.selectedPaymentInstruments}" var="payment">
                            <isif condition="${payment.paymentMethod === 'CREDIT_CARD'}">
                                <isset name="cardType" value="${payment.type.toLowerCase().replace(/\s/g, '')}" scope="page"/>
                                <div class="d-flex">
                                    <span class="text-uppercase">
                                        ${Resource.msg('label.' + cardType, 'creditCard', null)}
                                    </span>
                                    <span>&nbsp;</span>
                                    <span>*${payment.lastFour}</span>
                                    <span class="mx-3"></span>
                                    <iscreditcard creditcard_type="${payment.type}" />
                                </div>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'COD'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.payment.type.cod', 'confirmation', 'Cash on Delivery')},
                                    ${pdict.order.totals.serviceFee.formatted}
                                </span>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'GIFT_CERTIFICATE'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.payment.giftcertificate', 'confirmation', 'Gift Certificate')}
                                </span>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'DW_APPLE_PAY'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.paid.using.apple.pay', 'applePay', 'Apple Pay')}
                                </span>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'KNET'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.payment.type.knet', 'confirmation', 'KNET')}
                                </span>
                            </isif>
                        </isloop>
                    </div>
                </div>
            </div>
        </div>

    </isif>
</isloop>
