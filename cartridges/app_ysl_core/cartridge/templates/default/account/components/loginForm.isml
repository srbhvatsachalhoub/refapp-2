<isinclude template="/components/modules" sf-toolkit="off" />
<h4>
    ${Resource.msg('link.signin', 'login', null)}
</h4>
<h6 class="pt-3 pb-3 registered-customers-header">
    ${Resource.msg('label.registered.members', 'login', null)}
</h6>
<isinclude template="account/components/oauth" />
<isif condition="${pdict.errorMessage}">
    <div class="alert alert-danger alert-dismissible valid-cart-error fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        ${pdict.errorMessage}
    </div>
</isif>

<form action="${pdict.actionUrl}" class="js-login-form" method="POST" name="login-form">
    <div class="form-group required">
        <label class="form-control-label" for="login-form-email">
            ${Resource.msg('label.email.address', 'forms', null)}
        </label>
        <input  id="login-form-email"
                class="form-control"
                name="loginEmail"
                pattern="^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$|^[0-9\+]{10,15}$"
                required
                aria-required="true"
                aria-describedby="form-email-error"
                data-missing-error="<isprint value="${Resource.msg('error.message.required.email', 'forms', null)}" encoding="htmldoublequote" />"
                data-pattern-mismatch="<isprint value="${Resource.msg('error.message.parse.email', 'forms', null)}" encoding="htmldoublequote" />"
        />
        <div class="invalid-feedback" id="form-email-error"></div>
    </div>

    <div class="form-group required position-relative">
        <label class="form-control-label" for="login-form-password">
            ${Resource.msg('label.input.login.password', 'login', null)}
        </label>
        <input  type="password"
                id="login-form-password"
                class="form-control js-input-password"
                name="loginPassword"
                required
                aria-required="true"
                aria-describedby="form-password-error"
                minlength="8"
                data-missing-error="<isprint value="${Resource.msg('error.message.required.password', 'forms', null)}" encoding="htmldoublequote" />"
                data-range-error="<isprint value="${Resource.msg('error.message.range.password', 'forms', null)}" encoding="htmldoublequote" />"
        />
        <span class="password-show-toggle js-password-show-toggle">
            <i class="icon-eye"></i>
        </span>
        <div class="invalid-feedback" id="form-password-error"></div>
    </div>

    <input  type="hidden"
            name="${pdict.csrf.tokenName}"
            value="${pdict.csrf.token}" />

    <div class="login-option-menu">
        <div class="form-group mb-0">
            <input  type="checkbox"
                    id="rememberMe"
                    name="loginRememberMe"
                    value="true" <isif condition="${pdict.rememberMe}">checked</isif>
            >
            <label for="rememberMe">
                ${Resource.msg('label.checkbox.rememberme', 'forms', null)}
            </label>
        </div>
        <div class="forgot-password">
            <a id="password-reset" class="page-reset-password-btn"
                title="${Resource.msg('link.forgot.password', 'forms', null)}"
                href="${URLUtils.url('Account-PasswordResetDialogForm')}"
                data-dismiss="modal"
                data-toggle="modal"
                data-target="#requestPasswordResetModal"
                data-remove-class-d-none=".js-reset-password-form-container"
                data-add-class-d-none=".js-reset-password-result"
                data-gtm-enhancedecommerce-onclick='{"event": "uaevent", "eventCategory": "log in form", "eventAction": "send", "eventLabel": "reset password"}'>
                ${Resource.msg('link.forgot.password', 'forms', null)}
            </a>
        </div>
    </div>
    <button type="submit" class="btn btn-block btn-primary col-md-6 col-lg-12 mt-5" 
            data-gtm-enhancedecommerce-onclick='{"event": "uaevent", "eventCategory": "log in", "eventAction": "social::regular", "eventLabel": "sucessfull"}'>
        ${Resource.msg('button.text.loginform', 'login', null)}
    </button>
</form>
