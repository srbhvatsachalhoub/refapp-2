'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;

    var model = new HashMap();
    model.image = ImageTransformation.getScaledImage(content.image);
    model.alt = content.alt;
    model.description = content.description;
    model.header = content.header;
    model.link = content.link;
    model.URL = content.URL;

    var cat = content.category;
    if (cat) {
        model.cid = cat.ID;
        model.URL = cat.custom && 'alternativeUrl' in cat.custom && cat.custom.alternativeUrl
            ? cat.custom.alternativeUrl
            : URLUtils.url('Search-Show', 'cgid', cat.getID()).toString();
    }

    return new Template('experience/components/commerce_assets/sliderHalfRow').render(model).text;
};
