'use strict';

var gtmEnhancedEcommerce = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerce');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

module.exports = function (object, product) { // eslint-disable-line no-unused-vars
    var CurrentSite = require('dw/system/Site').current;
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    gtmEnhancedEcommerce(object);

    Object.defineProperty(object.gtmEnhancedEcommerce, 'removeFromCart', {
        enumerable: true,
        value: {
            event: 'removeFromCart',
            eventCategory: 'Ecommerce',
            eventAction: 'Remove from Cart',
            ecommerce: {
                remove: {
                    products: [gtmEnhancedEcommerceDataLayerHelpers.getProductObject(object, false, 1)]
                }
            }
        }
    });
};
