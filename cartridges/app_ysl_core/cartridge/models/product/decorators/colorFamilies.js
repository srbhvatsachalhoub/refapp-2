'use strict';

module.exports = function (product) {
    if ('variationAttributes' in product && product.variationAttributes) {
        var Resource = require('dw/web/Resource');
        var colorFamiliesArr = [{
            value: 'all',
            displayValue: Resource.msg('product.colorfamilies.all', 'product', null)
        }];
        var HashMap = require('dw/util/HashMap');
        var colorFamilyMap = new HashMap();
        product.variationAttributes.forEach(function (attr) {
            if (attr && attr.attributeId === 'color' && attr.values && attr.values.length) {
                attr.values.forEach(function (variationValue) {
                    if ('colorFamily' in variationValue && variationValue.colorFamily) {
                        variationValue.colorFamily.forEach(function (colorFamily) {
                            if (!colorFamilyMap.containsKey(colorFamily.value)) {
                                colorFamilyMap.put(colorFamily.value, colorFamily);
                            }
                        });
                    }
                });
            }
        });
        if (!colorFamilyMap.empty) {
            var colorFamiliesValItr = colorFamilyMap.values().iterator();
            while (colorFamiliesValItr.hasNext()) {
                var colorFamily = colorFamiliesValItr.next();
                colorFamiliesArr.push(colorFamily);
            }
            colorFamiliesArr = colorFamiliesArr.sort(function (a, b) { // eslint-disable-line
                return a.value.localeCompare(b.value);
            });
            // var Resource = require('dw/web/Resource');
            // colorFamiliesArr.unshift(Resource.msg(function))
            Object.defineProperty(product, 'colorFamilies', {
                enumerable: true,
                value: colorFamiliesArr
            });
        }
    }
};
