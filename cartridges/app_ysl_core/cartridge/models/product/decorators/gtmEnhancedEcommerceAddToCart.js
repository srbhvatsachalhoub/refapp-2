'use strict';

var gtmEnhancedEcommerce = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerce');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

module.exports = function (object, product) { // eslint-disable-line no-unused-vars
    var CurrentSite = require('dw/system/Site').current;
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    gtmEnhancedEcommerce(object);

    Object.defineProperty(object.gtmEnhancedEcommerce, 'addToCart', {
        enumerable: true,
        value: {
            event: 'addToCart',
            eventCategory: 'Ecommerce',
            eventAction: 'Add to Cart',
            ecommerce: {
                currencyCode: gtmEnhancedEcommerceDataLayerHelpers.getCurrencyCode(object),
                add: {
                    products: [gtmEnhancedEcommerceDataLayerHelpers.getProductObject(object, false, 1)]
                }
            }
        }
    });
};
