'use strict';

var server = require('server');
server.extend(module.superModule);

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Show', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.productDetail(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Variation', function (req, res, next) {
    gtmEnhancedEcommerceDataLayerHelpers.productDetail(req, res);
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

module.exports = server.exports();
