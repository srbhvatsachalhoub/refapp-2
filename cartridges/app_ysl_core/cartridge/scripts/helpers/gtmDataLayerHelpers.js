/* eslint-disable no-param-reassign */
'use strict';
/* global request */

var CurrentSite = require('dw/system/Site').current;
/**
 * Gets pageType
 * @param {Object} res response object
 * @returns {string} pageType
 */
function getPageType(res) {
    var action = res.viewData.action;
    var pageType = '';
    switch (action) {
        case 'Home-Show':
            pageType = 'homepage';
            break;
        case 'Search-Show':
            var productSearch = res.viewData.productSearch;
            if (productSearch) {
                if (productSearch.isCategorySearch || productSearch.isRefinedCategorySearch) {
                    pageType = 'product results page';
                } else {
                    pageType = 'internal search';
                }
            }
            break;
        case 'Product-Show':
            pageType = 'product detail page';
            break;
        case 'Cart-Show':
            pageType = 'cart page';
            break;
        case 'Order-Confirm':
            pageType = 'transaction page';
            break;
        case 'Account-Show':
            pageType = 'my account';
            break;
        case 'Stores-Find':
            pageType = 'store locator page';
            break;
        case 'Checkout-Begin':
            pageType = 'checkout page';
            break;
        default:
            pageType = 'others';
            break;
    }
    return pageType;
}
/**
 * Handles category tags
 * @param {*} category - Current category
 * @param {Object} dataLayer - dataLayer object
 */
function handleCategoryFields(category, dataLayer) {
    if (!category) {
        return;
    }

    dataLayer.searchCategory = category.ID;
}

/**
 * Handles search related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 * @constructor
 */
function handleCategoryRelatedFields(req, res, dataLayer) {
    dataLayer.pageIsSection = false;
    dataLayer.pageIsProductSection = false;

    var productSearch = res.viewData.productSearch;
    if (!productSearch) {
        return;
    }

    var SecureEnoder = require('dw/util/SecureEncoder');

    dataLayer.searchKeyword = SecureEnoder.forJSONValue(productSearch.searchKeywords || '');
    if (!productSearch.isCategorySearch || productSearch.isRefinedCategorySearch) {
        return;
    }

    handleCategoryFields(res.viewData.category || productSearch.category, dataLayer);
}

/**
 * Handles product related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleProductRelatedFields(req, res, dataLayer) {
    var product = res.viewData.product;
    if (!product) {
        return;
    }

    dataLayer.pageArticleDescription = product.longDescription;
    dataLayer.productFeedbackCount = !!product.ratingSummary && !!product.ratingSummary.ratingCount ? product.ratingSummary.ratingCount : 0;
    dataLayer.productInStock = product.available;
    dataLayer.productPrice = !!product.price && !!product.price.sales && !!product.price.sales.value ? product.price.sales.value : null;
    dataLayer.productComplementarySkus = !!product.recommendationsComplementary && product.recommendationsComplementary.itemIdsArr ? product.recommendationsComplementary.itemIdsArr : [];
    if (product.images && product.images.large[0]) {
        dataLayer.pageArticleImageUrl = product.images.large[0].absUrl;
    }

    if (product.custom && product.custom.ingredients) {
        dataLayer.productIngredients = product.custom.ingredients.source;
    }

    var productCategory = product.category;
    handleCategoryFields(productCategory, dataLayer);
}

/**
 * Handles article related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleArticlePage(req, res, dataLayer) {
    var content = res.viewData.content;
    if (!content) {
        dataLayer.pageIsArticle = false;
        return;
    }

    dataLayer.pageIsArticle = true;
    dataLayer.pageArticleId = content.ID;
    dataLayer.pageArticleTitle = content.name;
    dataLayer.pageArticleShortTitle = '';
    dataLayer.pageArticleSku = content.ID;
    dataLayer.pageArticleRangeTitle = '';
}

/**
 * Handles order confirmation page related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleConfirmationPage(req, res, dataLayer) {
    var order = res.viewData.order;
    if (!order) {
        return;
    }
    var OrderMgr = require('dw/order/OrderMgr');
    var apiOrder = OrderMgr.getOrder(order.orderNumber);
    if (apiOrder) {
        var defaultShipment = apiOrder.getDefaultShipment();
        if (defaultShipment && defaultShipment.shippingMethod) {
            dataLayer.transactionShippingMethod = defaultShipment.shippingMethod.displayName;
        }
        if (apiOrder.customer && !apiOrder.customer.registered) {
            dataLayer.funnelType = 'guest';
        }
    }
}

/**
 * Handles user related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleCommonFields(req, res, dataLayer) {
    var Locale = require('dw/util/Locale');
    var currentLocale = Locale.getLocale(request.locale);
    var viewData = res.getViewData();

    dataLayer.ISOA3 = currentLocale.ISO3Country;
    dataLayer.country = currentLocale.getCountry();
    dataLayer.language = currentLocale.getLanguage();
    dataLayer.pageTitle = Object.hasOwnProperty.call(viewData, 'CurrentPageMetaData') ? viewData.CurrentPageMetaData.title : '';
    dataLayer.brand = 'YSL';
    dataLayer.pagePath = request.httpURL.relative().toString();
    dataLayer.pageReferrerUrl = request.httpReferer;
    dataLayer.pageTemplate = res.viewData.action;
    dataLayer.pageUrl = request.httpURL.abs().toString();
    dataLayer.pageCategory = getPageType(res);
    dataLayer.siteTypeLevel = 'main';
}

/**
 * Handles basket related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleBasketRelatedFields(req, res, dataLayer) {
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.currentBasket;
    if (!currentBasket) {
        return;
    }

    var collections = require('*/cartridge/scripts/util/collections');
    var ImageModel = require('*/cartridge/models/product/productImages');

    var orderProductIds = [];
    var orderProductNames = [];
    var orderProductImagesUrl = [];
    var orderProductPrices = [];
    var orderProductQuantities = [];
    var orderProductSkus = [];
    var orderProductSectionKeys = [];
    var orderProductSectionTitles = [];
    var orderProductSections = [];
    var orderBagDiscountNames = [];
    var orderBagDiscountAmounts = [];

    var lineItems = currentBasket.getAllProductLineItems();
    collections.forEach(lineItems, function (lineItem) {
        if (!lineItem.product) {
            return;
        }
        orderProductIds.push(lineItem.product.ID);
        orderProductNames.push(lineItem.product.name);
        var imageModel = new ImageModel(lineItem.product, { types: ['large'], quantity: 'all' });
        if (imageModel.large[0]) {
            orderProductImagesUrl.push(imageModel.large[0].absUrl);
        }
        orderProductPrices.push(lineItem.product.priceModel.price.value);
        // orderProductExtendedPrices.push(StringUtils.formatMoney(lineItem.product.priceModel.price));
        orderProductQuantities.push(lineItem.quantityValue);
        orderProductSkus.push(lineItem.product.manufacturerSKU ? lineItem.product.manufacturerSKU : '');
        var primaryCategory = lineItem.product.primaryCategory;
        orderProductSections.push(primaryCategory ? primaryCategory.ID : '');
        orderProductSectionKeys.push(primaryCategory ? primaryCategory.ID : '');
        orderProductSectionTitles.push(primaryCategory ? primaryCategory.displayName : '');

        collections.forEach(lineItem.priceAdjustments, function (priceAdjustment) {
            if (priceAdjustment.isCustom()) {
                return;
            }

            orderBagDiscountNames.push(priceAdjustment.campaignID);
            orderBagDiscountAmounts.push(-1 * priceAdjustment.priceValue);
        });
    });

    dataLayer.sampleSku = orderProductIds && orderProductIds.length ? orderProductIds.join('::') : '';
    dataLayer.sampleName = orderProductSectionTitles && orderProductSectionTitles.length ? orderProductSectionTitles.join('::') : '';
}


/**
 * Gives current site type (Tablet, Mobile, or Desktop)
 * @param {Object} request - The current request
 * @returns {string} platform
 */
function getSiteType() {
    var userAgent = request.httpUserAgent;
    if (!userAgent) {
        userAgent = request.httpHeaders['user-agent'];
    }
    var platform = '';
    if (userAgent) {
        if (userAgent.match(/Tablet|iPad/i)) {
            platform = 'Tablet';
        } else if (userAgent.match(/Mobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Opera Mini|\bCrMo\/|Opera Mobi/i)) {
            platform = 'Mobile';
        } else {
            platform = 'Desktop';
        }
    }
    return platform;
}

/**
 * Handles user related tags
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - dataLayer object
 */
function handleUserRelatedFields(req, res, dataLayer) {
    var customer = request.session.customer;
    dataLayer.uiLoggedStatus = customer.authenticated;
    dataLayer.userAgent = request.httpUserAgent;
    dataLayer.siteType = getSiteType();
    if (!customer.authenticated) {
        return;
    }
    var currentCustomerProfile = customer.profile;
    dataLayer.uiUser = currentCustomerProfile.customerNo;
    dataLayer.userEmail = currentCustomerProfile.email;
    dataLayer.emailAddress = currentCustomerProfile.email;
    dataLayer.userFirstName = currentCustomerProfile.firstName;
    dataLayer.userLastName = currentCustomerProfile.lastName;
    dataLayer.userTitle = currentCustomerProfile.title;
    if (currentCustomerProfile.custom.emailOptin) {
        dataLayer.newsletterSubscription = 'true';
    }
    if (customer.addressBook.preferredAddress) {
        var defaultAddress = customer.addressBook.preferredAddress;
        dataLayer.uiPostalCode = defaultAddress.postalCode;
        dataLayer.uiCity = defaultAddress.city;
    }
    if (currentCustomerProfile.birthday) {
        dataLayer.uiBirthday = currentCustomerProfile.birthday.getFullYear();
    }
}

/**
 * Builds dataLayer Object from req & res & model
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @returns {Object} dataLayer - the data layer object
*/
function getCachablePageDataLayer(req, res) {
    var dataLayer = res.viewData.pageDataLayer || {};
    handleCommonFields(req, res, dataLayer);
    switch (res.viewData.action) {
        case 'Cart-Show':
            dataLayer.currentBagStep = 1;
            break;
        case 'Checkout-Begin':
            dataLayer.currentBagStep = 3;
            break;
        case 'Product-Show':
            handleProductRelatedFields(req, res, dataLayer);
            break;
        case 'Search-Show':
            handleCategoryRelatedFields(req, res, dataLayer);
            break;
        case 'Order-Confirm':
            handleConfirmationPage(req, res, dataLayer);
            break;
        case 'Page-Show':
            handleArticlePage(req, res, dataLayer);
            break;
        default:
            break;
    }
    return dataLayer;
}

/**
 * Extends dataLayer Object from with non cachable attributes
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - Existing dataLayer object
 * @returns {Object} dataLayer - the data layer object
*/
function getNonCachablePageDataLayer(req, res, dataLayer) {
    dataLayer = dataLayer || {};
    handleUserRelatedFields(req, res, dataLayer);
    handleBasketRelatedFields(req, res, dataLayer);
    return dataLayer;
}

/**
 * Attach Page Data Layer object into viewdata
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 */
function attachCachablePageDataLayer(req, res) {
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    res.setViewData({
        pageDataLayer: getCachablePageDataLayer(req, res)
    });
}

/**
 * Attach Extended Page Data Layer object into viewdata by using existing datalayer
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {Object} dataLayer - existing datalayer object
 * @param {Object} action - the parent action
 */
function attachNonCachablePageDataLayer(req, res, dataLayer, action) { // eslint-disable-line no-unused-vars
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    res.setViewData({
        pageDataLayer: getNonCachablePageDataLayer(req, res, dataLayer)
    });
}

module.exports = {
    attachCachablePageDataLayer: attachCachablePageDataLayer,
    attachNonCachablePageDataLayer: attachNonCachablePageDataLayer
};
