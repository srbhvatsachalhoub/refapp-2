'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var baseProductHelper = module.superModule;
var originalExports = Object.keys(module.superModule);

originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});

/**
 * If a product is master auto select first variation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Object with selected parameters
 */
function normalizeSelectedAttributes(apiProduct, params) {
    if (!apiProduct.master) {
        return params.variables;
    }

    var variables = params.variables || {};
    if (apiProduct.variationModel) {
        collections.forEach(apiProduct.variationModel.productVariationAttributes, function (attribute) {
            var allValues = apiProduct.variationModel.getAllValues(attribute);
            if (allValues.length > 0) {
                variables[attribute.ID] = {
                    id: apiProduct.ID,
                    value: allValues.get(0).ID
                };
            }
        });
    }

    return Object.keys(variables) ? variables : null;
}

/**
 * Get information for model creation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Config object
 */
function getConfig(apiProduct, params) {
    var options = baseProductHelper.getConfig(apiProduct, params);

    if (params.pview === 'tile' && apiProduct.master) {
        if (!apiProduct.available) {
            return options;
        }

        // Preselect first variation
        var variables = normalizeSelectedAttributes(apiProduct, params);
        var variationModel = baseProductHelper.getVariationModel(apiProduct, variables);

        options.variationModel = variationModel;

        var lowPriceVariant;
        if (variationModel.master && variationModel.master.priceModel && variationModel.master.priceModel.minPrice && variationModel.master.priceModel.minPrice.value) {
            var minPriceValue = variationModel.master.priceModel.minPrice.value;
            lowPriceVariant = collections.find(variationModel.variants, function (variant) {
                if (!variant.priceModel || !variant.priceModel.price || !variant.priceModel.price.value) {
                    return false;
                }
                return minPriceValue === variant.priceModel.price.value;
            });
        }

        options.apiProduct = lowPriceVariant || variationModel.selectedVariant || apiProduct;
        options.productType = baseProductHelper.getProductType(options.apiProduct);
    }

    return options;
}

/**
 * Get the product tile context object
 * @param {Object} productTileParams - the JSON productTileParams
 * @return {Object} the product tile context object
 */
function getProductTileContext(productTileParams) {
    var URLUtils = require('dw/web/URLUtils');
    var context = baseProductHelper.getProductTileContext(productTileParams);

    if (context.product) {
        context.addToCartUrl = URLUtils.url('Cart-AddProduct');
    }

    return context;
}

module.exports.getProductTileContext = getProductTileContext;
module.exports.getConfig = getConfig;
