/**
 * Builds customer specific details for Order
 * @param {Object} orderModel order export model
 * @param {dw.order.Order} lineItemCtnr lineItemCtnr of Order
 */
module.exports = function (orderModel, lineItemCtnr) {
    var shipment = lineItemCtnr.getDefaultShipment();
    var shippingAddress = shipment.getShippingAddress();
    var shippingMethod = shipment.getShippingMethod();
    var currentSite = require('dw/system/Site').current;
    var giftWrapDescription = currentSite.getCustomPreferenceValue('giftWrapDescription');

    Object.defineProperty(orderModel, 'Brand_Name', {
        enumerable: true,
        value: currentSite.getCustomPreferenceValue('siteBrand') || currentSite.ID
    });

    if (shippingAddress.getAddress1()) {
        Object.defineProperty(orderModel, 'Customer_Address1', {
            enumerable: true,
            value: shippingAddress.getAddress1()
        });
    }
    if (shippingAddress.getAddress2()) {
        Object.defineProperty(orderModel, 'Customer_Address2', {
            enumerable: true,
            value: shippingAddress.getAddress2()
        });
    }
    if (shippingAddress.getCity()) {
        var city = shippingAddress.getCity();
        var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');
        city = addressHelpers.getCityReplacement(city, lineItemCtnr.customerLocaleID);
        Object.defineProperty(orderModel, 'Customer_City', {
            enumerable: true,
            value: city
        });
    }
    if (shippingAddress.getCountryCode()) {
        Object.defineProperty(orderModel, 'Customer_Country', {
            enumerable: true,
            value: shippingAddress.getCountryCode().value
        });
    }
    if (shippingAddress.getFirstName()) {
        Object.defineProperty(orderModel, 'Customer_FirstName', {
            enumerable: true,
            value: shippingAddress.getFirstName()
        });
    }
    if (shippingAddress.getLastName()) {
        Object.defineProperty(orderModel, 'Customer_LastName', {
            enumerable: true,
            value: shippingAddress.getLastName()
        });
    }
    if (shippingAddress.getPostalCode()) {
        Object.defineProperty(orderModel, 'Customer_PostalCode', {
            enumerable: true,
            value: shippingAddress.getPostalCode()
        });
    }
    if (shippingAddress.getTitle()) {
        Object.defineProperty(orderModel, 'Customer_Title', {
            enumerable: true,
            value: shippingAddress.getTitle()
        });
    }
    if (shippingAddress.getPhone()) {
        Object.defineProperty(orderModel, 'Customer_Phone', {
            enumerable: true,
            value: shippingAddress.getPhone()
        });
    }
    Object.defineProperty(orderModel, 'Gift_Message_Indicator', {
        enumerable: true,
        value: !!shipment.gift
    });
    if (shipment.custom.giftWrapDescription || (shipment.custom.isGiftWrapped && giftWrapDescription)) {
        Object.defineProperty(orderModel, 'Gift_Wrap_Description', {
            enumerable: true,
            value: shipment.custom.giftWrapDescription ? shipment.custom.giftWrapDescription : giftWrapDescription
        });
    }
    if (shipment.giftMessage) {
        var giftMessage = {
            Gift_Message1: shipment.giftMessage
        };
        Object.defineProperty(orderModel, 'Gift_Message', {
            enumerable: true,
            value: giftMessage
        });
    }
    Object.defineProperties(orderModel, {
        Customer_ID: {
            enumerable: true,
            value: lineItemCtnr.getCustomerNo()
        },
        Customer_Order_ID: {
            enumerable: true,
            value: lineItemCtnr.getOrderNo()
        },
        Customer_Email: {
            enumerable: true,
            value: lineItemCtnr.getCustomerEmail()
        },
        Ship_Method: {
            enumerable: true,
            value: 'methodRecordType' in shippingMethod.custom ? shippingMethod.custom.methodRecordType.value : shippingMethod.ID
        }
    });
};
