'use strict';

/**
 * Define speedbus related constants.
 */
module.exports = {
    shipping: {
        keys: {
            shippingStatus: 'shippingStatus',
            additionalShippingStatus: 'c_additionalOrderShipmentStatus'
        },
        values: {
            shipped: 'shipped',
            notShipped: 'not_shipped',
            packed: 'packed',
            delivered: 'delivered'
        }
    },
    shipment: {
        keys: {
            shipments: 'shipments',
            trackingNumber: 'trackingNumber'
        }
    },
    order: {
        keys: {
            status: 'status',
            externalOrderStatus: 'externalOrderStatus'
        },
        values: {
            cancelled: 'cancelled',
            refunded: 'refunded'
        }
    },
    pspPaymentStatus: {
        authorized: 'authorized',
        captured: 'captured',
        voided: 'voided',
        refunded: 'refunded'
    }
};
