'use strict';

/**
 * Recognize Order as Sale
 * @param {dw.order.Order} order order to be recognized as SALE
 * @returns {Object} Status
 */
function execute(order) {
    var speedBusService = require('*/cartridge/scripts/services/speedBusService');
    var omsSpeedBusService = require('*/cartridge/scripts/services/omsSpeedBusService');
    var speedBusHelpers = require('*/cartridge/scripts/helpers/speedbusHelpers');
    var logger = require('dw/system/Logger').getLogger('speedbus');
    var recognizeSaleOrder = require('*/cartridge/models/speedbus/recognizeSaleOrder')(order);
    var Order = require('dw/order/Order');
    var StringUtils = require('dw/util/StringUtils');
    var Transaction = require('dw/system/Transaction');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var Site = require('dw/system/Site');

    var requestPayload = {
        method: 'PUT',
        body: recognizeSaleOrder
    };
    var serviceResult;
    var speedBusServiceType = Site.current.getCustomPreferenceValue('speedBusServiceType');

    if (speedBusServiceType && speedBusServiceType.value === 'oms') {
        serviceResult = omsSpeedBusService.call(requestPayload);
    } else {
        serviceResult = speedBusService.call(requestPayload);
    }
    var serviceErrorResponse = StringUtils.format('status: {0} errorMessage: {1}', serviceResult.status, serviceResult.errorMessage);
    if (serviceResult.ok) {
        var serviceResponse = JSON.stringify(serviceResult.getObject());
        if (speedBusHelpers.isExportSucceeded(serviceResult.getObject())) {
            if (order.getExportStatus().value !== Order.EXPORT_STATUS_EXPORTED) {
                hooksHelper('app.order.status.setStatus', 'setStatus', [order, Order.ORDER_STATUS_COMPLETED], function () {
                    Transaction.wrap(function () {
                        order.setStatus(Order.ORDER_STATUS_COMPLETED);
                    });
                });
                hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_EXPORTED, { subject: 'SpeedBus SALE', text: serviceResponse }], function () {
                    Transaction.wrap(function () {
                        order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
                        order.addNote('SpeedBus SALE', serviceResponse);
                    });
                });
                // SFSC update trigger
                speedBusHelpers.sfscOrderUpdateTrigger(order);
            }
            return {
                success: true
            };
        }
        if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
            hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED, { subject: 'SpeedBus SALE ERROR', text: StringUtils.format('{0} {1}', serviceResponse, serviceErrorResponse) }], function () {
                Transaction.wrap(function () {
                    order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                    order.addNote('SpeedBus SALE ERROR', serviceResponse);
                });
            });
        }
        logger.error('While recognizing sale of order {0} an unexpected issue is occured \n\n {1}', order.orderNo, serviceErrorResponse);
        return {
            success: false,
            message: serviceErrorResponse,
            raw: serviceResult
        };
    }
    if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED], function () {
            Transaction.wrap(function () {
                order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                order.addNote('SpeedBus SALE ERROR', serviceErrorResponse);
            });
        });
    }
    logger.error('While recognizing sale of order {0} an unexpected issue is occured {1}', order.orderNo, serviceErrorResponse);
    return {
        success: false,
        message: serviceErrorResponse,
        raw: serviceResult
    };
}

module.exports = {
    execute: execute
};
