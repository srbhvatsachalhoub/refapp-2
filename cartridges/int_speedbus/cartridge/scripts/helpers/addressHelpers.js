'use strict';

var base = module.superModule;

// IMPORTANT! Added intentionally to avoid unnecessary dependency for the integration. Actual implementation must be in app cartridge
/**
 * Gets array of state map with code
 * @param {string} locale locale
 * @returns {Array} state set
 */
function getStates(locale) {
    return locale;
}
/**
 * Gets array of city map with code
 * @param {string} locale locale
 * @returns {Array} city set
 */
function getCities(locale) {
    return locale;
}
/**
 * Gets value of target locale
 * @param {string} state actual state value
 * @param {string} originLocale original locale
 * @returns {string} value of target locale
 */
function getStateReplacement(state, originLocale) { // eslint-disable-line
    return state;
}
/**
 * Gets value of target locale
 * @param {string} city actual city value
 * @param {string} originLocale original locale
 * @returns {string} value of target locale
 */
function getCityReplacement(city, originLocale) { // eslint-disable-line
    return city;
}

base.getStates = getStates;
base.getCities = getCities;
base.getStateReplacement = getStateReplacement;
base.getCityReplacement = getCityReplacement;
base.findCurrentCityValue = function (actualValue) { return actualValue; };
base.findCurrentStateValue = function (actualValue) { return actualValue; };

module.exports = base;
