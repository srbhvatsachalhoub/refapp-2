'use strict';

var speedbusConstants = require('*/cartridge/scripts/util/speedbusConstants');
var orderStatusHelpers = require('*/cartridge/scripts/helpers/orderStatusHelpers');
var speedbusHelpers = require('*/cartridge/scripts/helpers/speedbusHelpers');

/**
 * Updates the order shipping status.
 * Possible values: shipped, not_shipped
 * @param {dw.order.Order} order - The current order
 * @param {Object} orderInput - The JSON object body that belongs to OCAPI request
 */
function updateShippingStatus(order, orderInput) {
    var shippingStatus = (orderInput[speedbusConstants.shipping.keys.shippingStatus] || '').toLowerCase();
    var setShippingStatusResult;
    switch (shippingStatus) {
        case speedbusConstants.shipping.values.shipped:
            setShippingStatusResult = orderStatusHelpers.setShippingStatusAsShipped(order);
            orderStatusHelpers.setPaymentStatusAsPaid(order, speedbusConstants.shipping.values.shipped);
            if (setShippingStatusResult) {
                orderStatusHelpers.setExportStatusAsReady(order);
            }
            speedbusHelpers.sfscOrderUpdateTrigger(order);
            break;
        case speedbusConstants.shipping.values.notShipped:
            setShippingStatusResult = orderStatusHelpers.setShippingStatusAsNotShipped(order);
            if (setShippingStatusResult) {
                orderStatusHelpers.setExportStatusAsReady(order);
            }
            speedbusHelpers.sfscOrderUpdateTrigger(order);
            break;
        default: break;
    }
}

/**
 * Updates the order additional shipping status.
 * Possible values: packed, delivered
 * @param {dw.order.Order} order - The current order
 * @param {Object} orderInput - The JSON object body that belongs to OCAPI request
 */
function updateAdditionalShippingStatus(order, orderInput) {
    var additionalShippingStatus = (orderInput[speedbusConstants.shipping.keys.additionalShippingStatus] || '').toLowerCase();
    switch (additionalShippingStatus) {
        case speedbusConstants.shipping.values.packed:
            orderStatusHelpers.setAdditionalShippingStatus(order, additionalShippingStatus);
            orderStatusHelpers.setPaymentStatusAsPaid(order, additionalShippingStatus);
            orderStatusHelpers.setMarketingCloudStatus(order, 'shipped');
            speedbusHelpers.sfscOrderUpdateTrigger(order);
            break;
        case speedbusConstants.shipping.values.delivered:
            orderStatusHelpers.setAdditionalShippingStatus(order, additionalShippingStatus);
            orderStatusHelpers.setPaymentStatusAsPaid(order, additionalShippingStatus);

            speedbusHelpers.sfscOrderUpdateTrigger(order);
            break;
        default: break;
    }
}

/**
 * Updates the order status.
 * Possible values: cancel, refund
 * @param {dw.order.Order} order - The current order
 * @param {Object} orderInput - The JSON object body that belongs to OCAPI request
 */
function updateOrderStatus(order, orderInput) {
    var orderStatus = (orderInput[speedbusConstants.order.keys.status] || orderInput[speedbusConstants.order.keys.externalOrderStatus] || '').toLowerCase();
    switch (orderStatus) {
        case speedbusConstants.order.values.cancelled:
        case speedbusConstants.order.values.refunded:
            var cancelDescription = 'c_cancel_description' in orderInput && !!orderInput.c_cancel_description ? orderInput.c_cancel_description : null;
            var setOrderStatusResult = orderStatusHelpers.setOrderStatus(order, orderStatus, cancelDescription);
            if (setOrderStatusResult) {
                orderStatusHelpers.setExportStatusAsReady(order);
                orderStatusHelpers.setMarketingCloudStatus(order, 'cancelled');
            }

            speedbusHelpers.sfscOrderUpdateTrigger(order);
            break;
        default: break;
    }
}

/**
 * Updates the order shipments.
 * Possible values in shipments: trackingNumber
 * @param {dw.order.Order} order - The current order
 * @param {dw.util.ArrayList} shipmentsInput - The Array object that belongs to OCAPI request's shipments
 */
function updateShipments(order, shipmentsInput) {
    if (!shipmentsInput || !shipmentsInput.length) {
        return;
    }

    var Transaction = require('dw/system/Transaction');
    var StringUtils = require('dw/util/StringUtils');
    var collections = require('*/cartridge/scripts/util/collections');

    collections.forEach(shipmentsInput, function (shipmentInput) {
        var shipment;
        var shippingMethod;
        if (shipmentInput.shipmentId) {
            shipment = order.getShipment(shipmentInput.shipmentId);
        } else {
            shipment = order.getDefaultShipment();
        }

        if (shipment && shipmentInput.trackingNumber) {
            var Mac = require('dw/crypto/Mac');
            var Site = require('dw/system/Site');
            var Encoding = require('dw/crypto/Encoding');
            var Locale = require('dw/util/Locale');
            Transaction.wrap(function () {
                shipment.setTrackingNumber(shipmentInput.trackingNumber);
                shippingMethod = shipment.shippingMethod;
                var speedBusTrackingPrivateKey = Site.current.getCustomPreferenceValue('speedBusTrackingPrivateKey');
                if (shippingMethod && shippingMethod.custom.trackingUrl && speedBusTrackingPrivateKey) {
                    var custLocale = Locale.getLocale(order.customerLocaleID);
                    var hmac = new Mac(Mac.HMAC_SHA_256);
                    var token = hmac.digest(shipmentInput.trackingNumber, speedBusTrackingPrivateKey);
                    shipment.custom.trackingUrl = StringUtils.format(
                        shippingMethod.custom.trackingUrl,
                        shipmentInput.trackingNumber,
                        Encoding.toHex(token),
                        custLocale.getLanguage());
                }
            });
        }
    });
}

module.exports = {
    updateShippingStatus: updateShippingStatus,
    updateAdditionalShippingStatus: updateAdditionalShippingStatus,
    updateOrderStatus: updateOrderStatus,
    updateShipments: updateShipments
};
