'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');


/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.text = content.text || null;
    model.image = content.image ? ImageTransformation.getScaledImage(content.image) : null;
    model.imageMobile = content.imageMobile ? ImageTransformation.getScaledImage(content.imageMobile) : null;
    model.link = content.link || '#';
    model.alt = content.alt || null;
    model.useIcon = content.useIcon || false;

    return new Template('experience/components/commerce_assets/imageAndTextCustom').render(model).text;
};
