<isset name="miniCart" value="${true}" scope="page" />

<div class="minicart-wrapper cart row">
    <div class="col-7 minicart-product-container">
        <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
            <isinclude template="reporting/reportingUrls" />
        </isif>
        <div class="minicart-title">
            ${Resource.msg('link.minicart.head', 'cart', null)}
        </div>
        <div class="product-carousel global-carousel-slider js-global-carousel js-minicart-carousel"
            data-carousel-options='{
                "slidesToShow": 1,
                "slidesToScroll": 1,
                "rows": 1,
                "dots": false,
                "arrows": true,
                "infinite": false
            }'>
            <isloop items="${pdict.items}" var="lineItem">
                <div class="minicart-product-item">
                    <isif condition="${lineItem.bonusProductLineItemUUID === 'bonus'}">
                        <div class="${miniCart ? 'bonus-product-line-item' : ''} uuid-${lineItem.UUID}">
                            <isinclude template="checkout/productCard/bonusProductCard" />
                        </div>
                    <iselse/>
                        <div class="uuid-${lineItem.UUID}">
                            <isif condition="${lineItem.noProduct === true}">
                                <isinclude template="checkout/productCard/uncategorizedProductCard" />
                            <iselse/>
                                <isinclude template="checkout/productCard/productCard" />
                            </isif>
                        </div>
                    </isif>
                </div>
            </isloop>
            <isprint value="${pdict.items.length}" />
        </div>
    </div>
    <div class="col-5 minicart-actions text-right">
        <div class="row">
            <div class="col-12">
                <isif condition="${pdict.isQualifiedForFreeShipping}">
                    <div class="minicart-checkout-message">
                        ${Resource.msg('label.qualified.free.shipping', 'cart', null)}
                    </div>
                </isif>
                <div class="minicart-subtotal text-uppercase">
                    <div class="minicart-subtotal-label">
                        <span>${Resource.msg('label.sub.total', 'cart', null)}</span>
                    </div>
                    <div class="minicart-subtotal-amount sub-total">
                        ${pdict.totals.subTotal}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="minicart-checkout">
                    <a href="${URLUtils.https('Cart-Show')}"
                        class="btn btn-primary btn-block cart-btn ${pdict.valid && pdict.valid.error ? 'disabled' : ''} col"
                        role="button"
                        aria-pressed="true">
                        ${Resource.msgf('button.view.cart','cart', null, pdict.numItems)}
                    </a>
                    <a href="${URLUtils.https('Checkout-Login')}"
                        class="btn btn-primary btn-block checkout-btn ${pdict.valid && pdict.valid.error ? 'disabled' : ''} col"
                        role="button"
                        aria-pressed="true">
                        ${Resource.msg('button.checkout','cart',null)}
                    </a>
                </div>
            </div>
        </div>
        <div class="minicart-error cart-error minicart-errorBox">
            <isif condition="${pdict.valid.error && pdict.items.length !== 0}">
                <div class="alert alert-danger alert-dismissible valid-cart-error fade show" role="alert">
                    <iscomment> <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> </iscomment>

                    <button class="miniCart-errorBox-dismiss" data-dismiss="alert" aria-label="Close">
                        <i class="sc-icon-cross"></i>
                    </button>
                    ${pdict.valid.message}
                </div>
            </isif>
        </div>
    </div>
</div>
