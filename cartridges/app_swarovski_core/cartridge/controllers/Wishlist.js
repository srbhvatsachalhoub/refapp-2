'use strict';

var server = require('server');
server.extend(module.superModule);

var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var cache = require('*/cartridge/scripts/middleware/cache');

server.get(
    'IncludeHeaderMenu',
    consentTracking.consent,
    server.middleware.include,
    cache.applyDefaultCache,
    function (req, res, next) {
        var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
        var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
        var WishlistModel = require('*/cartridge/models/productList');
        var loggedIn = !req.currentCustomer.raw.anonymous;
        var product = '';

        var wishlistModel = new WishlistModel(
            list,
            {
                type: 'wishlist',
                publicView: false,
                pageSize: 1,
                pageNumber: 1
            }
        ).productList;

        if (wishlistModel.items && wishlistModel.items.length > 0) {
            var productTileParams = {
                pid: wishlistModel.items[0].pid,
                pview: 'tile'
            };
            product = productHelpers.getProductTileContext(productTileParams);
        }

        res.render('/components/header/wishlist', {
            totalWishlistItems: wishlistModel.length,
            product: product,
            loggedIn: loggedIn
        });

        next();
    }
);

module.exports = server.exports();
