'use strict';

module.exports = function () {
    $('.js-load-video').click(function (e) {
        e.preventDefault();
        var videoCover = $(this).parent().find('.js-player-cover');

        $(this).hide();
        videoCover.fadeOut();
        $(this).parent().addClass('video-player-expanded');
        var targetElement = $(this).parent().find('.js-embeded-video');
        var videoId = targetElement.attr('data-video-id');
        var videoSrc = 'https://www.youtube.com/embed/';
        var videoFrame =
            '<iframe width="100%" height="100%" src=" ' +
            videoSrc +
            videoId +
            '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen"></iframe>';
        targetElement.html(videoFrame);
    });
};
