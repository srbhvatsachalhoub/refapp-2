'use strict';

var throttle = require('lodash/throttle');

/**
 * basic menu slider
 */
function menuSlider() {
    var args = arguments[0];
    var width = $(window).width();
    var breakpoint = args.element.attr('data-menu-slider-breakpoint') || 'md';
    var initialized = args.element.attr('data-menu-slider-initialized') || false;
    var isVisible = args.element.is(':visible');
    if (!isVisible || (args.action === 'scroll' && initialized !== false)) {
        return;
    }

    if (width >= window.RA_BREAKPOINTS[breakpoint]) {
        var mainMenuMaxWidth = args.element.innerWidth();
        var mainMenuWidth = 0;
        var breakLoop = false;
        var firstItem = '';

        var menuItems = args.element.find('ul:first > li');

        if (args.action === 'init') {
            menuItems.removeClass('d-none');
        }

        var menuItemsArray = menuItems.get();

        if (args.action === 'prev') {
            menuItemsArray = menuItemsArray.reverse();
        }

        if (args.action === 'next' || args.action === 'prev') {
            menuItemsArray = menuItemsArray.filter(function (item) {
                if (breakLoop !== true && $(item).hasClass('d-none')) {
                    return false;
                }

                breakLoop = true;
                return true;
            });

            $(menuItemsArray[0]).addClass('d-none');
            firstItem = menuItemsArray.shift();
        }

        breakLoop = false;
        menuItemsArray.map(function (item) {
            var $item = $(item);

            $item.removeClass('d-none');
            var menuItemWidth = $item.outerWidth(true);
            if (breakLoop !== true && mainMenuWidth + menuItemWidth < mainMenuMaxWidth) {
                mainMenuWidth += $item.outerWidth(true);
            } else {
                breakLoop = true;
                $item.addClass('d-none');
            }

            return true;
        });

        if (firstItem !== '') {
            menuItemsArray.unshift(firstItem);
        }

        args.element.find('[class*="js-menu-slider-"]').addClass('d-none');

        if ($(menuItemsArray.pop()).hasClass('d-none')) {
            args.element.find(args.action === 'prev' ? '.js-menu-slider-prev' : '.js-menu-slider-next').removeClass('d-none');
        }

        if ($(menuItemsArray.shift()).hasClass('d-none')) {
            args.element.find(args.action === 'prev' ? '.js-menu-slider-next' : '.js-menu-slider-prev').removeClass('d-none');
        }
    } else {
        args.element.find('ul:first > li').removeClass('d-none');
        args.element.find('[class*="js-menu-slider-"]').addClass('d-none');
    }

    args.element.attr('data-menu-slider-initialized', true);
}

module.exports = function (selector) {
    $(selector || '.js-menu-slider').each(function () {
        var menu = $(this);
        var $win = $(window);

        menuSlider({
            element: menu,
            action: 'init'
        });

        menu.find('.js-menu-slider-next').on('click', function (e) {
            e.preventDefault();

            menuSlider({
                element: menu,
                action: 'next'
            });
        });

        menu.find('.js-menu-slider-prev').on('click', function (e) {
            e.preventDefault();

            menuSlider({
                element: menu,
                action: 'prev'
            });
        });

        $win.on('resize', throttle(function () {
            menuSlider({
                element: menu,
                action: 'init'
            });
        }, 100));

        // need improvements
        $win.on('scroll', throttle(function () {
            menuSlider({
                element: menu,
                action: 'scroll'
            });
        }, 100));
    });
};
