'use strict';

module.exports = function initReadMore() {
    $(document).on('click', '.js-read-more', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $infoBlock = $(this).closest('.js-info-block');
        var $infoBlockText = $infoBlock.find('.js-info-block-description');
        $infoBlockText.removeClass('view-more');
    });
};
