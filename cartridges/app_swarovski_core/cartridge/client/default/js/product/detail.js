'use strict';

var base = require('brand_core/product/detail');
var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');
var quantitySelector = require('./quantitySelector');

var caruselSelectors = {
    selImageCarousel: '.js-pdp-product-image-carousel',
    selThumbnailCarousel: '.js-pdp-product-thumbnail-carousel',
    selThumbnailCarouselVertical: '.js-pdp-product-thumbnail-carousel-vertical'
};

var carouselElements = {
    $imageCarousel: $(caruselSelectors.selImageCarousel),
    $thumbnailCarousel: $(caruselSelectors.selThumbnailCarousel),
    $thumbnailCarouselVertical: $(caruselSelectors.selThumbnailCarouselVertical)
};

var thumbnailCount = carouselElements.$thumbnailCarousel.find('img').length;
var thumbnailSlideLimit = 5;
var thumbnailCarouselConfig = {
    infinite: thumbnailCount > thumbnailSlideLimit,
    centerMode: thumbnailCount > thumbnailSlideLimit,
    slidesToScroll: thumbnailCount > thumbnailSlideLimit ? 1 : thumbnailCount,
    slidesToShow: thumbnailCount > thumbnailSlideLimit ? thumbnailSlideLimit : thumbnailCount,
    arrows: true,
    focusOnSelect: true,
    centerPadding: '0.21875rem',
    nextArrow: nextArrow,
    prevArrow: prevArrow
};

/**
 * Initializes product image carousel
 * @param {jQuery} $target - jQuery element for carousel
 * @param {string} asNavFor - mapping for vertical or horisontal carousel
 * @param {boolean} isThumbnail - is carousel thumbnail
 * @param {boolean} isvertical - vertical or horisontal carousel
 */
function initCarousel($target, asNavFor, isThumbnail, isvertical) {
    var config;

    if (isThumbnail) {
        config = thumbnailCarouselConfig;
        config.asNavFor = asNavFor;
        config.vertical = isvertical || false;
    } else {
        config = {
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            autoplay: false,
            nextArrow: nextArrow,
            prevArrow: prevArrow,
            asNavFor: asNavFor
        };
    }

    if ($target.get(0).slick && !($target.hasClass('slick-destroyed'))) {
        $target.slick('unslick');
    } else {
        $target.removeClass('slick-initialized');
    }

    carousel($target, config);
    // don't remove it to avoid regression on PDP
    $target.removeClass('slick-destroyed');
}

/**
 * Updates poduct id on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateProductIdAfterAttributeSelect(product, $productContainer) {
    $('.js-product-cta-placeholder').parent()
        .data('pid', $productContainer.data('pid'));
}

/**
 * Initializes product image carousel
 */
function initProductImageCarousel() {
    var windowWidth = window.innerWidth;

    if (windowWidth >= window.RA_BREAKPOINTS.xxl) {
        initCarousel(carouselElements.$imageCarousel, caruselSelectors.selThumbnailCarouselVertical);
        initCarousel(carouselElements.$thumbnailCarouselVertical, caruselSelectors.selImageCarousel, true, true);
    } else {
        initCarousel(carouselElements.$imageCarousel, caruselSelectors.selThumbnailCarousel);
        initCarousel(carouselElements.$thumbnailCarousel, caruselSelectors.selImageCarousel, true);
    }
}

/**
 * Updates carousel images on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $container - product detail section element that contains carousel
 */
function updateCarouselAfterAttributeSelect(product, $container) {
    var largeImageUrls = product.images.large;
    var smallImageUrls = product.images.small;
    var hiresImageUrls = product.images['hi-res'];
    var $imageCarousel = $container.find('.js-pdp-product-image-carousel');
    var $thumbnailCarousel = $container.find('.js-pdp-product-thumbnail-carousel');
    var $imageRef = $imageCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $thumbnailRef = $thumbnailCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var imagesFragment = document.createDocumentFragment();
    var thumbnailsFragment = document.createDocumentFragment();

    // Prepare new carousel images
    if (largeImageUrls) {
        largeImageUrls.forEach(function (image, i) {
            var $image = $imageRef.parent().clone();

            if ($image.length === 0) return;

            $image.find('img').attr('src', image.url);
            $image.find('img').attr('alt', image.alt);

            // Update zoom images
            if (hiresImageUrls && hiresImageUrls[i]) {
                $image.find('img').attr('data-zoom', hiresImageUrls[i].url);
            } else {
                $image.find('img').removeClass('.js-zoom-image');
                $image.find('img').removeData('zoom');
            }

            // Append new image to fragment
            imagesFragment.appendChild($image.get(0));
        });
    }

    // Prepare new navigation thumbnails
    if (smallImageUrls && $thumbnailRef.length) {
        smallImageUrls.forEach(function (image) {
            thumbnailsFragment.appendChild(
                $thumbnailRef.clone()
                    .attr('src', image.url)
                    .attr('alt', image.alt)
                    .get(0)
            );
        });
    }

    // Refresh image carousel content
    $imageCarousel
        .slick('unslick')
        .html(imagesFragment);

    // Refresh thumbnail carousel content
    if (carouselElements.$thumbnailCarousel.get(0).slick) {
        carouselElements.$thumbnailCarousel
            .data('count', (thumbnailsFragment.children || thumbnailsFragment.childNodes).length + '.0')
            .slick('unslick')
            .html(thumbnailsFragment)
            .removeClass('slick-initialized');
    } else {
        carouselElements.$thumbnailCarouselVertical
            .data('count', (thumbnailsFragment.children || thumbnailsFragment.childNodes).length + '.0')
            .slick('unslick')
            .html(thumbnailsFragment)
            .removeClass('slick-initialized');
    }

    // Reinitialize carousel
    initProductImageCarousel();
}

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {string} attr.values.displayValue - Attribute display value
 * @param {Object} attr.values.images - Attribute images
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processSwatchValues(attr, $productContainer) {
    var $selectedSwatch = $productContainer.find('.js-selected-swatch');
    var $selectedSwatchName = $productContainer.find('.js-selected-swatch-name');

    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer
            .find('[data-attr="' + attr.id + '"] [data-attr-value="' + attrValue.value + '"]');
        var $swatchAnchor = $attrValue.parent();

        $attrValue.removeClass('selectable unselectable selected');
        $attrValue.addClass(attrValue.selectable ? 'selectable' : 'unselectable');
        $swatchAnchor.attr('href', 'javascript:void(0)'); //eslint-disable-line

        if (attrValue.selected) {
            $attrValue.addClass('selected');
            $selectedSwatch.removeClass();
            $selectedSwatch.attr('data-attr-value', attrValue.value);
            $selectedSwatch.addClass(attr.id + '-value swatch-circle swatch-value js-selected-swatch');
            $selectedSwatch.css('background-image', 'url("' + attrValue.images.swatch[0].url + '")');
            $selectedSwatchName.html(attrValue.displayValue);
        } else if (attrValue.url) {
            $swatchAnchor.attr('href', attrValue.url);
        }
    });
}

/**
 * Process attribute values associated with an attribute that does not have image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processNonSwatchValues(attr, $productContainer) {
    var $select = $productContainer
        .find('[data-attr="' + attr.id + '"] select.select-' + attr.id);

    // Update default option's url
    $select.find('.js-attr-default-option')
        .attr('value', attr.resetUrl);


    attr.values.forEach(function (attrValue) {
        $select.find('[data-attr-value="' + attrValue.value + '"]')
            .attr('value', attrValue.url)
            .attr('disabled', !attrValue.selectable);
    });

    var $buttons = $productContainer
        .find('[data-attr="' + attr.id + '"]');
    $buttons.find('button.selected.disabled').removeClass('selected').removeClass('disabled');

    // Update default option's url
    $buttons.find('.js-attr-default-option')
        .attr('value', attr.resetUrl);

    attr.values.forEach(function (attrValue) {
        $buttons.find('button[data-attr-value="' + attrValue.value + '"]')
            .attr('data-url', attrValue.url)
            .attr('disabled', !attrValue.selectable)
            .addClass(attrValue.selected ? 'selected disabled' : '');
    });

    $select.selectpicker('refresh');
}

/**
 * Updates attribute data on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAttributesAfterAttributeSelect(product, $productContainer) {
    if (product.variationAttributes) {
        // Currently, the only attribute type that has image swatches is Color.
        var attrsWithSwatches = {
            color: 1
        };

        product.variationAttributes.forEach(function (attr) {
            if (attrsWithSwatches[attr.id]) {
                processSwatchValues(attr, $productContainer);
            } else {
                processNonSwatchValues(attr, $productContainer);
            }
        });
    }
}

/**
 * Updates quantity input and buttons on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateQuantityAfterAttributeSelect(product, $productContainer) {
    if ($productContainer.parent('.bonus-product-item').length === 0) {
        var $quantityBtn = $productContainer.find('.js-quantity-btn');
        var $quantityInput = $productContainer.find('.js-quantity-input');
        var current = $quantityInput.val();
        var min = 1;
        var max = 0;

        if (product.quantities && product.quantities.length) {
            min = product.quantities[0].value;
            max = product.quantities[product.quantities.length - 1].value;
        }

        var newQuantity = quantitySelector.getQuantity(0, current, min, max);

        if (product.available) {
            $quantityInput.val(newQuantity);
            $quantityInput.removeProp('readonly');
            $quantityInput.removeClass('disabled');
            $quantityBtn.each(function () {
                var $this = $(this);
                if ($this.hasClass('disabled-limit') === false) {
                    $this.removeClass('disabled');
                }
            });
        } else {
            $quantityInput.val(0);
            $quantityInput.prop('readonly', true);
            $quantityInput.addClass('disabled');
            $quantityBtn.addClass('disabled');
        }
    }
}

/**
 * Updates availability on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateAvailabilityAfterAttributeSelect(product, $productContainer) {
    var $availabilityContainer = $productContainer.find('.js-availability-container');

    // Update availability data attributes on contanier
    $availabilityContainer
        .data('ready-to-order', product.readyToOrder)
        .data('available', product.available)
        .addClass('d-none');

    if (!product.available &&
        product.readyToOrder &&
        product.availability &&
        product.availability.messages &&
        product.availability.messages.length) {
        // Prepare messages
        var msgHtmlStr = '';
        product.availability.messages.forEach(function (msg) {
            msgHtmlStr += ('<div class="btn btn-block btn-outline-secondary">' + msg + '</div>');
        });

        // Append message string to DOM
        $productContainer
            .find('.js-availability-msg')
            .html(msgHtmlStr);

        // Show availability container
        $availabilityContainer.removeClass('d-none');
    }
}

/**
 * Updates promotions on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updatePromotionsAfterAttributeSelect(product, $productContainer) {
    var html = '';
    if (product.promotions && product.promotions.length) {
        product.promotions.forEach(function (promotion) {
            if (promotion.calloutMsg) {
                html += '<div class="callout" title="' + promotion.details + '">' +
                    promotion.calloutMsg + '</div>';
            }
        });
    }
    $productContainer.find('.js-promotions').html(html);
}

/**
 * Updates title on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateTitleAfterAttributeSelect(product, $productContainer) {
    if (product && product.productName) {
        $productContainer.find('.js-name').html(product.productName);
    }
}

/**
 * Updates short description on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $productContainer - product detail section element that contains attributes
 */
function updateDescriptionAfterAttributeSelect(product, $productContainer) {
    if (product && product.shortDescription) {
        $productContainer.find('.js-short-description').html(product.shortDescription);
    }
}

/**
 * Updates url on each attribute change
 * @param {Object} product - product object including images array
 */
function updateUrlAfterAttributeSelect(product) {
    if (product && product.selectedProductUrl) {
        history.replaceState({}, document.title, product.selectedProductUrl);
    }
}

/**
 * Initializes product call to action box
 * that will be visible after scrolling
 */
function initProductCta() {
    var el = {
        $details: $('.js-product-details'),
        $detailsImage: $('.js-image-container'),
        $detailsName: $('.js-name-container'),
        $detailsCarousel: $('.js-pdp-product-image-carousel'),
        $detailsThumbnailCarousel: $('.js-pdp-product-thumbnail-carousel'),

        $info: $('.js-product-info'),
        $infoPrice: $('.js-price-container'),
        $infoBtn: $('.js-add-to-cart-btn-container'),
        $infoAttributes: $('.js-attributes-container'),
        $infoMainAttributes: $('.js-main-attributes-container'),

        $placeholder: $('.js-product-cta-placeholder'),
        $placeholderBtn: $('.js-product-cta-btn-ph'),
        $placeholderName: $('.js-product-cta-name-ph'),
        $placeholderImage: $('.js-product-cta-image-ph'),
        $placeholderPrice: $('.js-product-cta-price-ph'),
        $placeholderAttributes: $('.js-product-cta-attributes-ph'),
        $placeholderMainAttributes: $('.js-product-cta-main-attributes-ph')
    };

    el.$placeholder.parent().data('pid', el.$details.data('pid'));
}

base.initShippingInformationModal = function () {
    var $content = $('.js-shipping-info-content');

    // Remove shipping information button and modal if there is no content in slot
    if ($content.children().length === 0) {
        $content.closest('.js-shipping-info-container').remove();
    }
};

base.initTabsWithSlotContent = function () {
    var $content = $('.js-tab-slot-content');

    // Remove tab if there is no content in slot
    if ($content.children().length === 0) {
        var $container = $content.closest('.js-tab-slot-container');
        var $title = $('#' + $container.attr('aria-labelledby'));
        $title.remove();
        $container.remove();
    }
};


base.updateProductAfterAttributeSelect = function () {
    $('body').on('product:afterAttributeSelect', function (e, response) {
        updateProductIdAfterAttributeSelect(response.data.product, response.container);
        updateCarouselAfterAttributeSelect(response.data.product, response.container);
        updateQuantityAfterAttributeSelect(response.data.product, response.container);
        updateAttributesAfterAttributeSelect(response.data.product, response.container);
        updatePromotionsAfterAttributeSelect(response.data.product, response.container);
        updateTitleAfterAttributeSelect(response.data.product, response.container);
        updateDescriptionAfterAttributeSelect(response.data.product, response.container);
        updateUrlAfterAttributeSelect(response.data.product);
    });

    $('body').on('product:updateAvailability', function (e, response) {
        updateAvailabilityAfterAttributeSelect(response.product, response.$productContainer);
    });

    $('.js-attributes-container select:not(.js-quantity-select)').each(function () {
        var $select = $(this);
        $select.selectpicker('refresh');
    });
};

base.initViewAll = function () {
    var $doc = $(document);
    var $win = $(window);

    $doc.on('click', '.js-social-icon-share', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.js-social-icons').slideToggle('slow');
    });

    $doc.on('click', '.js-video-icon', function (e) {
        e.preventDefault();
        $('.js-video-top-image').addClass('d-none');
    });

    $doc.on('click', '.js-product-zoom-image', function (e) {
        e.preventDefault();
        $('.js-product-zoom').addClass('modal modal-md fade show');
        $('.js-body-hidden').addClass('modal-open');
        initProductImageCarousel();
    });

    $doc.on('click', '.js-product-zoom-close', function (e) {
        e.preventDefault();
        $('.js-product-zoom').removeClass('modal modal-md fade show');
        $('.js-body-hidden').removeClass('modal-open');
        initProductImageCarousel();
    });

    $doc.on('click', '[class$="-attributes-container"] button', function (e) {
        e.preventDefault();

        $(this).closest('[class$="-attributes-container"]')
            .find('select').val($(this).attr('data-url'))
            .trigger('change');
    });

    $win.on('resize', function () {
        initProductImageCarousel();
    });
};

base.initProductCta = initProductCta;
base.initQuantitySelector = quantitySelector.init;
base.initProductImageCarousel = initProductImageCarousel;

module.exports = base;
