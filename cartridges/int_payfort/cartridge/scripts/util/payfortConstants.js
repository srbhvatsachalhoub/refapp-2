'use strict';

/**
 * Define payFort related constants.
 */
module.exports = {
    channelType: {
        merchantPage20: 'merchantPage20',
        trustedChannel: 'trustedChannel'
    },
    shaType: {
        sha256: 'SHA-256',
        sha512: 'SHA-512'
    },
    command: {
        tokenization: 'TOKENIZATION',
        authorization: 'AUTHORIZATION',
        purchase: 'PURCHASE',
        capture: 'CAPTURE',
        voidAuthorization: 'VOID_AUTHORIZATION',
        refund: 'REFUND',
        createToken: 'CREATE_TOKEN',
        updateToken: 'UPDATE_TOKEN',
        inactivateToken: 'INACTIVE',
        activateToken: 'ACTIVE',
        checkStatus: 'CHECK_STATUS'
    },
    creditCardTypes: {
        visa: 'VISA',
        amex: 'AMEX',
        master: 'MASTERCARD',
        mastercard: 'MASTERCARD',
        'master card': 'MASTERCARD',
        mada: 'MADA',
        meeza: 'MEEZA',
        discover: 'DISCOVER',
        dinersclub: 'DINERSCLUB'
    },
    eci: {
        ecommerce: 'ECOMMERCE',
        moto: 'MOTO'
    },
    serviceCredentialType: {
        single: 'single',
        country: 'country',
        locale: 'locale'
    },
    logExcludedKeys: ['access_code', 'merchant_identifier', 'expiry_date', 'card_number', 'card_security_code'],
    signatureExcludedKeys: {
        default: ['signature'],
        merchantPage20: ['card_security_code', 'card_number', 'expiry_date', 'card_holder_name', 'remember_me', 'signature']
    },
    serviceStatus: {
        authorizationSuccess: '02', // 02 Authorization Success.
        captureSuccess: '04', // 04	Capture Success.
        refundSuccess: '06', // 06 Refund Success.
        authorizationVoidSuccess: '08', // 08 Authorization Voided Successfully.
        checkStatusSuccess: '12', // 12	Check status success.
        purchaseSuccess: '14', // 14 Purchase Success.
        tokenizationSuccess: '18', // 18 Tokenization success.
        onHold: '20', // 20 On hold.
        redemptionSuccess: '30', // 30 Redemption success.
        tokenCreateSuccess: '52', // 52 Token created successfully.
        tokenUpdateSuccess: '58', // 58 Token updated successfully.
        invalidRequest: '00', // 00	Invalid Request.
        uncertainTransaction: '15', // 15 Uncertain Transaction.
        authorizationFailed: '03', // 03 Authorization Failed.
        captureFailed: '05', // 05 Capture failed.
        refundFailed: '07', // 07 Refund failed.
        authorizationVoidFailed: '09', // 08 Authorization Void Failed.
        purchaseFailed: '13', // 13 Purchase Failure.
        checkStatusFailed: '11', // 11 Check status Failed.
        tokenizationFailed: '17' // 17 Tokenization failed.
    },
    serviceResponseCode: {
        threeDSecureCheck: '20064' // 20 On hold, 064 3-D Secure check requested.
    },
    serviceParams: {
        threeDSecureUrl: '3ds_url'
    },
    rememberMe: {
        yes: 'YES',
        no: 'NO'
    },
    pspPaymentStatus: {
        authorized: 'authorized',
        captured: 'captured',
        declined: 'declined',
        pending: 'pending',
        voided: 'voided',
        refunded: 'refunded',
        error: 'error'
    },
    pspPaymentStatusNoteSubject: 'Payment Status'
};
